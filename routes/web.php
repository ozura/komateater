<?php
use App\Setting;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
	return view('index');
});

// Route::get('/admin', function () {
// 	return view('admin.master');
// });


Route::get('/daftarproduksi', 'indexController@showDaftarProduksi');
Route::get('/daftarproduksi/search', 'indexController@searchDaftarProduksi');
Route::get('/daftarkatakoma/search', 'indexController@searchDaftarKataKoma');
Route::get('/daftaranggota/search', 'indexController@searchDaftarAnggota');
Route::get('/daftarvideo', 'indexController@showDaftarVideo');
Route::get('/daftarberita', 'indexController@showDaftarBerita');
Route::get('/daftarkatakoma', 'indexController@showDaftarKataKoma');
Route::get('/detailproduksi/{id}', 'indexController@showDetailProduksi');
// Route::get('/folderfoto', 'indexController@showFolderFoto');
// Route::get('/folderid', 'indexController@showFolderId');
Route::get('/detailprofil/{id}', 'indexController@showDetailProfil');
Route::get('/daftarpendiri', 'indexController@showDaftarPendiri');



// Route::get('/berita', 'adminController@showBerita');

Route::get('/pekerja', 'indexController@showDaftarPekerja');
Route::get('/pemain', 'indexController@showDaftarPemain');
Route::get('/daftarmerchandise', 'indexController@showDaftarMerchandise');

Route::get('/pemainpekerja', 'indexController@showPemainPekerja');
Route::get('/detailmerchandise/{id}', 'indexController@detailMerchandise')->name('detailmerchandise');



/* Admin User */
// Route::group(['prefix' => 'cdmanager'], function(){
// 	Route::get('/', 'AccountController@indexSignIn');
// 	Route::post('signin', 'AccountController@actionSignIn');
// 	Route::get('dashboard', 'AccountController@indexDashboard');
// 	Route::get('welcome', 'AccountController@indexWelcome');

// 	Route::group(['prefix' => 'post'], function(){
// 		Route::get('/', 'PostController@indexList');
// 		Route::get('new', 'PostController@indexManage');
//         Route::get('update/{post_id}', 'PostController@indexManage');

// 		Route::post('datatables', 'PostController@commonList');
// 	});
// 	  Route::group(['prefix' => 'collection'], function(){
//             Route::get('/', 'CollectionController@indexList');
            
//             Route::post('datatables', 'CollectionController@commonList');
//             Route::post('save', 'CollectionController@actionSave');
//             Route::post('delete', 'CollectionController@actionDelete');
//         });

// });

// Route::get('signout', 'AccountController@actionSignOut');


/* Public User */
Route::get('/', 'LayoutController@indexHome');
Route::get('posts/{category_id}', 'LayoutController@indexPosts');
Route::get('album', 'LayoutController@indexGalleries');
Route::get('gallery/{gallery}', 'LayoutController@indexGalleryImages');
Route::get('post/{post_url}', 'LayoutController@indexPost');
Route::get('file/{tanggal}/{name_file}', 'FileController@getFile');
Route::get('media-informasi/berita', 'LayoutController@indexNewses');
Route::get('media-informasi/galeri', 'LayoutController@indexGalleries');
Route::get('media-informasi/lapangan-kerja', 'LayoutController@indexJobs');

// Route::get('register', 'AccountController@actionRegister');
Route::get('change-lang', 'SettingController@actionChangeLanguage');


Route::group(['prefix' => 'cdmanager'], function(){
    Route::get('/', 'AccountController@indexSignIn');
    Route::post('signin', 'AccountController@actionSignIn');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('signout', 'AccountController@actionSignOut');
    Route::group(['prefix' => 'cdmanager'], function(){
        Route::get('dashboard', 'AccountController@indexDashboard');
        Route::get('welcome', 'AccountController@indexWelcome');
        Route::post('password/change', 'AccountController@actionChangePassword');
        
        Route::group(['prefix' => 'post'], function(){
            Route::get('/', 'PostController@indexList');
            Route::get('new', 'PostController@indexManage');
            Route::get('update/{post_id}', 'PostController@indexManage');
            
            Route::post('datatables', 'PostController@commonList');
            Route::post('save', 'PostController@actionSave');
            Route::post('delete', 'PostController@actionDelete');
        });

        Route::group(['prefix' => 'produksi'], function(){
            Route::get('/', 'ProduksiController@indexList');
            Route::get('new', 'ProduksiController@indexManage');
            Route::get('update/{post_id}', 'ProduksiController@indexManage');
            
            Route::post('datatables', 'ProduksiController@commonList');
            Route::post('save', 'ProduksiController@actionSave');
            Route::post('delete', 'ProduksiController@actionDelete');
        });

          Route::group(['prefix' => 'hproduksi'], function(){
            Route::get('/', 'HProduksiController@indexList');
            Route::get('new', 'HProduksiController@indexManage');
            Route::get('update/{post_id}', 'HProduksiController@indexManage');
            
            Route::post('datatables', 'HProduksiController@commonList');
            Route::post('save', 'HProduksiController@actionSave');
            Route::post('delete', 'HProduksiController@actionDelete');
        });


        Route::group(['prefix' => 'tiket'], function(){
            Route::get('/', 'TiketController@indexList');
            Route::get('new', 'TiketController@indexManage');
            Route::get('update/{post_id}', 'TiketController@indexManage');
            
            Route::post('datatables', 'TiketController@commonList');
            Route::post('save', 'TiketController@actionSave');
            Route::post('delete', 'TiketController@actionDelete');
        });

        Route::group(['prefix' => 'popup'], function(){
            Route::get('/', 'PopupController@indexList');
            Route::get('new', 'PopupController@indexManage');
            Route::get('update/{post_id}', 'PopupController@indexManage');
            
            Route::post('datatables', 'PopupController@commonList');
            Route::post('save', 'PopupController@actionSave');
            Route::post('delete', 'PopupController@actionDelete');
        });


        Route::group(['prefix' => 'halamanproduksi'], function(){
            Route::get('/', 'HProduksiController@indexList');
            Route::get('new', 'HProduksiController@indexManage');
            Route::get('update/{post_id}', 'HProduksiController@indexManage');
            
            Route::post('datatables', 'HProduksiController@commonList');
            Route::post('save', 'HProduksiController@actionSave');
            Route::post('delete', 'HProduksiController@actionDelete');
        });

          Route::group(['prefix' => 'crew'], function(){
            Route::get('/', 'crewController@indexList');
            Route::get('new', 'crewController@indexManage');
            Route::get('update/{post_id}', 'crewController@indexManage');
            
            Route::post('datatables', 'CrewController@commonList');
            Route::post('save', 'CrewController@actionSave');
            Route::post('delete', 'CrewController@actionDelete');
        });

          Route::group(['prefix' => 'kru'], function(){
            Route::get('/', 'KruController@indexList');
            Route::get('new', 'KruController@indexManage');
            Route::get('update/{post_id}', 'KruController@indexManage');
            
            Route::post('datatables', 'KruController@commonList');
            Route::post('save', 'KruController@actionSave');
            Route::post('delete', 'KruController@actionDelete');
        });
           Route::group(['prefix' => 'kruproduksi'], function(){
            Route::get('/', 'KruProduksiController@indexList');
            Route::get('new', 'KruProduksiController@indexManage');
            Route::get('update/{post_id}', 'KruProduksiController@indexManage');
            
            Route::post('datatables', 'KruProduksiController@commonList');
            Route::post('save', 'KruProduksiController@actionSave');
            Route::post('delete', 'KruProduksiController@actionDelete');
        });

        Route::group(['prefix' => 'lihatkru'], function(){
            Route::get('/', 'LihatKruController@indexList');
            Route::get('new', 'LihatKruController@indexManage');
            Route::get('update/{post_id}', 'LihatKruController@indexManage');
            
            Route::post('datatables', 'LihatKruController@commonList');
            Route::post('save', 'LihatKruController@actionSave');
            Route::post('delete', 'LihatKruController@actionDelete');
        });

        Route::group(['prefix' => 'collection'], function(){
            Route::get('/', 'CollectionController@indexList');
            Route::get('new', 'CollectionController@indexManage');
            Route::get('update/{post_id}', 'CollectionController@indexManage');
            
            Route::post('datatables', 'CollectionController@commonList');
            Route::post('datatables2', 'CollectionController@commonList2');
            Route::post('save', 'CollectionController@actionSave');
            Route::post('save2', 'CollectionController@actionSave2');
            Route::post('delete', 'CollectionController@actionDelete');
        });
        Route::group(['prefix' => 'kategori'], function(){
            Route::get('/', 'KategoriController@indexList');
        
            Route::post('datatables', 'KategoriController@commonList');
            Route::post('save', 'KategoriController@actionSave');
            Route::post('delete', 'KategoriController@actionDelete');
        });

        Route::group(['prefix' => 'partner'], function(){
            Route::get('/', 'PartnerController@indexList');
            Route::get('new', 'PartnerController@indexManage');
            Route::get('update/{post_id}', 'PartnerController@indexManage');
            
            Route::post('datatables', 'PartnerController@commonList');
            Route::post('save', 'PartnerController@actionSave');
            Route::post('save2', 'PartnerController@actionSave2');
            Route::post('delete', 'PartnerController@actionDelete');
        });
        Route::group(['prefix' => 'categories'], function(){
            Route::get('/', 'CategoryController@indexList');
            
            Route::post('save', 'CategoryController@actionSave');
            Route::post('delete', 'CategoryController@actionDelete');
        });

        Route::group(['prefix' => 'image'], function(){
            Route::get('/', 'ImageController@indexList');
            
            Route::post('datatables', 'ImageController@commonList');
            Route::post('save', 'ImageController@actionSave');
            Route::post('delete', 'ImageController@actionDelete');
    
            Route::post('uploads', 'ImageController@actionUploadImage');
        });

          Route::group(['prefix' => 'himage'], function(){
            Route::get('/', 'HImageController@indexList');
            Route::post('datatables', 'HImageController@commonList');
            Route::post('datatables2', 'HImageController@commonList2');
            Route::post('save', 'HImageController@actionSave');
            Route::post('save2', 'HImageController@actionSave2');
            Route::post('delete', 'HImageController@actionDelete');
            Route::post('delete2', 'HImageController@actionDelete2');
        });

         Route::group(['prefix' => 'video'], function(){
            Route::get('/', 'VideoController@indexList');
            Route::post('datatables', 'VideoController@commonList');
            Route::post('save', 'VideoController@actionSave');
            Route::post('delete', 'VideoController@actionDelete');
        });

         Route::group(['prefix' => 'hvideo'], function(){
            Route::get('/', 'HVideoController@indexList');
            Route::post('datatables', 'HVideoController@commonList');
            Route::post('save', 'HVideoController@actionSave');
            Route::post('delete', 'HVideoController@actionDelete');
        });

        Route::group(['prefix' => 'files'], function(){
            Route::get('/', 'FileController@indexList');
    
            Route::post('save', 'FileController@actionSave');
            Route::post('delete', 'FileController@actionDelete');
        });

        Route::group(['prefix' => 'setting'], function(){
            Route::get('/', 'SettingController@indexList');
    
            Route::post('save', 'SettingController@actionSave');
        });

        Route::group(['prefix' => 'gallery'], function(){
            Route::get('/', 'GalleryController@indexList');
            Route::get('image/{gallery}', 'GalleryController@indexImageList');
            
            Route::post('datatables', 'GalleryController@commonList');
            Route::post('save', 'GalleryController@actionSave');
            Route::post('delete', 'GalleryController@actionDelete');

            Route::post('image/datatables/{gallery}', 'GalleryController@commonImageList');
            Route::post('image/save', 'GalleryController@actionSaveImage');
            Route::post('image/delete', 'GalleryController@actionDeleteImage');
        });

        Route::group(['prefix' => 'dt/get'], function(){
            Route::post('category', 'CategoryController@commonList');
            Route::post('file', 'FileController@commonList');
            Route::post('setting', 'SettingController@commonList');
        });
    });

});





