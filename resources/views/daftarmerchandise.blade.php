@extends('layout')

@section('content')

<div class="general">
		<div class="container text-center">
		<h3 style="background-color: black; color: white; font-size: 30px;" >MERCHANDISE</h3>
		</div>
		<div class="container">
			<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
				
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<?php $flag = 1; ?>
					@foreach($cmerchandise as $category)
					<li role="presentation" @if($flag==1) class="active" @endif ><a style="color: white;" href="#{{$category->name}}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">{{$category->name}}</a></li>
					<?php $flag++; ?>
					@endforeach
					
				</ul>
				
				<div id="myTabContent" class="tab-content">
					<?php $flag=1;?>
					@foreach($cmerchandise as $category)
					<div role="tabpanel" @if($flag==1)class="tab-pane fade active in" @else class="tab-pane fade"@endif id="{{$category->name}}" aria-labelledby="imdb-tab">
						<?php $flag++; ?>
					@foreach($merchandise as $item)
						@if($item->collection_category_id == $category->collection_category_id)
						<div style="height: 200px;" class="col-md-2 w3l-movie-gride-agile">
							<a href="{{ url('/detailmerchandise/'.$item->collection_id) }}" class="hvr-shutter-out-horizontal"><img style="height: 150px; width: 150px;" src="{{$item->thumbnail_url}}" title="album-name" class="img-responsive" alt=" " />
								
							</a>
							<div class="mid-1 agileits_w3layouts_mid_1_home">
								<div class="w3l-movie-text">
									<h6><a href="single.html">{{$item->name}}</a></h6>				
								</div>
								<div class="mid-2 agile_mid_2_home">
									
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="ribben">
								<p>NEW</p>
							</div>
						</div>
				
						@endif	
						@endforeach	
						<div class="clearfix"> </div>

					</div>

					
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection