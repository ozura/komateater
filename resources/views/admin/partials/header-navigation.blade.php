<nav class="navbar is-black is-primary-color">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{url('cdmanager')}}">
             ADMIN Teater Koma 
        </a>

        <div class="navbar-burger burger" data-show="quickview" data-target="right-menu">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div id="main-navigation" class="navbar-menu is-hidden-mobile">
        <div class="navbar-end">
            <div class="navbar-item">
                <div class="field is-grouped">
                    <p class="control">
                        <a class="button is-white" data-show="quickview" data-target="right-menu">
                            <span class="icon">
                                <i class="typcn typcn-cog"></i>
                            </span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</nav>
