<div id="right-menu" class="quickview">
    <header class="quickview-header">
        <p class="title">Menu</p>
        <span class="delete" data-dismiss="quickview"></span>
    </header>
    <div class="quickview-body">
        <div class="quickview-block">
            <aside class="menu">
                <ul class="menu-list">
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#welcome">
                            <span class="icon">
                                <i class="typcn typcn-home"></i>
                            </span>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#post">
                            <span class="icon">
                                <i class="typcn typcn-news"></i>
                            </span>
                            Post
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#kru">
                            <span class="icon">
                                <i class="typcn typcn-group"></i>
                            </span>
                            Kru
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#produksi">
                            <span class="icon">
                                <i class="typcn typcn-film"></i>
                            </span>
                            Produksi
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#tiket/update/1">
                            <span class="icon">
                                <i class="typcn typcn-ticket"></i>
                            </span>
                            Tiket
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#image">
                            <span class="icon">
                                <i class="typcn typcn-image"></i>
                            </span>
                            Images
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#video">
                            <span class="icon">
                                <i class="typcn typcn-video"></i>
                            </span>
                            Videos
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#collection">
                            <span class="icon">
                                <i class="typcn typcn-th-large-outline"></i>
                            </span>
                            Merchandise
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#gallery">
                            <span class="icon">
                                <i class="typcn typcn-camera-outline"></i>
                            </span>
                            Galleries
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#popup/update/1">
                            <span class="icon">
                                <i class="typcn typcn-image-outline"></i>
                            </span>
                            Popup
                        </a>
                    </li>
                    <li>
                        <a class="target-link" href="cdmanager/dashboard#partner">
                            <span class="icon">
                                <i class="typcn typcn-image-outline"></i>
                            </span>
                            Partner
                        </a>
                    </li>
                </ul>
            </aside>
        </div>
    </div>

    <footer class="quickview-footer">
        <aside class="menu">
            <ul class="menu-list">
                <li>
                    <a class="target-link" href="cdmanager/dashboard#setting">
                        <span class="icon">
                            <i class="typcn typcn-cog"></i>
                        </span>
                        Setting
                    </a>
                </li>
                <li>
                    <a href="{{url('signout')}}">
                        <span class="icon">
                            <i class="typcn typcn-key"></i>
                        </span>
                        Logout
                    </a>
                </li>
            </ul>
        </aside>
    </footer>
</div>