@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a class="button modal-button is-white" data-target="#select_video">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>

<div class="tabs">
    <ul>
        <li class="is-active" data-target="#page1">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-beer" aria-hidden="true"></i>
                </span>
                <span>Video</span>
            </a>
        </li>
        <li data-target="#page2">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-dropbox" aria-hidden="true"></i>
                </span>
                <span>Halaman Video</span>
            </a>
        </li>
    </ul>
</div>

<div id="page1" style="display: block;">
    <div class="content">
        <table class="table is-hoverable is-fullwidth cards" id="primary_table">
            <thead>
                <tr>
                    <th>Video</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="page2" style="display: none;">
        <div class="content">
            <table class="table is-striped is-bordered is-fullwidth" id="secondary_table">
                <thead>
                    <tr>
                        <th>Video</th>
                        
                    </tr>
                </thead>
            </tbody>
        </table>
        </div>
    </div>

<div id="select_video" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow-y: auto;">
        <header class="modal-card-head">
            <p class="modal-card-title">Upload Your Video</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <input type="hidden" name="video_id" value="">
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Video Url</label>
                </div>
                <div class="field-body">
                <div class="field">
                      <div class="control">
                        <input class="input is-primary" name="video_url" type="text" placeholder="Primary input">
                    </div>
                </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Video Name</label>
                </div>
                <div class="field-body">
                <div class="field">
                      <div class="control">
                        <input class="input is-primary" name="video_name" type="text" placeholder="Primary input">
                    </div>
                </div>
                </div>
            </div>
             <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Tampilkan</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="tampilkan">

                                    <option value="1" selected>Ya</option>                                
                                    <option value="0">Tidak</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
          
        </section>
        <footer class="modal-card-foot">
            <button class="button is-link" onclick="saveAction(this)">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>

<script>
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    var file = null;
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'video/datatables',
            type: 'POST'
        },
        columns: [
            { data: 'video', name: 'video', searchable: false, orderable: false, 
                render: function(data) {
                    return '<iframe style="width: 100%; height: 160px;" '+
                    'src="'+data.src+'" frameborder="0" '+
                    'allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" '+
                    'allowfullscreen></iframe>'+
                        '<small class="block-center content-1ellipsis"><b>'+data.alt+'</b></small>';
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<p class="buttons has-addons is-centered">'+
                    '<a class="button is-small is-link is-outlined modal-button" data-target="#select_video" onclick="editActionPopUp(this)" data-id="'+data.id+'" data-content="'+data.content+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-cog"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>'+
                    '<a class="button is-small is-link is-outlined modal-button" onclick="saveAction2(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Tampilkan</span>'+
                    '</a>'+
                    '<a class="button is-small is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a></p>';
                }
            }
        ]
    });


   var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'hvideo/datatables',
            type: 'POST'
        },
        columns: [
            { data: 'video', name: 'video', searchable: false, orderable: false, 
                render: function(data) {
                    return '<iframe style="width: 100%; height: 160px;" '+
                    'src="'+data.src+'" frameborder="0" '+
                    'allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" '+
                    'allowfullscreen></iframe>'+
                        '<small class="block-center content-1ellipsis"><b>'+data.alt+'</b></small>';
                }
            }
           
        ]
    });


    function editActionPopUp(element){
        var item = $(element);
        var id = item.attr('data-id');
        var data = JSON.parse(item.attr('data-content'));
        
        $('input[name=video_url]').val(data.video_url);
        $('input[name=video_name]').val(data.video_name);
        $('select[name=tampilkan]').val(data.tampilkan);
        $('input[name=video_id]').val(id);
        
        
    }

    function saveAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'video/save',
                    data: {
                        video_name: $('input[name=video_name]').val(),
                        video_url : 'https://www.youtube.com/embed/'+getId($('input[name=video_url]').val()),
                         tampilkan: $('select[name=tampilkan]').val(),
                         video_id: $('input[name=video_id]').val(),
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('input[name=video_name]').val('');
                            $('input[name=video_url]').val('');
                                $('select[name=tampilkan]').val(1);
                                $('input[name=video_id]').val('');
                            swal('Good job!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

     function saveAction2(element){
        var item = $(element);
        var id = item.attr('data-id');
        $('input[name=video_id]').val(id);
        
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'hvideo/save',
                    data: {
                        
                        video_id: $('input[name=video_id]').val(),
                        
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('select[name=jenis]').val('1');
                            $('input[name=role]').val('');
                            swal('Good job!', result.message, 'success')
                            
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'video/delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }
     $(document).on('click', '.tabs li', function(){
            var item = $(this);
            var target = item.attr('data-target');
            $('.tabs li').each(function() {
                $(this).removeClass('is-active');
                $($(this).attr('data-target')).hide();
            });

            item.addClass('is-active');
            $(target).show();
        });


</script>