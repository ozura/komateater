@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<div class="content">
    <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
</div>
<div class="content">
    <a class="button is-black is-primary-color new-item-btn">
        <span class="icon is-small">
            <i class="typcn typcn-plus"></i>
        </span>
        <span>New Kategori</span>
    </a>
</div>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Kategori</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<div id="modal-manage-item" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">New/Update Category</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Category Name</label>
                    <input class="input" type="hidden" name="category_id">
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="text" placeholder="Category Name" name="category_name">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-tag"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <div class="field is-grouped is-grouped-right">
                <div class="control">
                    <button class="button is-link save-item-btn is-primary-color">Save</button>
                </div>
                <div class="control">
                    <button class="button is-text">Cancel</button>
                </div>
            </div>
        </footer>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'cdmanager/dt/get/category',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'category_name', name: 'category_name' },
            { data: 'action', name: 'action', searchable: false, orderable: false, 
                render: function(data){
                    if(data.is_editable == 1){
                        return '<a class="button is-small is-rounded is-black is-primary-color edit-item-btn" data-id="'+data.id+'" data-name="'+data.name+'">'+
                        '    <span class="icon is-small">'+
                        '        <i class="typcn typcn-edit"></i>'+
                        '    </span>'+
                        '    <span>Edit</span>'+
                        '</a>'+
                        '<a class="button is-small is-rounded is-danger is-outlined delete-item-btn" data-id="'+data.id+'">'+
                        '    <span class="icon is-small">'+
                        '        <i class="typcn typcn-delete-outline"></i>'+
                        '    </span>'+
                        '    <span>Delete</span>'+
                        '</a>';
                    }else{
                        return '';
                    }
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    $(document).on('click', '.edit-item-btn', function () {
        var item = $(this);
        $('#modal-manage-item').addClass('is-active');
        $('input[name=category_id]').val(item.attr('data-id'));
        $('input[name=category_name]').val(item.attr('data-name'));
    });

    $(document).on('click', '.new-item-btn', function () {
        $('#modal-manage-item').addClass('is-active');
        $('input[name=category_id]').val('');
    });

    $(document).on('click', '.save-item-btn', function () {
        $.ajax({
            type: 'POST',
            url: base_url + 'cdmanager/categories/save',
            data: {
                category_id: $('input[name=category_id]').val(),
                category_name: $('input[name=category_name]').val()
            },
            success: function (result) {
                if(result.status == 200){
                    $('input[name=category_id]').val('');
                    $('input[name=category_name]').val('');
                    iziToast.success({ title: 'OK', message: result.message, position: 'topRight' });
                    primary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                }
            }
        });
    });

    $(document).on('click', '.delete-item-btn', function () {
        var item = $(this);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url + 'cdmanager/categories/delete',
                    data: {
                        category_id: item.attr('data-id')
                    },
                    success: function (result) {
                        if(result.status == 200){
                            iziToast.success({ title: 'OK', message: result.message, position: 'topRight' });
                            primary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }
        });
    });
</script>