@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <div class="buttons">
                <a class="button is-white" onclick="newActionPopUp(this)">
                    <span class="icon is-small">
                        <i class="typcn typcn-plus"></i>
                    </span>
                    <span>New Album</span>
                </a>
                <a class="button is-white" onclick="newImageActionPopUp(this)">
                    <span class="icon is-small">
                        <i class="typcn typcn-plus"></i>
                    </span>
                    <span>Add Image</span>
                </a>
            </div>
        </div>
    </div>
</nav>
<div class="content">
    <table class="table is-hoverable" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Gallery</th>
                <th>Action</th>
            </tr>
        </thead>
        </tbody>
    </table>
</div>
<div id="select-image" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow-y: auto;">
        <header class="modal-card-head">
            <p class="modal-card-title">Select and upload your image</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Select Image</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="file">
                            <label class="file-label">
                                <input class="file-input" type="file" name="image_base" accept=".jpg,.png,.jpeg,.bmg" required>
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="typcn typcn-cloud-storage"></i>
                                    </span>
                                    <span class="file-label" for="image_base">
                                        Choose a file…
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Album</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="gallery_id">
                                    @foreach(\App\Gallery::select('gallery_id', 'gallery_name')->get() as $gallery) 
                                    <option value="{{$gallery->gallery_id}}">{{$gallery->gallery_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <figure class="image">
                    <img src="" style="max-width: 22rem; margin: auto;" id="image-preview">
                </figure>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-link" onclick="saveImageAction(this)">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>
<div id="add-album" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Manage Album</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <input class="input" type="hidden" name="gallery_id">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Name</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input class="input" type="text" name="gallery_name">
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-black is-primary-color" onclick="saveGalleryAction(this)">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>

<script>
    var file = null;
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'gallery/datatables',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'gallery_name', name: 'gallery_name', className: 'has-text-centered imagename' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a class="button is-small is-rounded is-black is-outlined target-link" href="cdmanager/dashboard#gallery/image/'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-eye"></i>'+
                    '    </span>'+
                    '    <span>View</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-link is-outlined" onclick="editActionPopUp(this)" data-id="'+data.id+'" data-gallery="'+data.name+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-pencil"></i>'+
                    '    </span>'+
                    '    <span>Edit Name</span>'+
                    '</a>'+
                    '<a class="button is-small is-danger is-rounded is-outlined" onclick="deleteGalleryAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function newImageActionPopUp(element){
        $('html').addClass('is-clipped');
		$('#select-image').addClass('is-active');
    }
    function newActionPopUp(element){
        $('input[name=gallery_id]').val('');
        $('input[name=gallery_name]').val('');

        $('html').addClass('is-clipped');
		$('#add-album').addClass('is-active');
    }

    function editActionPopUp(element){
        var id = $(element).attr('data-id');
        var name = $(element).attr('data-gallery');
        
        $('input[name=gallery_id]').val(id);
        $('input[name=gallery_name]').val(name);
        $('html').addClass('is-clipped');
		$('#add-album').addClass('is-active');
    }

    function saveGalleryAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'gallery/save',
            data: {
                gallery_id: $('input[name=gallery_id]').val(),
                gallery_name: $('input[name=gallery_name]').val()
            },
            success: function (result) {
                if(result.status == 200){
                    swal('Good job!', result.message, 'success')
                    primary_table.ajax.reload(null, false);
                }else{
                    swal('Oops', result.message, 'error')
                }
            },
            complete: function() {
                item.removeClass('is-loading');
            }
        });
    }
    
    function saveImageAction(element){
        var data = new FormData();
        data.append('files[]', file.files[0]);
        data.append('gallery_id', $('select[name=gallery_id]').val());
        swal({
            onOpen:  () => {
                swal.showLoading()
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'gallery/image/save',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        $('input[name=image_base]').val('');
                        $('input[name="jenis_file"]').val('');
                        $('#image-preview').attr('src', '');
                    },
                    complete: function() {
                        swal.close() 
                        swal('Good job!', 'Upload successfully', 'success')
                    }
                });
            },
            allowOutsideClick: () => !swal.isLoading()
        })
    }

    function deleteGalleryAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'gallery/delete',
                        data: {
                            gallery_id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }

    $(document).on('change', '.file-input', function() {
        file = this;
        var name = $(this).attr('name');
        if(file.files.length > 0){
            $('span[for="'+name+'"]').html(file.files[0].name);
            $('input[name="jenis_file"]').val(name);

            var reader = new FileReader();
            reader.onload = function(e) {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(file.files[0]);
        }
    });
</script>