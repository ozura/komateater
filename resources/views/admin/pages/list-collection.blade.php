@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a class="button modal-button is-white" data-target="#select_category">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Kategori</span>
            </a>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a href="{{url('cdmanager/dashboard#collection/new')}}" class="button is-white target-link">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>

<div class="tabs">
    <ul>
        <li class="is-active" data-target="#page1">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-beer" aria-hidden="true"></i>
                </span>
                <span>Merchandise</span>
            </a>
        </li>
        <li data-target="#page2">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-dropbox" aria-hidden="true"></i>
                </span>
                <span>Kategori</span>
            </a>
        </li>
    </ul>
</div>

<div id="page1" style="display: block;">
        
        <div class="content">
            <table class="table is-hoverable is-fullwidth cards" id="primary_table">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    <div id="page2" style="display: none;">
        <div class="content">
            <table class="table is-striped is-bordered is-fullwidth" id="secondary_table">
                <thead>
                    <tr>
                        <th>Name</th>                        
                        <th>Action</th>

                    </tr>
                </thead>
            </tbody>
        </table>
        </div>
    </div>

</div>


<div id="select_category" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow-y: auto;">
        <header class="modal-card-head">
            <p class="modal-card-title">Tambah Kategori</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <input type="hidden" name="video_id" value="">
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Nama Kategori</label>
                </div>
                <div class="field-body">
                <div class="field">
                      <div class="control">
                        <input class="input is-primary" name="kategori" type="text" placeholder="Primary input">
                    </div>
                </div>
                </div>
            </div>

           
            
          
        </section>
        <footer class="modal-card-foot">
            <button class="button is-link" onclick="saveAction2(this)">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>



<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'collection/datatables',
            type: 'POST'
        },
        columns: [
            { data: 'image', name: 'image', searchable: false, orderable: false, 
                render: function(data) {
                    return '<figure class="image">'+
                        '<img src="'+data.thumbnail+'" alt="'+data.name+'">'+
                        '</figure>'+
                        '<small class="block-center"><b>'+data.category+'</b></small>'+
                        '<small class="block-center content-2ellipsis">'+data.name+'</small>';
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<p class="buttons has-addons is-centered">'+
                    '<a class="button is-small is-rounded is-black target-link" href="{{url('cdmanager/dashboard#collection/update')}}/'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>'+
                    '<a class="button is-small is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a></p>';
                }
            }
        ]
    });


  var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'kategori/datatables',
            type: 'POST'
        },
        columns: [
            
            { data: 'name', name: 'name' },
           
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    
                        return '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction2(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                    
                    
                    
                }
            }
        ]
    });

  
    function addActionPopUp(element){
        var item = $(element);
        $('#modal-manage-item').addClass('is-active');
        $('input[name=collection]').val('');
    }

    function editActionPopUp(element){
        var item = $(element);
        var id = item.attr('data-id');
        var data = JSON.parse(item.attr('data-content'));
        
        $('input[name=collection]').val(id);
        $('select[name=category]').val(data.collection_category_id);
        $('input[name=name]').val(data.collection_name);
        $('input[name=image]').val(data.thumbnail_url);
        $('input[name=redirect]').val(data.target_url);
        $('input[name=order]').val(data.sequence);

        $('#select-image').addClass('is-active');
    }


      function saveAction2(element){
        var item = $(element);
        
        
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'collection/save2',
                    data: {
                        
                        kategori: $('input[name=kategori]').val(),
                        
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('select[name=jenis]').val('1');
                            $('input[name=role]').val('');
                            swal('Good job!', result.message, 'success')
                            
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }



    function saveAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: 'POST',
            url: base_url + admin_url + 'collection/save',
            data: {
                collection: $('input[name=collection]').val(),
                category: $('select[name=category]').val(),
                name: $('input[name=name]').val(),
                image: $('input[name=image]').val(),
                redirect: $('input[name=redirect]').val(),
                order: $('input[name=order]').val()
            },
            success: function (result) {
                if(result.status == 200){
                    $('input[name=collection]').val('');
                    $('select[name=category]').val(1);
                    $('input[name=name]').val('');
                    $('input[name=image]').val('');
                    $('input[name=redirect]').val('');
                    $('input[name=order]').val('0');
                    swal('Good job!', result.message, 'success')
                    primary_table.ajax.reload(null, false);
                }else{
                    swal('Oops', result.message, 'error')
                }
            },
            complete: function() {
                item.removeClass('is-loading');
            }
        });
    }

    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'collection/delete',
                        data: {
                            collection: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }


    function deleteAction2(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'kategori/delete',
                        data: {
                            kategori: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                secondary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }


    //   function newAction(element){
    //     var item = $(element);
    //     $('input[name=kru_id]').val(item.attr('data-id'));
    //     console.log(item.attr('data-id'));  
        
    // }
    $(document).on('click', '.tabs li', function(){
            var item = $(this);
            var target = item.attr('data-target');
            $('.tabs li').each(function() {
                $(this).removeClass('is-active');
                $($(this).attr('data-target')).hide();
            });

            item.addClass('is-active');
            $(target).show();
        });
</script>