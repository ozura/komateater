@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <button class="button is-white modal-button" data-target="#modal-ubah-password">Ubah Password</button>
        </div>
    </div>
</nav>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>Numz</th>
                <th>Page Name</th>
                <th>Content</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
        </thead>
        </tbody>
    </table>
</div>
<div id="modal-manage-item" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Update Content</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Setting Name</label>
                    <input class="input" type="hidden" name="setting_id">
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="text" name="setting_name" value="" >
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-tag"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Setting Content</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <textarea class="textarea" placeholder="Content" name="setting_content"></textarea>
                        <!-- <p class="control has-icons-left is-expanded">
                            <input class="input" type="text" placeholder="Setting Content" name="setting_content">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-tag"></i>
                            </span>
                        </p> -->
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <div class="field is-grouped is-grouped-right">
                <div class="control">
                    <button class="button is-black is-primary-color save-item-btn">Save</button>
                </div>
                <div class="control">
                    <button class="button is-text">Cancel</button>
                </div>
            </div>
        </footer>
    </div>
</div>
<div id="modal-ubah-password" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Ubah Password</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Password Lama</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input class="input" type="password" name="old_password">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Password Baru</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input class="input" type="password" name="new_password">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Ulangi password baru</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input class="input" type="password" name="new_confirm_password">
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <div class="field is-grouped is-grouped-right">
                <div class="control">
                    <button class="button is-black is-primary-color save-ubah-password">Save</button>
                </div>
                <div class="control">
                    <button class="button is-text">Cancel</button>
                </div>
            </div>
        </footer>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'cdmanager/dt/get/setting',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'description', name: 'description' },
            { data: 'setting_content', name: 'setting_content', searchable: false, orderable: false, 
                render: function(data){
                    return '<div style="word-wrap: break-word; width: 15rem;">'+data+'</div>'
                }
            },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-primary-color is-black edit-item-btn" data-id="'+data.id+'" data-name="'+data.name+'" data-content="'+data.content+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $(document).on('click', '.edit-item-btn', function () {
        var item = $(this);
        $('#modal-manage-item').addClass('is-active');
        $('input[name=setting_id]').val(item.attr('data-id'));
        $('input[name=setting_name]').val(item.attr('data-name'));
        $('input[name=setting_content]').val(item.attr('data-content'));
    });

    $(document).on('click', '.new-item-btn', function () {
        $('#modal-manage-item').addClass('is-active');
        $('input[name=setting_id]').val('');
    });

    $(document).on('click', '.save-item-btn', function () {
        $.ajax({
            type: 'POST',
            url: base_url + 'cdmanager/setting/save',
            data: {
                setting_id: $('input[name=setting_id]').val(),
                setting_content: $('textarea[name=setting_content]').val()
            },
            success: function (result) {
                if(result.status == 200){
                    $('input[name=setting_id]').val('');
                    $('input[name=setting_name]').val('');
                    $('textarea[name=setting_content]').val('');
                    iziToast.success({ title: 'OK', message: result.message, position: 'topRight' });
                    primary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                }
            }
        });
    });

    $(document).on('click', '.save-ubah-password', function () {
        $.ajax({
            type: 'POST',
            url: base_url + 'cdmanager/password/change',
            data: {
                old_password: $('input[name=old_password]').val(),
                new_password: $('input[name=new_password]').val(),
                new_confirm_password: $('input[name=new_confirm_password]').val()
            },
            success: function (result) {
                if(result.status == 200){
                    $('input[name=old_password]').val('');
                    $('input[name=new_password]').val('');
                    $('input[name=new_confirm_password]').val('');
                    swal('OK', result.message, 'success');
                }else{
                    swal('Oops', result.message, 'error');
                }
            }
        });
    });
    
</script>