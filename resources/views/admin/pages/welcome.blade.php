@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<div class="columns">
    <div class="column is-8">
    <p class="title">{{end($breadcrumb)->name}}</p>
        Selamat datang di Admin dashboard website Anda, <br>silakan download manual book pengguna <a href="{{url('download/Manual Book PT. PANCA INDAH JAYAMAHE.docx')}}">di sini</a>
    </div>
    <div class="column">
        <p class="title">Manage post terbaru</p>
        <div class="content">
            <table>
                <tbody>
                    @foreach($posts as $post)
                    <tr>
                        <td class="content-1ellipsis"><a class="target-link" href="{{url('cdmanager/dashboard#post/update/'.$post->post_id)}}">{{$post->post_title}}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
{{-- <section class="section hero is-black">
    <div class="columns">
        <div class="column is-6">
            <p class="title">Support By</p>
            <a href="https://www.jagoanhosting.com/" target="_blank">
                <figure class="image">
                    <img src="https://www.jagoanhosting.com/wp-content/uploads/2017/07/JH_agustusan_LOGO-05.png" style="height:40px; width:auto;">
                </figure>
            </a>
            <br>
            <a href="https://www.tawk.to/" target="_blank">
                <figure class="image">
                    <img src="https://www.budgeto.com/wp-content/uploads/2017/02/apps_tawkto_White-420x203.png" style="height:60px; width:auto;">
                </figure>
            </a>
            <br>
            <a href="https://analytics.google.com/" target="_blank">
                <figure class="image">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9ILeRAeTXQ0499PEo-Jl4JqzjF6-jsmMxdRNWX8d0KfmsoLXm" style="height:60px; width:auto;">
                </figure>
            </a>
        </div>
        <div class="column is-6">
            <p class="title">Other Link</p>
            <a href="https://webmail.ptpij.com/" target="_blank">
                <figure class="image">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLElT_82a2CseKNCz9XShngKxyhzp4XW9vcP6axV7B6VSA0ZaB" style="height:60px; width:auto;">
                </figure>
            </a>
        </div>
    </div>
</section> --}}