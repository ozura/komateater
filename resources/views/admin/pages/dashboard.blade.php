@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('admin/app')
@section('content')
@include('admin/partials/header-navigation')
@include('admin/partials/right-menu')
<section class="section">
    <div class="container" id="appcontent">
    </div>
</section>
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $(document).ready(function  () {
        var original_title = location.hash;
        var target_url = original_title.replace('#','');
        if (target_url == '' || target_url == '/' || target_url == 'undefined') {
            loadURI('welcome');
        }else{
            loadURI(target_url);
        }
    });

    $(document).on('click', 'a.target-link', function(e){
        e.preventDefault();
        var item = $(this);
        var target_url = item.attr('href').split('#')[1];
        loadURI(target_url);
    });

    function loadURI(target_url, content) {
        content = typeof content !== 'undefined' ? content : 'appcontent';
        NProgress.start();
        $.ajax({
            type: "GET",
            url: base_url + 'cdmanager/' + target_url,
            contentType: false,
            success: function (data) {
                $("#" + content).html(data);
                location.hash = target_url;

                NProgress.done();
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    }
</script>
@endpush