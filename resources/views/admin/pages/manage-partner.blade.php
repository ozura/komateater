@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a class="button is-white" onclick="addActionPopUp(this)">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>
<div class="content">
    <table class="table is-hoverable is-fullwidth cards" id="primary_table">
        <thead>
            <tr>
                <th>Image</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<div id="modal-manage-item" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Add/Edit Partner</p>
            <button class="delete"></button>
        </header>
        <section class="modal-card-body">
            <input class="input" type="hidden" name="partner">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Name</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="text" name="name">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-tag"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
           
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Image URL</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="text" name="image">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-image"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Redirect</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control has-icons-left is-expanded">
                            <input class="input" type="text" name="redirect">
                            <span class="icon is-small is-left">
                                <i class="typcn typcn-media-fast-forward"></i>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <div class="field is-grouped is-grouped-right">
                <div class="control">
                    <button class="button is-link" onclick="saveAction(this)">Save</button>
                </div>
                <div class="control">
                    <button class="button is-text">Cancel</button>
                </div>
            </div>
        </footer>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'partner/datatables',
            type: 'POST'
        },
        columns: [
            { data: 'image', name: 'image', searchable: false, orderable: false, 
                render: function(data) {
                    return '<figure class="image">'+
                        '<img src="'+data.thumbnail+'" alt="'+data.name+'">'+
                        '</figure>'+
                        
                        '<small class="block-center content-2ellipsis"><b>'+data.name+'</b></small>';
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<p class="buttons has-addons is-centered">'+
                    
                    '<a class="button is-small is-link is-outlined" onclick="editActionPopUp(this)" data-id="'+data.id+'" data-content="'+data.content+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-cog"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>'+
                    '<a class="button is-small is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a></p>';
                }
            }
        ]
    });

    function addActionPopUp(element){
        var item = $(element);
        $('#modal-manage-item').addClass('is-active');
        $('input[name=partner]').val('');
    }

    function editActionPopUp(element){
        var item = $(element);
        var id = item.attr('data-id');
        var data = JSON.parse(item.attr('data-content'));
        
        $('input[name=partner]').val(id);
        
        $('input[name=name]').val(data.name);
        $('input[name=image]').val(data.thumbnail_url);
        $('input[name=redirect]').val(data.target_url);
        

        $('#modal-manage-item').addClass('is-active');
    }

    function saveAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: 'POST',
            url: base_url + admin_url + 'partner/save',
            data: {
                partner: $('input[name=partner]').val(),
                name: $('input[name=name]').val(),
                image: $('input[name=image]').val(),
                redirect: $('input[name=redirect]').val(),

                
            },
            success: function (result) {
                if(result.status == 200){
                    $('input[name=collection]').val('');
                    $('select[name=category]').val(1);
                    $('input[name=name]').val('');
                    $('input[name=image]').val('');
                    $('input[name=redirect]').val('');
                    $('input[name=order]').val('0');
                    swal('Good job!', result.message, 'success')
                    primary_table.ajax.reload(null, false);
                }else{
                    swal('Oops', result.message, 'error')
                }
            },
            complete: function() {
                item.removeClass('is-loading');
            }
        });
    }

    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'partner/delete',
                        data: {
                            partner: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }
</script>