@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a class="button is-white modal-button" data-target="#add-file-modal">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>
<div class="content">
    <table class="table is-hoverable is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Document</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<div id="add-file-modal" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow-y: auto;">
        <header class="modal-card-head">
            <p class="modal-card-title">Select and upload your document</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Title</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input class="input" type="text" placeholder="Document Title" name="file_name" required>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Select File</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="file">
                            <label class="file-label">
                                <input class="file-input" type="file" name="file_base" accept=".pdf,.doc,.docx,.xlsx" required>
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="typcn typcn-cloud-storage"></i>
                                    </span>
                                    <span class="file-label" for="file_base">
                                        Choose a file…
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-success save-item-btn is-primary-color">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>

<script>
    var file = null;
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'cdmanager/dt/get/file',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'file_name', name: 'file_name' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a class="button is-small is-black is-primary-color is-rounded" href="'+data.url+'" target="_blank">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-download-outline"></i>'+
                    '    </span>'+
                    '    <span>Download</span>'+
                    '</a>'+
                    '<a class="button is-small is-danger is-outlined delete-item-btn is-rounded" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    $(document).on('change', '.file-input', function() {
        file = this;
        var name = $(this).attr('name');
        if(file.files.length > 0){
            $('span[for="'+name+'"]').html(file.files[0].name);

            var reader = new FileReader();
            reader.onload = function(e) {
            }
            reader.readAsDataURL(file.files[0]);
        }
    });

    $(document).on('click', '.save-item-btn', function(){
        var data = new FormData();
        data.append('file_base', file.files[0]);
        data.append('file_name', $('input[name=file_name]').val());
        $.ajax({
            type: "POST",
            url: base_url + 'cdmanager/files/save',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                if(result.status == 200){
                    $('input[name=file_name]').val('');
                    $('input[name=file_base]').val('');
                    iziToast.success({ title: 'Good Job', message: result.message });
                    primary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message });
                }
            }
        });
    });

    $(document).on('click', '.delete-item-btn', function(){
        var item = $(this);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url + 'cdmanager/files/delete',
                    async: false,
                    data: {
                        file_id: item.attr('data-id')
                    },
                    success: function (result) {
                        if(result.status == 200){
                            iziToast.success({ title: 'Good Job', message: result.message });
                            primary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message });
                        }
                    },
                    error: function () {
                        iziToast.warning({ title: 'Oops', message: 'Operation error!' });
                    }
                });
            }
        });
    });
</script>