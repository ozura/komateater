@include('partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<div class="content">
    <nav class="level">
        <div class="level-left">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>

        @if($mutasi_barang->STATUS_MUTASI_BARANG <= 2)
        <div class="level-right">
            <div class="field is-grouped">
                <p class="control">
                    <button class="button is-black is-primary-color" onclick="sendAction(this)">
                        Pendataan Selesai dan kirim barang
                    </button>
                </p>
            </div>
        </div>
        @endif
    </nav>
</div>
@if($mutasi_barang->STATUS_MUTASI_BARANG <= 2)
<div class="tabs">
    <ul>
        <li class="is-active" data-target="#page1">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-beer" aria-hidden="true"></i>
                </span>
                <span>Data Barang</span>
            </a>
        </li>
        <li data-target="#page2">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-dropbox" aria-hidden="true"></i>
                </span>
                <span>Sudah dipilih</span>
            </a>
        </li>
    </ul>
</div>
<div id="page1" style="display: block;">
@else
<div id="page1" style="display: none;">
@endif
    <div class="content">
        <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Brand</th>
                    <th>Jenis</th>
                    <th>Ukuran</th>
                    <th>Harga</th>
                    <th>Tersedia</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@if($mutasi_barang->STATUS_MUTASI_BARANG <= 2)
<div id="page2" style="display: none;">
@else
<div id="page2" style="display: block;">
@endif
    <div class="content">
        <table class="table is-striped is-bordered is-fullwidth" style="width:100% !important;" id="secondary_table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Brand</th>
                    <th>Jenis</th>
                    <th>Ukuran</th>
                    <th>Harga</th>
                    <th>QTY</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/get/mutasi/barang/all?brand={{$mutasi_barang->ID_BRAND}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'KODE_BARANG', name: 'KODE_BARANG' },
            { data: 'NM_BRAND', name: 'NM_BRAND', searchable: false, orderable: false, },
            { data: 'NM_JENIS_BARANG', name: 'NM_JENIS_BARANG', searchable: false, orderable: false, },
            { data: 'NM_SIZE_DETAIL', name: 'NM_SIZE_DETAIL', searchable: false, orderable: false, },
            { data: 'HARGA_MASUK_BARANG', name: 'HARGA_MASUK_BARANG', searchable: false, orderable: false, },
            { data: 'STOK', name: 'STOK', searchable: false, orderable: false, },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<input class="input" style="width:48px; margin-right:2rem;" min="0" type="number" value="0" id="mutasi-'+data.id+'-'+data.size+'">'+
                    '<a class="button is-small is-rounded is-black is-primary-color" onclick="addAction(this)" data-id="'+data.id+'" data-size="'+data.size+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-plus"></i>'+
                    '    </span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + 'api/get/mutasi/barang/check/{{$mutasi_barang->ID_MUTASI_BARANG}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'KODE_BARANG', name: 'KODE_BARANG' },
            { data: 'NM_BRAND', name: 'NM_BRAND', searchable: false, orderable: false, },
            { data: 'NM_JENIS_BARANG', name: 'NM_JENIS_BARANG', searchable: false, orderable: false, },
            { data: 'NM_SIZE_DETAIL', name: 'NM_SIZE_DETAIL', searchable: false, orderable: false, },
            { data: 'HARGA_MASUK_BARANG', name: 'HARGA_MASUK_BARANG', searchable: false, orderable: false, },
            { data: 'QTY', name: 'QTY', searchable: false, orderable: false, },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    @if($mutasi_barang->STATUS_MUTASI_BARANG <= 2)
                    return '<a class="button is-small is-rounded is-danger" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-times"></i>'+
                    '    </span>'+
                    '</a>';
                    @else
                    return '';
                    @endif
                }
            }
        ]
    });

    secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function addAction(element){
        var item = $(element);

        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            async: false,
            url: base_url + 'api/mutasi/add',
            data: {
                ID_MUTASI_BARANG: {{$mutasi_barang->ID_MUTASI_BARANG}},
                ID_BARANG: item.attr('data-id'),
                ID_SIZE_DETAIL: item.attr('data-size'),
                QTY: $('#mutasi-'+item.attr('data-id')+'-'+item.attr('data-size')).val()
            },
            success: function (result) {
                item.removeClass('is-loading');
                if(result.status == 200){
                    iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                    primary_table.ajax.reload(null, false);
                    secondary_table.ajax.reload(null, false);
                }else{
                    iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                }
            }
        });
    };

    function deleteAction(element){
        var item = $(element);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: base_url + 'api/mutasi/delete',
                    data: {
                        ID_MUTASI_BARANG_DETAIL: item.attr('data-id')
                    },
                    success: function (result) {
                        if(result.status == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }
        });
    };

    function sendAction(element){
        var item = $(element);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: base_url + 'api/mutasi/send',
                    data: {
                        ID_MUTASI_BARANG: {{$mutasi_barang->ID_MUTASI_BARANG}}
                    },
                    success: function (result) {
                        if(result.status == 200){
                            iziToast.success({ title: 'Good Job', message: result.message, position: 'topRight' });
                            loadURI('mutasi');
                        }else{
                            iziToast.warning({ title: 'Oops', message: result.message, position: 'topRight' });
                        }
                    }
                });
            }
        });
    };

    $(document).on('click', '.tabs li', function(){
        var item = $(this);
        var target = item.attr('data-target');
        $('.tabs li').each(function() {
            $(this).removeClass('is-active');
            $($(this).attr('data-target')).hide();
        });

        item.addClass('is-active');
        $(target).show();
    });
</script>