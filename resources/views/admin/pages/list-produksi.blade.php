@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<input type="hidden" name="produksi_id" value="">
<input type="hidden" name="hproduksi_id" value="">
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a href="{{url('cdmanager/dashboard#produksi/new')}}" class="button is-white target-link">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>
<div class="tabs">
    <ul>
        <li class="is-active" data-target="#page1">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-beer" aria-hidden="true"></i>
                </span>
                <span>Produksi</span>
            </a>
        </li>
        <li data-target="#page2">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-dropbox" aria-hidden="true"></i>
                </span>
                <span>Halaman Produksi</span>
            </a>
        </li>
    </ul>
</div>

<div id="page1" style="display: block;">
        <div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No0</th>
                <th>Judul</th>
                <th>Tempat</th>
                <th>Action</th>
            </tr>
        </thead>
        </tbody>
    </table>
    </div>
    </div>


    <div id="page2" style="display: none;">
        <div class="content">
            <table class="table is-striped is-bordered is-fullwidth" id="secondary_table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Action</th>

                    </tr>
                </thead>
            </tbody>
        </table>
        </div>
    </div>

</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'produksi/datatables',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'judul_lakon', name: 'judul_lakon' },
            { data: 'tempat_pentas', name: 'tempat_pentas' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-black target-link" href="{{url('cdmanager/dashboard#produksi/update')}}/'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-black target-link" href="{{url('cdmanager/dashboard#kruproduksi')}}?produksi_id='+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Tambah Crew</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="saveAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Tampilkan</span>'+
                    '</a>';   
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();



    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'hproduksi/datatables',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'judul_lakon', name: 'judul_lakon' },
           
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    
                        return '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction2(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                    
                    
                    
                }
            }
        ]
    });
    secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();
    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'produksi/delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }

    function deleteAction2(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'hproduksi/delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                secondary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }
    function saveAction(element){
        var item = $(element);
        var id = item.attr('data-id');
        $('input[name=produksi_id]').val(id);
        $('input[name=hproduksi_id]').val(id);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'hproduksi/save',
                    data: {
                        
                        produksi_id: $('input[name=produksi_id]').val(),
                        hproduksi_id: $('input[name=hproduksi_id]').val(),
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('select[name=jenis]').val('1');
                            $('input[name=role]').val('');
                            swal('Good job!', result.message, 'success')
                            
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


         $(document).on('click', '.tabs li', function(){
            var item = $(this);
            var target = item.attr('data-target');
            $('.tabs li').each(function() {
                $(this).removeClass('is-active');
                $($(this).attr('data-target')).hide();
            });

            item.addClass('is-active');
            $(target).show();
        });



    
</script>