@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a class="button modal-button is-white" data-target="#select-image">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>

<div class="tabs">
    <ul>
        <li class="is-active" data-target="#page1">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-beer" aria-hidden="true"></i>
                </span>
                <span>Images</span>
            </a>
        </li>
        <li data-target="#page2">
            <a>
                <span class="icon is-small">
                    <i class="typcn typcn-dropbox" aria-hidden="true"></i>
                </span>
                <span>Slideshow</span>
            </a>
        </li>
       
    </ul>
</div>

<div id="page1" style="display: block;">
    
    <div class="content">
        <table class="table is-hoverable is-fullwidth cards" id="primary_table">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="page2" style="display: none;">
        <div class="content">
            <table class="table is-striped is-bordered is-fullwidth" id="secondary_table">
                <thead>
                    <tr>
                        <th>Image</th>
                                
                    </tr>
                </thead>
            </tbody>
        </table>
        </div>
    </div>
<div id="page3" style="display: none;">
        <div class="content">
            <table class="table is-striped is-bordered is-fullwidth" id="tertiary_table">
                <thead>
                    <tr>
                        <th>Image</th>
                                
                    </tr>
                </thead>
            </tbody>
        </table>
        </div>
    </div>


<div id="select-image" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow-y: auto;">
        <header class="modal-card-head">
            <p class="modal-card-title">Select and upload your image</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Select Image</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="file">
                            <label class="file-label">
                                <input class="file-input" type="file" name="image_base" accept=".jpg,.png,.jpeg,.bmg" required>
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="typcn typcn-cloud-storage"></i>
                                    </span>
                                    <span class="file-label" for="image_base">
                                        Choose a file…
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <figure class="image">
                    <img src="" style="max-width: 22rem; margin: auto;" id="image-preview">
                </figure>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-link" onclick="saveAction(this)">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>

<div class="modal" id="zoom-image">
    <div class="modal-background"></div>
    <div class="modal-content">
        <p class="image">
            <img src="https://bulma.io/images/placeholders/1280x960.png">
        </p>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>

<script>
    var file = null;
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'image/datatables',
            type: 'POST'
        },
        columns: [
            { data: 'image', name: 'image', searchable: false, orderable: false, 
                render: function(data) {
                    return '<figure class="image" style="margin: 0;">'+
                        '<img src="'+data.src+'" alt="'+data.alt+'" data-origin="'+data.original+'" class="zoom-image-btn modal-button" data-target="#zoom-image">'+
                        '</figure>'+
                        '<small class="block-center"><b>Upload date:</b> '+data.date+'</small>'+
                        '<small class="block-center content-1ellipsis"><b>'+data.alt+'</b></small>';
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a class="button is-small is-danger is-outlined" onclick="deleteAction(this)" style="padding: 0; width: 100%;" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>'+
                    '<a class="button is-small is-link is-outlined modal-button" onclick="saveAction2(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Slideshow</span>'+
                    '</a>';
                }
            }
        ]
    });

    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'himage/datatables',
            type: 'POST'
        },
        columns: [
            { data: 'image', name: 'image', searchable: false, orderable: false, 
                render: function(data) {
                     return '<figure class="image" style="margin: 0;">'+
                        '<img style="height:200px;" src="'+data.src+'" alt="'+data.alt+'" data-origin="'+data.original+'" class="zoom-image-btn modal-button" data-target="#zoom-image">'+
                        '</figure>'+
                       '<a class="button is-small is-danger is-outlined" onclick="deleteAction2(this)" style="padding: 0; width: 100%;" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                }
            }
           
        ]
    });

    var tertiary_table = $('#tertiary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'himage/datatables2',
            type: 'POST'
        },
        columns: [
            { data: 'image', name: 'image', searchable: false, orderable: false, 
                render: function(data) {
                     return '<figure class="image" style="margin: 0;">'+
                        '<img style="height:200px;" src="'+data.src+'" alt="'+data.alt+'" data-origin="'+data.original+'" class="zoom-image-btn modal-button" data-target="#zoom-image">'+
                        '</figure>'+
                       '<a class="button is-small is-danger is-outlined" onclick="deleteAction3(this)" style="padding: 0; width: 100%;" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                }
            }
           
        ]
    });


    function saveAction(element){
        var data = new FormData();
        data.append('files[]', file.files[0]);
        swal({
            onOpen:  () => {
                swal.showLoading()
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'image/save',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        $('input[name=image_base]').val('');
                        $('input[name="jenis_file"]').val('');
                        $('#image-preview').attr('src', '');
                        primary_table.ajax.reload(null, false);
                    },
                    complete: function() {
                        swal.close() 
                        swal('Good job!', 'Upload successfully', 'success')
                    }
                });
            },
            allowOutsideClick: () => !swal.isLoading()
        })
    }

    function saveAction2(element){
        var item = $(element);
        var id = item.attr('data-id');
        $('input[name=image_id]').val(id);
        
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'himage/save',
                    data: {
                        
                         image_id: item.attr('data-id')
                        
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('select[name=jenis]').val('1');
                            $('input[name=role]').val('');
                            swal('Good job!', result.message, 'success')
                            
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

    function saveAction3(element){
        var item = $(element);
        var id = item.attr('data-id');
        $('input[name=image_id]').val(id);
        
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'himage/save2',
                    data: {
                        
                         image_id: item.attr('data-id')
                        
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('select[name=jenis]').val('1');
                            $('input[name=role]').val('');
                            swal('Good job!', result.message, 'success')
                            
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'image/delete',
                        data: {
                            image_id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }


     function deleteAction2(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'himage/delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                secondary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }

     function deleteAction3(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'himage/delete2',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                tertiary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }

    $(document).on('change', '.file-input', function() {
        file = this;
        var name = $(this).attr('name');
        if(file.files.length > 0){
            $('span[for="'+name+'"]').html(file.files[0].name);
            $('input[name="jenis_file"]').val(name);

            var reader = new FileReader();
            reader.onload = function(e) {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(file.files[0]);
        }
    });

    $(document).on('click', '.zoom-image-btn', function(){
        var item = $(this);
        var source = item.attr('data-origin');

        $('#zoom-image img').attr('src', source);
    });
     $(document).on('click', '.tabs li', function(){
            var item = $(this);
            var target = item.attr('data-target');
            $('.tabs li').each(function() {
                $(this).removeClass('is-active');
                $($(this).attr('data-target')).hide();
            });

            item.addClass('is-active');
            $(target).show();
        });


 </script>