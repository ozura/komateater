@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a href="{{url('cdmanager/dashboard#post/new')}}" class="button is-white target-link">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Title</th>
                <th>Category</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
        </thead>
        </tbody>
    </table>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'post/datatables',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'post_title', name: 'post_title' },
            { data: 'category_name', name: 'category_name' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-black target-link" href="{{url('cdmanager/dashboard#post/update')}}/'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'post/delete',
                        data: {
                            post: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }
</script>