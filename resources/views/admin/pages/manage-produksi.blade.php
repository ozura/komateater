@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level">
    <div class="level-left">
        <div class="level-item">
            <p class="title">
                <strong>{{end($breadcrumb)->name}}</strong>
            </p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <div class="box">
                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">Edited date: <b>{{Carbon\Carbon::now()->format('j M Y')}}</b></label>
                    </div>
                    <div class="field body is-grouped is-grouped-right">
                        <div class="control">
                            <button class="button is-link is-primary-color" onclick="saveAction(this)">
                                <span class="icon">
                                    <i class="typcn typcn-folder-add"></i>
                                </span>
                                <span>Save</span>
                            </button>
                        </div>
                        <div class="control">
                            <a href="{{url('cdmanager/dashboard#produksi')}}" class="target-link">
                                <button class="button is-text">Back</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Judul Produksi</label>
        </div>
        <div class="field-body">
            <div class="field">
                <p class="control">
                    @if(!empty($item))
                    <input class="input" type="hidden" name="produksi_id" value="{{$item->produksi_id}}">
                    <input class="input" name="judul_lakon" type="text" placeholder="Judul" value="{{$item->judul_lakon}}">
                    
                    <!-- <input class="input" type="text" placeholder="Post Title" name="post_title" value="{{$item->judul_lakon}}">  -->
                    @else
                    <input class="input" type="hidden" name="produksi_id">
                    <input class="input" name="judul_lakon" type="text" placeholder="Judul" >
                    
                    <!-- <input class="input" type="text" placeholder="Post Title" name="post_title">  -->
                    @endif
                </p>
            </div>
        </div>
    </div>
   {{--  @if(!empty($item))
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">View Post</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    <a class="button is-black" target="_blank" href="{{url('post/'.$item->produksi_url)}}">
                        <span class="icon">
                            <i class="typcn typcn-eye"></i>
                        </span>
                        <span>View</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif --}}
   
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Gambar Produksi</label>
        </div>
        <div class="field-body">
            <div class="box">
                <figure>
                    @if(!empty($item) && !empty($item->foto))
                    <img id="feature_image" style="height: 16rem;" src="{{$item->foto}}"> 
                    @else
                    <img id="feature_image" style="height: 16rem;" src="http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/256/user-2-icon.png"> 
                    @endif
                    <figcaption class="has-text-centered">
                        <a class="set-image-btn" onclick="selectedPopUp(this)" data-target="#select-image">Set Feature Image</a>
                    </figcaption>
                </figure>
                @if(!empty($item))
                <input class="input" type="hidden" name="foto" value="{{$item->foto}}"> 
                @else
                <input class="input" type="hidden" name="foto" value="http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/256/user-2-icon.png" > 
                @endif
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
            <div class="field-label is-small">
                <label class="label">Video Url</label>
            </div>
            <div class="field-body">
                <div class="field">
                  <div class="control">
                    @if(!empty($item->video) && $item->video != 'https://www.youtube.com/embed/error'  )
                    <input class="input" name="video_url" type="text" placeholder="Primary input" value="{{$item->video}}">
                    @else
                    <input class="input" name="video_url" type="text" placeholder="Primary input">
                    @endif
                </div>
            </div>
        </div>
    </div>
     <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Album</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="gallery_id">
                                    @if(!empty($item->gallery_id))
                                    
                                    @foreach(\App\Gallery::select('gallery_id', 'gallery_name')->get() as $gallery) 
                                    <?php $selected = ''; ?>
                                    @if($gallery->gallery_id == $item->gallery_id) 
                                    {
                                       <?php  $selected = 'selected="selected"'; ?>
                                    }
                                    @endif
                                    <option value="{{$gallery->gallery_id}}" {{$selected}}>{{$gallery->gallery_name}}</option>
                                    @endforeach
                                    @else
                                     <option disabled selected value> -- Select Album -- </option>
                                    @foreach(\App\Gallery::select('gallery_id', 'gallery_name')->get() as $gallery) 
                                   
                                    <option value="{{$gallery->gallery_id}}">{{$gallery->gallery_name}}</option>
                                    @endforeach
                                    @endif


                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    {{-- <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Tersedia Tiket</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="tiket">

                                    <option value="1" selected>Ya</option>                                
                                    <option value="0">Tidak</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
    </div> --}}
    {{--   <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Tiket</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    @if(!empty($item))
                    <div id="item-tiket">{!!$item->tiket!!}</div>
                    @else
                    <div id="item-tiket"></div>
                    @endif
                </div>
            </div>
        </div>
    </div> --}}
    {{--  <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Tampilkan Produksi</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="tampilkan">

                                    <option value="1" selected>Ya</option>                                
                                    <option value="0">Tidak</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
    </div> --}}
   
    <div class="field is-horizontal">
            <div class="field-label is-small">
                <label class="label">Awal Pentas</label>
            </div>
            <div class="field-body">
                <div class="field">
                  <div class="control">
                    @if(!empty($item))
                    <input class="input" name="awal" type="date" placeholder="Waktu Pentas" value="{{$awalpentas}}"> 
                    @else
                    <input class="input" name="awal" type="date" placeholder="Waktu Pentas">
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="field is-horizontal">
            <div class="field-label is-small">
                <label class="label">Akhir Pentas</label>
            </div>
            <div class="field-body">
                <div class="field">
                  <div class="control">
                    @if(!empty($item))
                    <input class="input" name="akhir" type="date" placeholder="Waktu Pentas" value="{{$akhirpentas}}"> 
                    @else
                    <input class="input" name="akhir" type="date" placeholder="Waktu Pentas">
                    @endif
                </div>
            </div>
        </div>
    </div>

  
    <div class="field is-horizontal">
            <div class="field-label is-small">
                <label class="label">Tempat Pentas</label>
            </div>
            <div class="field-body">
                <div class="field">
                  <div class="control">
                    @if(!empty($item))
                    <input class="input" name="tempat" type="text" placeholder="Tempat Pentas" value="{{$item->tempat_pentas}}">
                    @else
                    <input class="input" name="tempat" type="text" placeholder="Tempat Pentas">
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Sinopsis</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    @if(!empty($item))
                    <div id="item-content">{!!$item->sinopsis!!}</div>
                    @else
                    <div id="item-content"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div id="select-image" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Select your image</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Image URL</label>
                </div>
                <div class="field-body">
                    <div class="field has-addons">
                        <p class="control">
                            <input class="input" type="text" name="image_selected_url" >
                        </p>
                        <div class="control">
                            <button class="button is-link" onclick="selectedAction(this)">
                                <span class="icon">
                                    <i class="typcn typcn-folder-add"></i>
                                </span>
                                <span>Save</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns is-multiline" id="select-image-content">
            </div>
        </section>
    </div>
</div>

<script>
    $('#item-content').summernote({
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], '#item-content');
            }
        }
    });
     $('#item-tiket').summernote({
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], '#item-content');
            }
        }
    });


    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    function saveAction(element) {
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'produksi/save',
            data: {
                produksi_id: $('input[name=produksi_id]').val(),
                judul_lakon: $('input[name=judul_lakon]').val(),
                foto: $('input[name=foto]').val(),
                video_url : 'https://www.youtube.com/embed/'+getId($('input[name=video_url]').val()),
                awal: $('input[name=awal]').val(),
                akhir: $('input[name=akhir]').val(),
                tempat: $('input[name=tempat]').val(),
                gallery_id: $('select[name=gallery_id]').val(),
                sinopsis: $('#item-content').summernote('code')

            },
            success: function (result) {
                if(result.status == 200){
                    swal('Good job!', result.message, 'success')
                    if(result.redirect){
                        loadURI('produksi');
                    }
                }else{
                    swal('Oops', result.message, 'error')
                }
            },
            complete: function() {
                item.removeClass('is-loading');
            }
        });
    };

    function selectedPopUp (element){
        $('html').addClass('is-clipped');
        $('#select-image').addClass('is-active');
        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'image/datatables',
            success: function (result) {
                $('#select-image-content').html('');
                $.each(result.data, function(i, item) {
                    var image = result.data[i].image;
                    var html = '<div class="column is-4">'+
                    '<figure class="image">'+
                    '<img class="image-selected" style="cursor: pointer;" src="'+image.src+'" alt="'+image.alt+'" data-original="'+image.original+'">'+
                    '</figure>'+
                    '</div>';
                    $('#select-image-content').append(html);
                })
            }
        });
    }

    function selectedAction(element){
        var item = $(element);
        var data = $('input[name=image_selected_url]').val();

        $('#feature_image').attr('src', data);
        $('input[name=foto]').val(data);
        $('html').removeClass('is-clipped');
        $('#select-image').removeClass('is-active');
    }

    $(document).on('click', '.image-selected', function () {
        var item = $(this);
        $('input[name=image_selected_url]').val(item.attr('data-original'));
    });
</script>