@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            
        </div>
    </div>
</nav>
<div id="select-image" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow-y: auto;">
        <header class="modal-card-head">
            <p class="modal-card-title">Tambahkan...</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Jenis</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="jenis">

                                    <option value="1" selected>pemain</option>                                
                                    <option value="2">pekerja</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Role</label>
                </div>
                <div class="field-body">
                <div class="field">
                      <div class="control">
                        <input class="input is-primary" name="role" type="text" placeholder="Primary input">
                    </div>
                </div>
                </div>
            </div>
          
        </section>


        <input type="hidden" name="kru_id" >



        <footer class="modal-card-foot">
            <button class="button is-link" onclick="saveAction(this)">Save changes</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>
<div class="content">
    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Role</th>
                <th>Action</th>

            </tr>
        </thead>
        </tbody>
    </table>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'kruproduksi/datatables?produksi_id={{$produksi_id}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'nama', name: 'nama' },
            { data: 'jenistext', name: 'jenistext' },
            { data: 'roletext', name: 'roletext' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    if(data.produksi_id==null){
                        return  '<a class="button modal-button is-black" data-target="#select-image" onclick="newAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '    <span>Tambah</span>'+
                    '</a>';                        
                    }else{
                        return '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>';
                    }
                    
                    
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();


    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'kruproduksi/delete',
                        data: {
                            kru_id: item.attr('data-id'),
                            produksi_id: {{$produksi_id}}
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }


      function newAction(element){
        var item = $(element);
        $('input[name=kru_id]').val(item.attr('data-id'));
        console.log(item.attr('data-id'));  
        
    }

    function saveAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "POST",
                    url: base_url + admin_url + 'kruproduksi/save',
                    data: {
                        kru_id: $('input[name=kru_id]').val(),
                        produksi_id: {{$produksi_id}},
                        jenis: $('select[name=jenis]').val(),
                        role : $('input[name=role]').val()
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('select[name=jenis]').val('1');
                            $('input[name=role]').val('');
                            swal('Good job!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

    
</script>