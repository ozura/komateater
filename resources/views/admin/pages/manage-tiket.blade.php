@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level">
    <div class="level-left">
        <div class="level-item">
            <p class="title">
                <strong>{{end($breadcrumb)->name}}</strong>
            </p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <div class="box">
                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">Edited date: <b>{{Carbon\Carbon::now()->format('j M Y')}}</b></label>
                    </div>
                    <div class="field body is-grouped is-grouped-right">
                        <div class="control">
                            <button class="button is-link is-primary-color" onclick="saveAction(this)">
                                <span class="icon">
                                    <i class="typcn typcn-folder-add"></i>
                                </span>
                                <span>Save</span>
                            </button>
                        </div>
                        <div class="control">
                            {{-- <a href="{{url('cdmanager/dashboard#tiket')}}" class="target-link">
                                <button class="button is-text">Back</button>
                            </a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Judul tiket</label>
        </div>
        <div class="field-body">
            <div class="field">
                <p class="control">
                    @if(!empty($item))
                    <input class="input" type="hidden" name="tiket_id" value="{{$item->tiket_id}}">
                    <input class="input" name="nama" type="text" placeholder="Judul" value="{{$item->nama}}">
                    
                    
                    @else
                    <input class="input" type="hidden" name="tiket_id">
                    <input class="input" name="nama" type="text" placeholder="Judul" >
                    
                    <!-- <input class="input" type="text" placeholder="Post Title" name="post_title">  -->
                    @endif
                </p>
            </div>
        </div>
    </div>
   {{--  @if(!empty($item))
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">View Post</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    <a class="button is-black" target="_blank" href="{{url('post/'.$item->tiket_url)}}">
                        <span class="icon">
                            <i class="typcn typcn-eye"></i>
                        </span>
                        <span>View</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif --}}
   
    
    
    <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Tampilkan Tiket</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <div class="select">
                                <select name="tampilkan">

                                    <option value="1" selected>Ya</option>                                
                                    <option value="0">Tidak</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
      <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Tiket</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    @if(!empty($item))
                    <div id="item-tiket">{!!$item->tiket!!}</div>
                    @else
                    <div id="item-tiket"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
   
  
 
</div>

<div id="select-image" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Select your image</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Image URL</label>
                </div>
                <div class="field-body">
                    <div class="field has-addons">
                        <p class="control">
                            <input class="input" type="text" name="image_selected_url" >
                        </p>
                        <div class="control">
                            <button class="button is-link" onclick="selectedAction(this)">
                                <span class="icon">
                                    <i class="typcn typcn-folder-add"></i>
                                </span>
                                <span>Save</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns is-multiline" id="select-image-content">
            </div>
        </section>
    </div>
</div>

<script>
 
     $('#item-tiket').summernote({
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], '#item-content');
            }
        }
    });

    function saveAction(element) {
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'tiket/save',
            data: {
                tiket_id: $('input[name=tiket_id]').val(),
                nama: $('input[name=nama]').val(),
                tampilkan: $('select[name=tampilkan]').val(),
                tiket: $('#item-tiket').summernote('code')
                


            },
            success: function (result) {
                if(result.status == 200){
                    swal('Good job!', result.message, 'success')
                    if(result.redirect){
                        loadURI('tiket');
                    }
                }else{
                    swal('Oops', result.message, 'error')
                }
            },
            complete: function() {
                item.removeClass('is-loading');
            }
        });
    };

    function selectedPopUp (element){
        $('html').addClass('is-clipped');
        $('#select-image').addClass('is-active');
        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'image/datatables',
            success: function (result) {
                $('#select-image-content').html('');
                $.each(result.data, function(i, item) {
                    var image = result.data[i].image;
                    var html = '<div class="column is-4">'+
                    '<figure class="image">'+
                    '<img class="image-selected" style="cursor: pointer;" src="'+image.src+'" alt="'+image.alt+'" data-original="'+image.original+'">'+
                    '</figure>'+
                    '</div>';
                    $('#select-image-content').append(html);
                })
            }
        });
    }

    function selectedAction(element){
        var item = $(element);
        var data = $('input[name=image_selected_url]').val();

        $('#feature_image').attr('src', data);
        $('input[name=foto]').val(data);
        $('html').removeClass('is-clipped');
        $('#select-image').removeClass('is-active');
    }

    $(document).on('click', '.image-selected', function () {
        var item = $(this);
        $('input[name=image_selected_url]').val(item.attr('data-original'));
    });
</script>