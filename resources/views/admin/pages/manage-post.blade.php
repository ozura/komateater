@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level">
    <div class="level-left">
        <div class="level-item">
            <p class="title">
                <strong>{{end($breadcrumb)->name}}</strong>
            </p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <div class="box">
                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">Edited date: <b>{{Carbon\Carbon::now()->format('j M Y')}}</b></label>
                    </div>
                    <div class="field body is-grouped is-grouped-right">
                        <div class="control">
                            <button class="button is-link is-primary-color" onclick="saveAction(this)">
                                <span class="icon">
                                    <i class="typcn typcn-folder-add"></i>
                                </span>
                                <span>Save</span>
                            </button>
                        </div>
                        <div class="control">
                            <a href="{{url('cdmanager/dashboard#post')}}" class="target-link">
                                <button class="button is-text">Back</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Post Title</label>
        </div>
        <div class="field-body">
            <div class="field">
                <p class="control">
                    @if(!empty($post))
                    <input class="input" type="hidden" name="post_id" value="{{$post->post_id}}">
                    <textarea class="textarea" placeholder="Post Title" name="post_title">{{$post->post_title}}</textarea>
                    <!-- <input class="input" type="text" placeholder="Post Title" name="post_title" value="{{$post->post_title}}">  -->
                    @else
                    <input class="input" type="hidden" name="post_id">
                    <textarea class="textarea" placeholder="Post Title" name="post_title"></textarea>
                    <!-- <input class="input" type="text" placeholder="Post Title" name="post_title">  -->
                    @endif
                </p>
            </div>
        </div>
    </div>
    @if(!empty($post))
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">View Post</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    <a class="button is-black" target="_blank" href="{{url('post/'.$post->post_url)}}">
                        <span class="icon">
                            <i class="typcn typcn-eye"></i>
                        </span>
                        <span>View</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Post Category</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    <div class="select">
                        <select name="category_id">
                            @foreach(\App\Category::select('category_id', 'category_name')->get() as $category) @if(!empty($post) && $post->category_id
                            == $category->category_id)
                            <option value="{{$category->category_id}}" selected>{{$category->category_name}}</option>
                            @else
                            <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                            @endif @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <input type="hidden" name="category_id" value="1"> --}}
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Feature Image</label>
        </div>
        <div class="field-body">
            <div class="box">
                <figure>
                    @if(!empty($post) && !empty($post->post_image_url))
                    <img id="feature_image" style="height: 16rem;" src="{{$post->post_image_url}}"> 
                    @else
                    <img id="feature_image" style="height: 16rem;" src="{{url('media/no-image.jpg')}}"> 
                    @endif
                    <figcaption class="has-text-centered">
                        <a class="set-image-btn" onclick="selectedPopUp(this)" data-target="#select-image">Set Feature Image</a>
                    </figcaption>
                </figure>
                @if(!empty($post))
                <input class="input" type="hidden" name="post_image_url" value="{{$post->post_image_url}}"> 
                @else
                <input class="input" type="hidden" name="post_image_url"> 
                @endif
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
        <div class="field-label is-small">
            <label class="label">Post Content</label>
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    @if(!empty($post))
                    <div id="item-content">{!!$post->post_content!!}</div>
                    @else
                    <div id="item-content"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div id="select-image" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Select your image</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-small">
                    <label class="label">Image URL</label>
                </div>
                <div class="field-body">
                    <div class="field has-addons">
                        <p class="control">
                            <input class="input" type="text" name="image_selected_url" >
                        </p>
                        <div class="control">
                            <button class="button is-link" onclick="selectedAction(this)">
                                <span class="icon">
                                    <i class="typcn typcn-folder-add"></i>
                                </span>
                                <span>Save</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns is-multiline" id="select-image-content">
            </div>
        </section>
    </div>
</div>

<script>
    $('#item-content').summernote({
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], '#item-content');
            }
        }
    });

    function saveAction(element) {
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'post/save',
            data: {
                post_id: $('input[name=post_id]').val(),
                category_id: $('select[name=category_id]').val(),
                post_title: $('textarea[name=post_title]').val(),
                post_image_url: $('input[name=post_image_url]').val(),
                post_content: $('#item-content').summernote('code')
            },
            success: function (result) {
                if(result.status == 200){
                    swal('Good job!', result.message, 'success')
                    if(result.redirect){
                        loadURI('post');
                    }
                }else{
                    swal('Oops', result.message, 'error')
                }
            },
            complete: function() {
                item.removeClass('is-loading');
            }
        });
    };

    function selectedPopUp (element){
        $('html').addClass('is-clipped');
        $('#select-image').addClass('is-active');
        $.ajax({
            type: "POST",
            url: base_url + admin_url + 'image/datatables',
            success: function (result) {
                $('#select-image-content').html('');
                $.each(result.data, function(i, item) {
                    var image = result.data[i].image;
                    var html = '<div class="column is-4">'+
                    '<figure class="image">'+
                    '<img class="image-selected" style="cursor: pointer;" src="'+image.src+'" alt="'+image.alt+'" data-original="'+image.original+'">'+
                    '</figure>'+
                    '</div>';
                    $('#select-image-content').append(html);
                })
            }
        });
    }

    function selectedAction(element){
        var item = $(element);
        var data = $('input[name=image_selected_url]').val();

        $('#feature_image').attr('src', data);
        $('input[name=post_image_url]').val(data);
        $('html').removeClass('is-clipped');
        $('#select-image').removeClass('is-active');
    }

    $(document).on('click', '.image-selected', function () {
        var item = $(this);
        $('input[name=image_selected_url]').val(item.attr('data-original'));
    });
</script>