@include('admin/partials/breadcrumb-navigation', ['breadcrumb' => $breadcrumb])
<nav class="level notification is-black">
    <div class="level-left">
        <div class="level-item">
            <p class="title"><strong>{{end($breadcrumb)->name}}</strong></p>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <a href="{{url('cdmanager/dashboard#produksi/new')}}" class="button is-white target-link">
                <span class="icon is-small">
                    <i class="typcn typcn-plus"></i>
                </span>
                <span>Add</span>
            </a>
        </div>
    </div>
</nav>

    <table class="table is-striped is-bordered is-fullwidth" id="primary_table">
        <thead>
            <tr>
                <th>No0</th>
                <th>Judul</th>
                <th>Waktu</th>
                <th>Tiket</th>
                <th>Urutan</th>
                <th>Action</th>
            </tr>
        </thead>
        </tbody>
    </table>
</div>

<script>
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url + admin_url + 'produksi/datatables',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'judul_lakon', name: 'judul_lakon' },
            { data: 'waktu_pentas', name: 'waktu_pentas' },
            { data: 'tiket', name: 'tiket' },
            { data: 'urutan', name: 'urutan' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    return '<a class="button is-small is-rounded is-black target-link" href="{{url('cdmanager/dashboard#produksi/update')}}/'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Delete</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-black target-link" href="{{url('cdmanager/dashboard#kruproduksi')}}?produksi_id='+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <span>Tambah Crew</span>'+
                    '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();


    function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: base_url + admin_url + 'produksi/delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Good job!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }


    
</script>