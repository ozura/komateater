@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('admin/app')
@section('content')
<section class="hero is-fullheight is-primary-color">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="columns">
                <div class="column is-4 is-offset-4">
                    <div class="box">
                        <h3 class="title has-text-grey">Sign In</h3>
                        <p class="subtitle has-text-grey">Please sign in to proceed.</p>
                        <figure>
                            <img src="{{asset('web/images/logo.png')}}" width="128">
                        </figure>
                        <div class="is-gap">
                            <form method="POST" action="{{url('cdmanager/signin')}}">
                                {{ csrf_field() }}
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" placeholder="username" name="username" value="{{Request::old('username')}}" required>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="password" placeholder="password" name="password" required>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control width-100">
                                        <button class="button is-black is-primary-color width-100">Sign In</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="is-gap">
                            <p class="has-text-grey">
                                Sign Up &nbsp;·&nbsp;
                                Forgot Password &nbsp;·&nbsp;
                                Need Help?
                                Contact me <a href="https://github.com/riordhn" style="color: inherit;">@riordhn</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection 

@push('scripts')
<!-- Javascript -->
@endpush