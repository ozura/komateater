<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
        <meta name="token" content="{{csrf_token()}}">
        <meta name="theme-color" content="#000000">

        <title>Teater Koma</title>
        <link rel="shortcut icon" href="{{asset('media/logo.png')}}">
        <link rel="manifest" href="{{asset('manifest.json')}}">
        
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <base href="{{url('')}}/">

        <meta name="ogTitle" property="og:title" content="PT PANCA INDAH JAYAMAHE">
        <meta name="ogDescription" property="og:description" content="">
        <meta name="ogImage" property="og:image" content="{{asset('media/logo.png')}}">
        <meta name="ogUrl" property="og:url" content="{{env('APP_URL', '/')}}">

        <meta name="apple-mobile-web-app-title" content="PT PANCA INDAH JAYAMAHE">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <link rel="apple-touch-icon" href="{{asset('media/logo.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('media/logo.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('media/logo.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('media/logo.png')}}">

        @stack('meta')
        
        <!-- external src -->
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,600">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css">
        
        <!-- internal src -->
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/sweetalert2-7.18.0/dist/sweetalert2.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/izitoast-1.3.0/dist/css/iziToast.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/datatables-1.10.7/css/jquery.dataTables.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/nprogress-0.2.0/nprogress.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-0.6.2/css/bulma.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('vendor/bulma-quickview/dist/bulma-quickview.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('css/style.css?id='.uniqid())}}">

        <script>
            var base_url = document.getElementsByTagName('base')[0].getAttribute('href');
            var admin_url = 'cdmanager/';
        </script>
    </head>
    <body>
        @yield('content')
    </body>
    
    <!-- external src -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js" integrity="sha256-ihAoc6M/JPfrIiIeayPE9xjin4UWjsx2mjW/rtmxLM4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{asset('js/summernote.js')}}"></script>
    
    <!-- internal src -->
    <script type="text/javascript" src="{{asset('vendor/sweetalert2-7.18.0/dist/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/izitoast-1.3.0/dist/js/iziToast.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/datatables-1.10.7/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/nprogress-0.2.0//nprogress.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/bulma-quickview/dist/bulma-quickview.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app-bulma.js?id='.uniqid())}}"></script>
    <script type="text/javascript" src="{{asset('node_modules/sortablejs/Sortable.js')}}"></script>
    @stack('scripts')
    <script>
        
    @if(Session::has('toast-error'))
        $(window).load(function(){
            iziToast.warning({ title: 'Oops', message: '{{Session::get('toast-error')}}', position: 'topRight' });
        });
    @endif
    @if(Session::has('toast-success'))
        $(window).load(function(){
            iziToast.success({ title: 'Good job', message: '{{Session::get('toast-success')}}', position: 'topRight' });
        });
    @endif
    @if(Session::has('swal-error'))
        $(window).load(function(){
            swal('Oops', '{{Session::get('swal-error')}}', 'error');
        });
    @endif
    @if(Session::has('swal-success'))
        $(window).load(function(){
            swal('Good job', '{{Session::get('swal-success')}}', 'success');
        });
    @endif
    
    function summernoteUploadsImage(image, notekey) {
        var data = new FormData();
        data.append("files[]", image);
        $.ajax({
            url: "",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function(data) {
                var url = data.split('}]')[1];
                var image = $('<img>').attr('src', url);
                $(notekey).summernote("insertNode", image[0]);
                console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
    </script>
</html>
