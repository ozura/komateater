@extends('layout')
@section('content')
<!-- header -->
	<!-- tables -->
<link rel="stylesheet" type="text/css" href="web/list-css/table-style.css" />
<link rel="stylesheet" type="text/css" href="web/list-css/basictable.css" />
<script type="text/javascript" src="web/list-js/jquery.basictable.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });
     $('#table-breakpoint1').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint2').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint3').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint4').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint5').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint6').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint7').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint8').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint9').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint10').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint11').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint12').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint13').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint14').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint15').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint16').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint17').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint18').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint19').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint20').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint21').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint22').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint23').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint24').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint25').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint26').basictable({
        breakpoint: 768
      });
    });
  </script>
<!-- //tables -->
<!-- //header -->


<div class="general_social_icons">
	<nav class="social">
		<ul>
			<li class="w3_twitter"><a href="#">Twitter <i class="fa fa-twitter"></i></a></li>
			<li class="w3_facebook"><a href="#">Facebook <i class="fa fa-facebook"></i></a></li>
			<li class="w3_dribbble"><a href="#">Dribbble <i class="fa fa-dribbble"></i></a></li>
			<li class="w3_g_plus"><a href="#">Google+ <i class="fa fa-google-plus"></i></a></li>				  
		</ul>  
  </nav>
</div>
<!-- faq-banner -->
	<div class="faq">
		<div class="container text-center">
			<h3 style="background-color: black; color: white; font-size: 30px;" >LIST PRODUKSI</h3>
		</div>
			<div class="container">
				
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="agile-news-table">
									
									<table id="table-breakpoint">
										<thead>
										  <tr>
											<th>No.</th>
											<th>Judul Lakon</th>
											<th>Waktu Pentas</th>
											<th>Lama Pentas</th>
											<th>Tempat Pentas</th>
											
										  </tr>
										</thead>
										
										<tbody>
											@foreach($daftar as $daftar)
										  <tr>
											<td>{{$daftar->id}}</td>
											<td class="w3-list-img"><a href="{{url('detailproduksi')}}"><img style="height: 30px; width: 30px;" src="web/images/n1.jpg" alt="" /> <span>{{$daftar->nama_lakon}}</span></a></td>
											<td>{{$daftar->waktu_pentas}}</td>
											<td>{{$daftar->lama_pentas}}</td>
											<td>{{$daftar->lama_pentas}}</td>
											
										  </tr>
										@endforeach 																		  
										</tbody>
										
									</table>
								</div>
							</div>
						
							
				</div>
			</div>
	</div>
	
</div>

<!-- //faq-banner -->
<!-- footer -->
	
@endsection