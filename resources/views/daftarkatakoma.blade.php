@extends('layout')
@section('content')
<br><br><br>

<div class="container" style="background-color: white;">
            <style scoped>


        *{
            margin: 0px;
            padding: 0px;
        }
        body{
            font-family: arial;
        }
        .main{

            margin: 2%;
        }

        .card{
            width: 20%;
            display: inline-block;
            box-shadow: 2px 2px 20px black;
            border-radius: 5px; 
            margin: 2%;
            /*overflow: hidden;*/
            /*height: 350px;*/
        }

        @media (max-width: 550px) { /* or 301 if you want really the same as previously.  */
                .card{   
                    width: 100%;
                }
            }

        .image img{
            width: 100%;
            height: 130px;
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;



        }

        .title{

            text-align: center;
            padding: 10px;
            color: black;

        }

        h1{
            font-size: 20px;
        }

        .des{
            padding: 3px;
            text-align: center;
            color: black;
            padding-top: 10px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        button{
            margin-top: 40px;
            margin-bottom: 10px;
            background-color: white;
            border: 1px solid black;
            border-radius: 5px;
            padding:10px;
            color: black;
        }
        button:hover{
            background-color: black;
            color: white;
            transition: .5s;
            cursor: pointer;
        }

    </style>
    


    <div class="main">
        <!--cards -->
        <form action="{{url('daftarkatakoma/search')}}" method="get" class="form-inline">
          
            <input type="text" class="form-control" name="s" placeholder="Kata Kunci" 
            value="{{isset($s) ? $s :''}}"><button style="margin-top: 0px;" class="btn btn-danger" type="submit">Search</button>

          
      </form> 
        @foreach($katakoma as $post)
        <a href="{{url('post/'.$post->post_url)}}">
        <div class="card">
            <div class="image">
                <img src="{{$post->post_image_url}}">
            </div>
            <div class="title">
                <h1 style="text-overflow: ellipsis; overflow: hidden; white-space: pre;"><strong>{{$post->post_title}}</strong></h1>
            </div>
            <div class="des">
                <p style="height: 80px; overflow: hidden; text-overflow: ellipsis;">
                {{$post->post_caption}}</p>
                <button>Read More...</button>
            </div>
        </div>
        <!--cards -->
        </a>
        @endforeach
     
        
    </div>

</div>
<div class="container" align="right">{{$katakoma->links()}}</div>
             <br><br><br><br><br>
@endsection