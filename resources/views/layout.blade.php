<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Teater Koma</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="{{asset('web/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset('web/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('web/css/contactstyle.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('web/css/faqstyle.css')}}" type="text/css" media="all" />
<link href="{{asset('web/css/single.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('web/css/medile.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('tmp/css/kartu.css')}}" rel='stylesheet' type='text/css' />
<!-- banner-slider -->
<link href="{{asset('web/css/jquery.slidey.min.css')}}" rel="stylesheet">
<!-- //banner-slider -->
<!-- pop-up -->
<link href="{{asset('web/css/popuo-box.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{asset('web/css/font-awesome.min.css')}}" />
<!-- //font-awesome icons -->
<!-- js -->
<script type="text/javascript" src="{{asset('web/js/jquery-2.1.4.min.js')}}"></script>


<!-- //js -->
<!-- banner-bottom-plugin -->
<link href="{{asset('web/css/owl.carousel.css')}}" rel="stylesheet" type="text/css" media="all">
<script src="{{asset('web/js/owl.carousel.js')}}"></script>
<script src="{{asset('js/popup.js')}}"></script>
<script>
    $(document).ready(function() { 
        $("#owl-demo").owlCarousel({
     
          autoPlay: 5000, //Set AutoPlay to 3 seconds
          
          items : 5,
          itemsDesktop : [640,4],
          itemsDesktopSmall : [414,3]
     
        });
     
    }); 
</script> 
<!-- //banner-bottom-plugin -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('web/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->

</head>
    
<body>
 <!-- header -->
 {{-- <section class="section" style="background: linear-gradient(to bottom right, grey, black);"> --}}
  <section class="section" style="background: url({{asset('merah.jpg')}})  ">
  {{-- <section class="section" style="background: #212121"> --}}
 {{-- <section class="selector" >
  <style type="text/css">
    .selector {
    background-image: url({{asset('web/images/abu1.jpg')}});
    background-attachment: fixed ;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
  </style> --}}
  <div class="header">
    <div class="container">
      <div class="w3layouts_logo">
        <a href="{{url('/')}}"><img src="{{asset('web/images/logo.png')}}"></a>
      </div>
     
    </div>
  </div>
<!-- //header --> 
<section class="section" style="background: linear-gradient(to bottom right, #e1e1da, #e1e1da);">
{{-- <section class="section" style="background: url({{asset('abu.jpg')}}) 50% repeat;"> --}}
  <div class="movies_nav" style="background: linear-gradient(to bottom right, #212121, red);" >
    <div class="container">
      <nav class="navbar fixed-top navbar-default">
        <div class="navbar-header navbar-left">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <nav >
            <ul class="nav navbar-nav">
              <li class=""><a href="{{url('/')}}">Beranda</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile<b class="caret"></b></a>
                <ul class="dropdown-menu multi-column columns-3">
                  <li>
                  <div class="" align="center">
                    <ul class="multi-column-dropdown">
                      <li><a href="{{url('post/tentang-kami')}}">TENTANG KAMI</a></li>
                      <br>
                      <li><a href="{{url('post/n-riantiarno')}}">N. RIANTIARNO</a></li>
                      <br>
                      <li><a href="{{url('post/ratna-riantiarno')}}">RATNA RIANTIARNO</a></li>
                      <br>
                      <li><a href="{{url('daftarpendiri')}}">PENDIRI</a></li>
                      <br>
                      <li><a href="{{url('pemainpekerja')}}">ANGGOTA</a></li>
                      
                      

                      
                    </ul>
                  </div>
                  
                  <div class="clearfix"></div>
                  </li>
                </ul>
              </li>
              <li><a href="{{url('daftarberita')}}">Berita</a></li>
              <li><a href="{{url('daftarproduksi')}}">Produksi</a></li>
              <li><a href="{{url('daftarkatakoma')}}">Kata Koma</a></li>              
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Galeri<b class="caret"></b></a>
                <ul class="dropdown-menu multi-column columns-3">
                  <li>
                    <div class="" align="center">
                      <ul class="multi-column-dropdown">
                        <li><a href="{{url('album')}}">FOTO</a></li>
                        <br>
                        <li><a href="{{url('daftarvideo')}}">VIDEO</a></li>                        
                      </ul>
                    </div>                   
                    <div class="clearfix"></div>
                  </li>
                </ul>
              </li>
              <li><a href="{{url('daftarmerchandise')}}">Merchandise</a></li>
              <li><a href="{{url('post/kontak')}}">Kontak</a></li>
            </ul>
          </nav>
        </div>
      </nav>  
    </div>
  </div>
@yield('content')
<!-- //here ends scrolling icon -->
<!-- //banner-bottom -->
<div class="general_social_icons">
  <nav class="social">
    <ul>
      <li style="color: blue;" class="w3_twitte"><a style="color: black;" href="{{$twitter->setting_content}}" target="_blank">Twitter <i style="color: blue; text-shadow: 1px 1px 1px #ccc;
    font-size: 1.5em;" class="fa fa-twitter"></i></a></li>
      <li class="w3_faceboo"><a style="color: black;" href="{{$facebook->setting_content}}" target="_blank">Facebook <i style="color: blue; text-shadow: 1px 1px 1px #ccc;
    font-size: 1.5em;" class="fa fa-facebook"></i></a></li>
      <li class="w3_youtube"><a style="color: black;" href="{{$youtube->setting_content}}" target="_blank">Youtube <i style="color: red; text-shadow: 1px 1px 1px #ccc;
    font-size: 1.5em;" class="fa fa-youtube"></i></a></li>
      <li class="w3_instagram"><a style="color: black;" href="{{$instagram->setting_content}}" target="_blank">Instagram <i style="color: red; text-shadow: 1px 1px 1px #ccc;
    font-size: 1.5em;" class="fa fa-instagram"></i></a></li>         
    </ul>
  </nav>
</div>
</body>

<!-- footer -->
<div class="footer">
        <div align="center">
          <div class="container">
            <div class="baris">
              <div class="kolom">
               
                 
                 <h3>TEATER KOMA</h3>
                 <a href="{{url('/')}}"><img src="{{asset('web/images/logo.png')}}"></a>
               
             </div>
             <div class="kolom">
              
                <a href=""></a>
                <p>{{$saddress->setting_content}}</p> <br>                
                {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.922577705888!2d106.75921171434115!3d-6.273910663161856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f04f1bf1165b%3A0xa17c2937854d1e3!2sJl.+Cempaka+Raya+No.15%2C+RT.6%2FRW.11%2C+Bintaro%2C+Pesanggrahan%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta+12330!5e0!3m2!1sen!2sid!4v1552029160459" frameborder="0" width="200"></iframe> --}}
                <iframe src="{{$smaps->setting_content}}" frameborder="0" width="200"></iframe>
              
            </div>
            <div class="kolom" >
                    <p><strong>Phone :</strong> <pre> 089382348291</pre></p>
                    <p><strong>Email:</strong> <pre>teaterkoma@gmail.com</pre></p>
             </div>


          </div>
        </div><br>
        {{-- <div class="container" align="center">
         <div >
          <div class="w3_search" >
            <form action="#" method="post">
              <input type="text" name="Search" placeholder="Search" required="">
              <input type="submit" value="Go">
            </form>
          </div>      
        </div>
        </div> --}}
        <br>
         <div class="container text-center" style="color: white;">

          <p>Teater Koma Ⓒ 2019 powered by Mataduniadigital</p>
        </div>
       
        <div class="clearfix"> </div>
      </div>    
    
  </div>
<!-- //footer -->
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('web/js/bootstrap.min.js')}}"></script>
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script>
$(document).ready(function(){
   

    $(".dropdown").on("click", function(){
      if($('.dropdown-menu', this).is(':visible')){
         $('.dropdown-menu', this).slideUp();
      }else{
      $('.dropdown-menu', this).slideDown();
      }
      if($(this).hasClass('open')){
        $(this).removeClass('open');
      }else{
        $(this).addClass('open');

      }
    });
});
</script>
<!-- //Bootstrap Core JavaScript -->
<!-- here stars scrolling icon -->
  <script type="text/javascript">
    $(document).ready(function() {
      /*
        var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear' 
        };
      */
                
      $().UItoTop({ easingType: 'easeOutQuart' });
                
      });
  </script>
<!-- //here ends scrolling icon -->
<!-- footer -->
  
</html>