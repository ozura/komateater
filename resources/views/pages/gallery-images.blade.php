@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('app')
@section('content')
<div class="container">
    <hr class="hr-text title" data-content="{{$gallery->gallery_name}}">
</div>
<section class="section" style="padding-top:4.5rem; background-color: rgba(255,255,255,0.85);">
    <div class="columns">
        <div class="column is-9">
        @if($gallery_images->first())
            <div class="columns is-centered is-multiline">
                @foreach($gallery_images as $image)
                <div class="column is-3">
                    <figure class="image is-4by3">
                        <img class="zoom-image-btn modal-button" data-target="#zoom-image" src="{{url('storage/img/origins').'/'.$image->image_url}}">
                    </figure>
                </div>
                @endforeach
            </div>
        @else
            <div class="content has-text-centered">
                <p class="subtitle">Belum ada foto</p>
            </div>
        @endif
        </div>
        <div class="column box">
            <div class="tile is-ancestor">
                <article class="tile notification is-link is-new-link">
                    <p class="title" style="color:white;">News</p>
                </article>
            </div>
            <div class="tile is-parent is-vertical is-ancestor">
            @foreach($other_posts as $other_post)
                <div class="tile is-child is-vertical" style="border-bottom: 0.25rem solid #eeeeee; padding-bottom:1.5rem;">
                    <article class="tile is-gapless is-parent">
                        <article class="tile is-child notification is-light" style="padding:0.5rem 0;">
                            <p class="title has-text-centered" style="color:red;">{{date('j', strtotime($other_post->created_at))}}</p>
                        </article>
                        <article class="tile is-child notification is-danger" style="padding:0.5rem 0;">
                            <p class="title has-text-centered" style="color:white;">{{date('M', strtotime($other_post->created_at))}}</p>
                        </article>
                    </article>
                    <article class="tile notification is-white" style="padding:0;">
                        <div class="content">
                            <a href="{{url('post/'.$other_post->post_url)}}" style="color:blue; text-align: justify;">
                                {{$other_post->post_title}}
                            </a>
                            <p class="content-5ellipsis" style="text-align: justify;">
                                {{$other_post->post_caption}}
                            </p>
                            <a href="{{url('post/'.$other_post->post_url)}}">
                                Read more
                            </a>
                        </div>
                    </article>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</section>
<div class="modal" id="zoom-image">
    <div class="modal-background"></div>
    <div class="modal-content">
        <p class="image">
            <img src="https://bulma.io/images/placeholders/1280x960.png">
        </p>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>
@include('partials.footer')
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $(document).on('click', '.zoom-image-btn', function(){
        var item = $(this);
        var source = item.attr('src');

        $('#zoom-image img').attr('src', source);
    });
</script>
@endpush