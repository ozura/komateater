@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('app')
@section('content')
<div class="container">
    <hr class="hr-text title" data-content="GALLERIES">
</div>
<section class="section" style="padding-top:4.5rem; background-color: rgba(255,255,255,0.85);">
    <div class="columns">
        <div class="column is-9">
        @if($galleries->first())
            <div class="columns is-centered is-multiline">
                @foreach($galleries as $gallery)
                <div class="column is-3">
                    <a href="{{url('gallery/'.$gallery->gallery_id)}}">
                        <figure class="image is-4by3">
                            @if($image = \App\GalleryImage::where('gallery_id', $gallery->gallery_id)->join('images', 'images.image_id', '=', 'gallery_images.image_id')->first())
                            <img src="{{url('storage/img/origins').'/'.$image->image_url}}">
                            @else
                            <img src="https://bulma.io/images/placeholders/1280x960.png">
                            @endif
                        </figure>
                    </a>
                    <div class="content has-text-centered">
                        <p class="subtitle">
                            <strong>{{$gallery->gallery_name}}</strong>
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        @else
            <div class="content has-text-centered">
                <p class="subtitle">Belum ada Album</p>
            </div>
        @endif
        </div>
        <div class="column box">
            <div class="tile is-ancestor">
                <article class="tile notification is-link is-new-link">
                    <p class="title" style="color:white;">News</p>
                </article>
            </div>
            <div class="tile is-parent is-vertical is-ancestor">
            @foreach($other_posts as $other_post)
                <div class="tile is-child is-vertical" style="border-bottom: 0.25rem solid #eeeeee; padding-bottom:1.5rem;">
                    <article class="tile is-gapless is-parent">
                        <article class="tile is-child notification is-light" style="padding:0.5rem 0;">
                            <p class="title has-text-centered" style="color:red;">{{date('j', strtotime($other_post->created_at))}}</p>
                        </article>
                        <article class="tile is-child notification is-danger" style="padding:0.5rem 0;">
                            <p class="title has-text-centered" style="color:white;">{{date('M', strtotime($other_post->created_at))}}</p>
                        </article>
                    </article>
                    <article class="tile notification is-white" style="padding:0;">
                        <div class="content">
                            <a href="{{url('post/'.$other_post->post_url)}}" style="color:blue; text-align: justify;">
                                {{$other_post->post_title}}
                            </a>
                            <p class="content-5ellipsis" style="text-align: justify;">
                                {{$other_post->post_caption}}
                            </p>
                            <a href="{{url('post/'.$other_post->post_url)}}">
                                Read more
                            </a>
                        </div>
                    </article>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</section>
<div class="modal" id="zoom-image">
    <div class="modal-background"></div>
    <div class="modal-content">
        <p class="image">
            <img src="https://bulma.io/images/placeholders/1280x960.png">
        </p>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>
@include('partials.footer')
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $(document).on('click', '.zoom-image-btn', function(){
        var item = $(this);
        var source = item.attr('src');

        $('#zoom-image img').attr('src', source);
    });
</script>
@endpush