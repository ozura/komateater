@push('meta')
<!-- Meta Tag -->

<style>
    .jssocials-share-link { 
        font-size: 14px;
    }
</style>
@endpush 

@extends('app')
@section('content')
<section class="section" style="padding-top:4.5rem; background-color: rgba(255,255,255,0.85);">
    <div class="columns">
        <div class="column is-9">
            <div class="content">
                <p class="title">{{\App\Helpers\GlobalFunction::getStringLang($post->post_title, $lang)}}</p>
                <!-- <p class="subtitle is-6">Post At : {{date('j M Y', strtotime($post->created_at))}} | Tags : {{$post->category->category_name}}</p> -->
                <hr> 
                @php
                    $post_content = \App\Helpers\GlobalFunction::getStringLang($post->post_content, $lang);
                @endphp
                {!!$post_content!!}
            </div>
        </div>
        <div class="column box">
            <!-- <div class="box"> -->
                <div class="tile is-ancestor">
                    <article class="tile notification is-link is-new-link">
                        <p class="title" style="color:white;">News</p>
                    </article>
                </div>
                <div class="tile is-parent is-vertical is-ancestor">
                @foreach($other_posts as $other_post)
                    <div class="tile is-child is-vertical" style="border-bottom: 0.25rem solid #eeeeee; padding-bottom:1.5rem;">
                        <article class="tile is-gapless is-parent">
                            <article class="tile is-child notification is-light" style="padding:0.5rem 0;">
                                <p class="title has-text-centered" style="color:red;">{{date('j', strtotime($other_post->created_at))}}</p>
                            </article>
                            <article class="tile is-child notification is-danger" style="padding:0.5rem 0;">
                                <p class="title has-text-centered" style="color:white;">{{date('M', strtotime($other_post->created_at))}}</p>
                            </article>
                        </article>
                        <article class="tile notification is-white" style="padding:0;">
                            <div class="content">
                                <a href="{{url('post/'.$other_post->post_url)}}" style="color:blue; text-align: justify;">
                                    {{$other_post->post_title}}
                                </a>
                                <p class="content-5ellipsis" style="text-align: justify;">
                                    {{$other_post->post_caption}}
                                </p>
                                <a href="{{url('post/'.$other_post->post_url)}}">
                                    Read more
                                </a>
                            </div>
                        </article>
                    </div>
                @endforeach
                </div>
            <!-- </div> -->
        </div>
    </div>
    <p class="title is-5">Share this post to:</p>
    <div id="share-button" style="color:white;"></div>
</section>
@include('partials.footer')
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $("#share-button").jsSocials({
        showCount: false,
        showLabel: true,
        shares: [
            "email",
            "twitter",
            "facebook",
            "googleplus",
            "linkedin",
            { share: "pinterest", label: "Pin this" },
            "stumbleupon",
            "whatsapp"
        ]
    });
</script>
@endpush