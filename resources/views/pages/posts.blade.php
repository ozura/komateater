@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('app')
@section('content')
    <section class="section hero is-white">
        <hr class="hr-text title" data-content="{{strtoupper($category->category_name)}}">
        @if($posts->first())
        <div class="columns is-centered is-multiline">
            @foreach($posts as $post)
            <div class="column is-3">
                <div class="card">
                    <div class="card-image">
                        <a href="{{url('post/'.$post->post_url)}}">
                            <figure class="image is-4by3">
                                @if(!empty($post->post_image_url))
                                <img src="{{$post->post_image_url}}">
                                @else
                                <img src="https://bulma.io/images/placeholders/256x256.png">
                                @endif
                            </figure>
                        </a>
                    </div>
                    <div class="card-content">
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            <div class="content">
                                <p class="subtitle">
                                    <strong>{{$post->post_title}}</strong>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="content has-text-centered">
            <p class="subtitle">Not added post yet.</p>
        </div>
        @endif
    </section>
@include('partials.footer')
@endsection 

@push('scripts')
<!-- Javascript -->
@endpush