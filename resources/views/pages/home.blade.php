@push('meta')
<!-- Meta Tag -->
@endpush 

@extends('app')
@section('content')
<section class="section hero is-white">
    <div class="columns">
        <div class="column is-8">
            <div class="owl-carousel owl-theme owl-loaded" id="banner-home">
                <div class="owl-stage-outer">
                    <div class="owl-stage">
                        @foreach($collection_banner as $banner)
                        <div class="owl-item" style="margin-right:0.5rem; margin-left:0.5rem;">
                        @if(!empty($banner->target_url))
                        <a href="{{$banner->target_url}}">
                        @endif
                            <figure class="image">
                            @if(!empty($banner->thumbnail_url))
                                <img src="{{$banner->thumbnail_url}}">
                            @else
                                <img src="{{url('media/no-image.jpg')}}">
                            @endif
                            </figure>
                        @if(!empty($banner->target_url))
                        </a>
                        @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <div class="content">
                <p class="title has-text-centered">
                    Welcome
                </p>
                <div class="content">
                    {!!\App\Setting::where('setting_name', 'welcome')->first()->setting_content!!}
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section hero is-link" style="background: rgba(47, 49, 139, 0.5) !important">
    <hr class="hr-text title" data-content="LIST OF PROJECTS">
    <div class="columns is-centered">
        @foreach($portfolios as $i => $post)
        <div class="column is-3">
            <a href="{{url('post/'.$post->post_url)}}">
                <div class="card">
                    <div class="card-image">
                        <figure class="image" style="margin: 0;">
                            @if($i == 1)
                            <img src="https://cdn.lynda.com/course/612167/612167-636616287959650642-16x9.jpg">
                            @else
                            <img src="https://cdn.lynda.com/course/424947/424947-636167164180499960-16x9.jpg">
                            @endif
                        </figure>
                    </div>
                    <div class="card-content has-text-centered">
                        <p class="subtitle is-6" style="color: #757575;">
                            {{$post->post_title}}
                        </p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</section>
@if(!empty($news_posts))
<section class="section hero is-light">
    <div class="tile is-ancestor">
        <article class="tile is-5 notification is-link" style="background: rgba(47, 49, 139, 1) !important;">
            <p class="title" style="color:white;">Latest Post</p>
        </article>
    </div>
    <div class="columns">
        @foreach($news_posts->slice(0, 1)->all() as $post)
        <div class="column">
            <div class="card">
                <div class="card-image">
                    <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                        <figure class="image">
                            @if(!empty($post->post_image_url))
                            <img src="{{$post->post_image_url}}">
                            @else
                            <img src="https://bulma.io/images/placeholders/256x256.png">
                            @endif
                        </figure>
                    </a>
                </div>
                <div class="card-content">
                    <div class="content">
                        <strong>{{$post->post_title}}</strong>
                        <p class="content-3ellipsis">{{$post->post_caption}}</p>
                        <br> Post on: {{date('j M Y', strtotime($post->created_at))}} | 
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            Read more
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="column is-8">
            @foreach($news_posts->slice(1, 3)->all() as $post)
            <div class="card">
                <div class="card-content media">
                    <div class="media-left">
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            <figure class="image" style="width:128px">
                                @if(!empty($post->post_image_url))
                                <img src="{{$post->post_image_url}}">
                                @else
                                <img src="https://bulma.io/images/placeholders/256x256.png">
                                @endif
                            </figure>
                        </a>
                    </div>
                    <div class="media-content">
                        <strong>{{$post->post_title}}</strong>
                        <p class="content-3ellipsis">{{$post->post_caption}}</p>
                        <br> Post on: {{date('j M Y', strtotime($post->created_at))}} | 
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            Read more
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
@if($collection_client->first())
<section class="section hero is-white">
    <div class="container">
        <div class="content has-text-centered">
            <p class="title">Our Client</p>
        </div>
        <div class="columns is-multiline is-centered">
            @foreach($collection_client as $client)
                @if(!empty($client->thumbnail_url))
                <div class="column is-2">
                @if(!empty($client->target_url))
                <a href="{{$client->target_url}}">
                @endif
                    <figure class="image is-1by1 logo-mitra">
                        <img src="{{$client->thumbnail_url}}">
                    </figure>
                @if(!empty($client->target_url))
                </a>
                @endif
                </div>
                @endif
            @endforeach
        </div>
    </div>
</section>
@endif
<section class="section hero is-white">
    <div class="content has-text-centered">
        <p class="title" style="color:rgba(47, 49, 139, 1);">
            PT. Panca Indah Jayamahe        
        </p>
        <p class="subtitle">
            -YES WE CAN DO IT FOR YOU-
        </p>
        <img src="{{asset('media/foto-all.png')}}">
    </div>
</section>
@include('partials.footer')
@endsection 

@push('scripts')
<!-- Javascript -->
<script>
    $("#banner-home").owlCarousel({
        items:1,
        lazyLoad:true,
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:false
    });
    $("#gallery").owlCarousel({
        lazyLoad:true,
        loop:true,
        stagePadding:75,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            }
        }
    });
    
    $("#posts").owlCarousel({
        lazyLoad:true,
        margin: 16,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            }
        }
    });

    $(document).ready(function(){
        $('.parallax').each(function(i, obj) {
            $(this).css('background-image', 'url("'+ $(this).attr('data-url') +'")');
        });
    });
</script>
@endpush