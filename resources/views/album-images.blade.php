@extends('layout')
@section('content')
<style scoped>
    .barisp {margin: 0 -5px;}
    .kolomp {
              float: left;
              width: 20%;
              /*padding: 0 10px;*/
              color: white;
            }
          .hehe{
                height: 190px;
            }

            @media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
                .hehe{   
                    height: 70px;
                }
                .kolomp{
                    width: 33.3%;
                }
                
            }

</style>
<link rel="stylesheet" type="text/css" href="{{asset('web/css/lightbox.min.css')}}">
  <script type="text/javascript" src="{{asset('web/js/lightbox-plus-jquery.min.js')}}"></script>
<div class="container" style="background-color: white;">
            <h2 align="center">{{$gallery->gallery_name}}</h2>
            <div class="barisp" style="background-color: white;  ">
                 @foreach($gallery_images as $image)
                
              <div class="kolomp" style="background-color: white;" align="center">
              	<br>
                
                            <a href="{{url('storage/img/origins').'/'.$image->image_url}}" data-lightbox="mygallery"> 
                              <div class="hehe" style="overflow: hidden;  width: 95%; display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;">

                             <img width="100%"  src="{{url('storage/img/origins').'/'.$image->image_url}}">
                             </div>
                            </a>                            
              </div>
                

             @endforeach
         
            </div>	
</div>
<br><br><br>
@endsection