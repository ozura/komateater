
@extends('layout')
<style scoped>
#thover{
  position:fixed;
  background:#000;
  width:100%;
  height:100%;
  z-index: 2;
  opacity: .6
}

#tpopup{
  position:fixed;
  width:600px;
  height:400px;
  background:#fff;
  left:50%;
  top:50%;
  border-radius:5px;
  padding:60px 0;
  margin-left:-320px; /* width/2 + padding-left */
  margin-top:-150px; /* height/2 + padding-top */
  text-align:center;
  box-shadow:0 0 10px 0 #000;
  z-index: 2;
}
	@media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
			#tpopup{   
				width: 500px;
				height: 10px;
			}
			
		}

#tclose{
  position:absolute;
  background:black;
  color:white;
  right:-15px;
  top:-15px;
  border-radius:50%;
  width:30px;
  height:30px;
  line-height:30px;
  text-align:center;
  font-size:8px;
  font-weight:bold;
  font-family:'Arial Black', Arial, sans-serif;
  cursor:pointer;
  box-shadow:0 0 10px 0 #000;
  z-index: 2;
}  
</style>
@if($popup->tampilkan==1)
<div id="thover"></div>

  <div id="tpopup">
    {{-- <img src="https://i.imgur.com/bSDX6Q0.gif"> --}}
    <h3>{{$popup->nama}}</h3>
    <div>{!!$popup->popup!!}</div> 

    <div id="tclose">X</div>    
</div>
@endif
 

@section('content')

<!-- bootstrap-pop-up -->

	<link rel="stylesheet" type="text/css" href="{{asset('Owl/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('Owl/owl.theme.default.min.css')}}">
<!-- nav -->
	
<!-- //nav -->
<!-- banner -->

		<style type="text/css">
			
			
			.owl-dots{
				height: 0px;
			}
			.owl-theme .owl-dots .owl-dot span{
				width: 20px;
				height: 10px;
				border-radius: 0;
				background: #000;
			}
			.owl-theme .owl-dots .owl-dot.active span{
				background: red;
			}
		</style>
		<style scoped>
			.item1{background: red; height: 720px;}

			@media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
				.item1{   
					background: blue; height: 200px;
				}
			}

			.wrapper{width: 100%;height: 750px; overflow: hidden;}

			@media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
				.wrapper{width: 100%;height: 220px; overflow: hidden;}
			}

			.wrapper2{width: 100%;height: 192px; overflow: hidden;}

			@media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
				.wrapper2{width: 100%;height: 220px; overflow: hidden;}
			}
		</style>
		
		<div class="wrapper">
			{{-- <div class="owl-carouselsatu sliderWrapper owl-theme"> --}}
				<div class="owl-carouselsatu owl-carousel owl-theme">

			@foreach($slideshow as $gambar)	

		    <div class="item1"><h4><img class="item1" src="{{asset('storage/img/origins').'/'.$gambar->image_url}}" style="width:100%;"></h4></div>

		    {{-- <div class="item1"><h4><img class="item1" src="{{$gambar->image_url}}" style="width:100%;"></h4></div> --}}
		    @endforeach
		    
		    
		    
			</div>
	
			<div class="sliderControl">
				<div class="owl-controls">
				<div class="{{-- owl-nav --}}"   >
					<div align="center" >
					{{-- <div class="prvBtn" style="float: left;"> <button class="btn-danger" >Previous</button></div>
					<div class="nxtBtn" style="float: left;"><button class="btn-danger">Next</button></div> --}}
				{{-- 	<button class="prvBtn"  style="color: red;"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
					<button class="nxtBtn" style="color: red;"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>				 --}}	
					</div>
				</div>
			{{-- 	<div class="owl-dots">
					<div class="owl-dot active"><span></span></div>
					<div class="owl-dot"><span></span></div>
					<div class="owl-dot"><span></span></div>
				</div> --}}
			</div>
			</div>

		</div>

		
{{-- 	<div id="slidey" style="display:none;">
		<ul>
			<li><img src="web/images/5.jpg" alt=" "><p class='title'>Tarzan</p><p class='description'> Tarzan, having acclimated to life in London, is called back to his former home in the jungle to investigate the activities at a mining encampment.</p></li>
			<li><img src="web/images/2.jpg" alt=" "><p class='title'>Maximum Ride</p><p class='description'>Six children, genetically cross-bred with avian DNA, take flight around the country to discover their origins. Along the way, their mysterious past is ...</p></li>
			<li><img src="web/images/3.jpg" alt=" "><p class='title'>Independence</p><p class='description'>The fate of humanity hangs in the balance as the U.S. President and citizens decide if these aliens are to be trusted ...or feared.</p></li>
			<li><img src="web/images/4.jpg" alt=" "><p class='title'>Central Intelligence</p><p class='description'>Bullied as a teen for being overweight, Bob Stone (Dwayne Johnson) shows up to his high school reunion looking fit and muscular. Claiming to be on a top-secret ...</p></li>
			<li><img src="web/images/6.jpg" alt=" "><p class='title'>Ice Age</p><p class='description'>In the film's epilogue, Scrat keeps struggling to control the alien ship until it crashes on Mars, destroying all life on the planet.</p></li>
			<li><img src="web/images/7.jpg" alt=" "><p class='title'>X - Man</p><p class='description'>In 1977, paranormal investigators Ed (Patrick Wilson) and Lorraine Warren come out of a self-imposed sabbatical to travel to Enfield, a borough in north ...</p></li>
		</ul>   	
    </div>
    <script src="web/js/jquery.slidey.js"></script>
    <script src="web/js/jquery.dotdotdot.min.js"></script>
	   <script type="text/javascript">
			$("#slidey").slidey({
				interval: 8000,
				listCount: 5,
				autoplay: false,
				showList: true
			});
			$(".slidey-list-description").dotdotdot();
		</script> --}}
<!-- //banner -->
	{{-- 	<style>
		* {box-sizing: border-box;}
		body {font-family: Verdana, sans-serif;}
		.mySlides {display: none;}
		img {vertical-align: middle;}

		/* Slideshow container */
		.slideshow-container {
			max-width: 100%;
			position: relative;
			margin: auto;
		}

		/* Caption text */
		.text {
			color: #f2f2f2;
			font-size: 15px;
			padding: 8px 12px;
			position: absolute;
			bottom: 8px;
			width: 100%;
			text-align: center;
		}

		/* Number text (1/3 etc) */
		.numbertext {
			color: #f2f2f2;
			font-size: 12px;
			padding: 8px 12px;
			position: absolute;
			top: 0;
		}

		/* The dots/bullets/indicators */
		.dot {
			height: 15px;
			width: 15px;
			margin: 0 2px;
			background-color: #bbb;
			border-radius: 50%;
			display: inline-block;
			transition: background-color 0.6s ease;
		}

		.tinggi{
			height: 500px;
		}
		.active {
			background-color: #717171;
		}

		/* Fading animation */
		.fade {
			-webkit-animation-name: fade;
			-webkit-animation-duration: 1.5s;
			animation-name: fade;
			animation-duration: 1.5s;
		}

		@-webkit-keyframes fade {
			from {opacity: .4} 
			to {opacity: 1}
		}

		@keyframes fade {
			from {opacity: .4} 
			to {opacity: 1}
		}

		/* On smaller screens, decrease text size */
		@media only screen and (max-width: 500px) {
			.tinggi {height: 200px}
		}
		</style>

		<div class="slideshow-container" >

			<div class="mySlides fade">
				<div class="numbertext">1 / 3</div>
				<img class="tinggi" src="tmp/images/c5.jpg" style="width:100%">
				<div class="text">Caption Text</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">2 / 3</div>
				<img class="tinggi" src="tmp/images/2.jpg" style="width:100%">
				<div class="text">Caption Two</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">3 / 3</div>
				<img class="tinggi" src="tmp/images/2.jpg" style="width:100%">
				<div class="text">Caption Three</div>
			</div>

		</div>
		<br>

		<div style="text-align:center">
			<span class="dot"></span> 
			<span class="dot"></span> 
			<span class="dot"></span> 
		</div>

		<script>
			var slideIndex = 0;
			showSlides();

			function showSlides() {
				var i;
				var slides = document.getElementsByClassName("mySlides");
				var dots = document.getElementsByClassName("dot");
				for (i = 0; i < slides.length; i++) {
					slides[i].style.display = "none";  
				}
				slideIndex++;
				if (slideIndex > slides.length) {slideIndex = 1}    
					for (i = 0; i < dots.length; i++) {
						dots[i].className = dots[i].className.replace(" active", "");
					}
					slides[slideIndex-1].style.display = "block";  
					dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 2000); // Change image every 2 seconds
}			
</script> --}}
			<style scoped>
				.tiket img{
					width: 100%;
				}
			</style>
			@if($tiket->tampilkan==1)
			<br><br><br>
			<div class="container text-center" >
			
			<h3>{{$tiket->nama}}</h3>
			<div class="tiket">{!!$tiket->tiket!!}</div>

			</div>
			
			
			
			<br><br><br>
			@endif
<div class="container">
	<div class="w3_agile_banner_bottom_grid" style="background-color: black;">
	<h3 style="font-size: 30px; color: white;" align="center">VIDEO</h3>
<div class="wrapper2">
			{{-- <div class="owl-two sliderWrapper owl-theme"> --}}
			<div class="owl-carouseldua owl-carousel owl-theme">
			@foreach($video as $vid)	

		    {{-- <div class="item1"><h4><img class="item1" src="{{asset('storage/img/origins').'/'.$gambar->image_url}}" style="width:100%;"></h4></div> --}}
		    <iframe style="width: 100%; height: 160px;" src="{{$vid->video_url}}" frameborder="0" allow="accelerometer; autoplay=0; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		    {{-- <div class="item1"><h4><img class="item1" src="{{$gambar->image_url}}" style="width:100%;"></h4></div> --}}
		    @endforeach
		    
		    
		    
			</div>
	
			

		</div>
</div>
<div align="center"> <a href="{{url('daftarvideo')}}"> <button style="width: 170px;" align="center" type="button" class="btn btn-danger">Selengkapnya</button></a>
                </div>	
</div>


<script src="{{asset('Owl/jquery.2.min.js')}}"></script>
	<script src="{{asset('Owl/owl.carousel.min.js')}}"></script>


	
	{{-- <script src="Owl/jquery-2.0.2.min.js"></script> --}}

	<script src="{{asset('Owl/jquer.js')}}"></script>
		<script src="{{asset('Owl/jquer.js')}}"></script>
	<script>var $j = jQuery.noConflict(true);</script>
<!-- banner-bottom -->
{{-- 	<div class="banner-bottom" >		
		<div class="container">		
			<div class="w3_agile_banner_bottom_grid" style="background-color: black;">
				<h3 style="font-size: 30px; color: white;" align="center">VIDEO</h3>
				<div id="owl-demo" class="owl-carousel owl-theme" align="center" style="height: 192px; ">
					
					@foreach($video as $vid)
					<div class="item" style="background-color: black;">
						<style scoped>
						@media (max-width:480px){
							.w3l-movie-gride-agile1 {
								text-align: center;
								box-shadow: 0px 0px 10px rgba(255, 255, 255, 0.35);
								margin-right: 0%;
								margin-bottom: 3%;
								position: relative;
								padding-left: 0;
								/*float: left;*/
								width: 33.33%;
							}
						}
					</style>
						<div class="w3l-movie-gride-agile1 w3l-movie-gride-agile1">
							<a href="single.html" class="hvr-shutter-out-horizontal">
								<iframe style="width: 100%; height: 160px;" src="{{$vid->video_url}}" frameborder="0" allow="accelerometer; autoplay=0; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								
							</a>
							<div class="mid-1 agileits_w3layouts_mid_1_home">
								<div class="w3l-movie-text" >
									<h6 ><a style="color: white;" href="single.html">{{$vid->video_name}}</a></h6>							
								</div>
								
							</div>
							
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div align="center"> <a href="{{url('daftarvideo')}}"> <button style="width: 170px;" align="center" type="button" class="btn btn-danger">Selengkapnya</button></a>
                </div>			
		</div>
	</div> --}}


			



		  <style scoped>


        *{
            margin: 0px;
            padding: 0px;
        }
        body{
            font-family: arial;
        }
        .main{

            margin: 2%;
        }

        .card{
            width: 20%;
            display: inline-block;
            box-shadow: 2px 2px 20px black;
            border-radius: 5px; 
            margin: 2%;
            /*overflow: hidden;*/
            /*height: 350px;*/
        }

        @media (max-width: 550px) { /* or 301 if you want really the same as previously.  */
                .card{   
                    width: 100%;
                }
            }

        .image img{
            width: 100%;
            height: 130px;
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;



        }

        .title{

            text-align: center;
            padding: 10px;
            color: black;

        }

        h1{
            font-size: 20px;
        }

        .des{
            padding: 3px;
            text-align: center;
            color: black;
            padding-top: 10px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        button{
            margin-top: 40px;
            margin-bottom: 10px;
            background-color: white;
            border: 1px solid black;
            border-radius: 5px;
            padding:10px;
            color: black;
        }
        button:hover{
            background-color: black;
            color: white;
            transition: .5s;
            cursor: pointer;
        }

    </style>
	
 <div class="container">
	<div class="main">

		<!--cards -->
		@foreach($news_posts as $post)
		<a href="{{url('post/'.$post->post_url)}}">
		<div class="card">

			<div class="image">
				<img src="{{$post->post_image_url}}">
			</div>
			<div class="title">
				<h1 style="text-overflow: ellipsis; overflow: hidden; white-space: pre;"><strong>{{$post->post_title}}</strong></h1>
			</div>
			<div class="des">
				<p style="height: 80px; overflow: hidden; text-overflow: ellipsis;">
				{{$post->post_caption}}</p>
				<button>Read More...</button>
			</div>
		</div>
		<!--cards -->
		
		</a>
		@endforeach
		<div align="center"> <a href="{{url('daftarberita')}}"> <button style="width: 170px;" align="center" type="button" class="btn btn-danger">Selengkapnya</button></a>
                </div>	

	</div>
	</div>

{{-- <section class="section hero is-light">
    <div class="tile is-ancestor">
        <article class="tile is-5 notification is-link" style="background: rgba(47, 49, 139, 1) !important;">
            <p class="title" style="color:white;">Latest Post</p>
        </article>
    </div>
    <div class="columns">
        @foreach($news_posts->slice(0, 1)->all() as $post)
        <div class="column">
            <div class="card">
                <div class="card-image">
                    <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                        <figure class="image">
                            @if(!empty($post->post_image_url))
                            <img src="{{$post->post_image_url}}">
                            @else
                            <img src="https://bulma.io/images/placeholders/256x256.png">
                            @endif
                        </figure>
                    </a>
                </div>
                <div class="card-content">
                    <div class="content">
                        <strong>{{$post->post_title}}</strong>
                        <p class="content-3ellipsis">{{$post->post_caption}}</p>
                        <br> Post on: {{date('j M Y', strtotime($post->created_at))}} | 
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            Read more
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="column is-8">
            @foreach($news_posts->slice(1, 3)->all() as $post)
            <div class="card">
                <div class="card-content media">
                    <div class="media-left">
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            <figure class="image" style="width:128px">
                                @if(!empty($post->post_image_url))
                                <img src="{{$post->post_image_url}}">
                                @else
                                <img src="https://bulma.io/images/placeholders/256x256.png">
                                @endif
                            </figure>
                        </a>
                    </div>
                    <div class="media-content">
                        <strong>{{$post->post_title}}</strong>
                        <p class="content-3ellipsis">{{$post->post_caption}}</p>
                        <br> Post on: {{date('j M Y', strtotime($post->created_at))}} | 
                        <a href="{{url('post/'.$post->post_url)}}" style="color: inherit;">
                            Read more
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section> --}}


<!-- general -->
	
<!-- //general -->
<!-- Latest-tv-series -->
	<div class="Latest-tv-series">
		<div class="container text-center">
		<h3 style="background-color: black; color: white; font-size: 30px;" >PRODUKSI</h3>
		</div>
		<div class="container">
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<div class="agile_tv_series_grid">
								<div class="col-md-6 agile_tv_series_grid_left">
									<div class="w3ls_market_video_grid1">
										
											<!-- Copy & Pasted from YouTube -->
											@if(!empty($tproduksi->video))
											<iframe style="width: 100%; height: 312px;" src="{{$tproduksi->video}}" frameborder="0" allowfullscreen></iframe>
											@else
											<img style="width: 100%; height: 312px;" src="{{$tproduksi->foto}}">
											@endif
										
									</div>
								</div> 
								<div class="col-md-6 agile_tv_series_grid_right" style="height: 312px;">
									<div align="center" style=" height: 312px;">
									{{-- <p class="fexi_header">Conjuring</p> --}}
									<p style="font-size: 25px;"><strong>{{$tproduksi->judul_lakon}}</strong></p>
									<p style="font-size: 20px;">------------------------</p>
									@if(!empty($tproduksi->awal_pentas))
									<p style="font-size: 20px;">{{$tproduksi->awal_pentas->format('D d, M Y ')}}</p>
									<p style="font-size: 20px;">sampai</p>
									<p style="font-size: 20px;">{{$tproduksi->akhir_pentas->format('D d, M Y ')}}</p>
									@endif
									<p style="font-size: 20px;">------------------------</p>
									<p style="font-size: 20px;">{{$tproduksi->tempat_pentas}}</p>
									{{-- <p style="font-size: 20px;">{{$tproduksi->lama_pentas}}</p> --}}
									</div>
								
								</div>
								<div class="clearfix"> </div>
								<div class="agileinfo_flexislider_grids">
									@foreach($produksi as $produksis)
									<div class="col-md-4 w3l-movie-gride-agile">
										<a href="{{url('detailproduksi').'/'.$produksis->produksi_id}}" class="hvr-shutter-out-horizontal">
											@if(!empty($produksis->foto))
											<img src="{{$produksis->foto}}" title="album-name" class="img-responsive" alt=" " style=" width: 320px; height: 180px;" />
											@else
											<img src="http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/256/user-2-icon.png" title="album-name" class="img-responsive" alt=" " style=" width: 320px; height: 180px;" />
											@endif
											
											{{-- <div class="w3l-action-icon"></div> --}}
										</a>
										<div class="mid-1 agileits_w3layouts_mid_1_home">
											<div class="w3l-movie-text">
												<h6><a href="{{url('detailproduksi').'/'.$produksis->produksi_id}}">{{$produksis->judul_lakon}}</a></h6>							
											</div>
											
										</div>
										<div class="ribben">
											<p>NEW</p>
										</div>
									</div>
									@endforeach
									<div class="clearfix"> </div>
									
								</div>
							</div>
						</li>
						
						
					</ul>
				</div>
			</section>
			<div align="center"> <a href="{{url('daftarproduksi')}}"> <button style="width: 170px;" align="center" type="button" class="btn btn-danger">Selengkapnya</button></a>
                </div>
		
		</div>
	</div>
	<!-- pop-up-box -->  
	{{-- 	<script src="web/js/jquery.magnific-popup.js" type="text/javascript"></script>
	<!--//pop-up-box -->
	<div id="small-dialog" class="mfp-hide">
		<iframe src="https://player.vimeo.com/video/164819130?title=0&byline=0"></iframe>
	</div>
	<div id="small-dialog1" class="mfp-hide">
		<iframe src="https://player.vimeo.com/video/148284736"></iframe>
	</div>
	<div id="small-dialog2" class="mfp-hide">
		<iframe src="https://player.vimeo.com/video/165197924?color=ffffff&title=0&byline=0&portrait=0"></iframe>
	</div>
	<script>
		$(document).ready(function() {
		$('.w3_play_icon,.w3_play_icon1,.w3_play_icon2').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});
																		
		});
	</script> --}}
<!-- //Latest-tv-series -->
@if($smerchandise->setting_content == 'unhide')

<div class="general">
		<div class="container text-center">
		<h3 style="background-color: black; color: white; font-size: 30px;" >MERCHANDISE</h3>
		</div>
		<div class="container">
			<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
				
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<?php $flag = 1; ?>
					@foreach($cmerchandise as $category)
					<li role="presentation" @if($flag==1) class="active" @endif ><a style="color: white;" href="#{{$category->name}}" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">{{$category->name}}</a></li>
					<?php $flag++; ?>
					@endforeach
					
				</ul>
				
				<div id="myTabContent" class="tab-content">
					<?php $flag=1;?>
					@foreach($cmerchandise as $category)
					<div role="tabpanel" @if($flag==1)class="tab-pane fade active in" @else class="tab-pane fade"@endif id="{{$category->name}}" aria-labelledby="imdb-tab">
						<?php $flag++; ?>
					@foreach($merchandise as $item)
						@if($item->collection_category_id == $category->collection_category_id)
						<div style="height: 200px;" class="col-md-2 w3l-movie-gride-agile">
							<a href="{{ url('/detailmerchandise/'.$item->collection_id) }}" class="hvr-shutter-out-horizontal"><img style="height: 150px; width: 150px;" src="{{$item->thumbnail_url}}" title="album-name" class="img-responsive" alt=" " />
								
							</a>
							<div class="mid-1 agileits_w3layouts_mid_1_home">
								<div class="w3l-movie-text">
									<h6><a href="single.html">{{$item->name}}</a></h6>				
								</div>
								<div class="mid-2 agile_mid_2_home">
									
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="ribben">
								<p>NEW</p>
							</div>
						</div>
				
						@endif	
						@endforeach	
						<div class="clearfix"> </div>

					</div>

					
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endif

@if($spartner->setting_content == 'unhide')
	<div class="container text-center">
			<h3 style="background-color: black; color: white; font-size: 30px;" >TEATER KOMA PARTNER</h3>
	</div>
	<div class="container" align="center">
		<style scoped>
		.barisp {margin: 0 -5px;}
		.kolomp {
			float: left;
			width: 16.6%;
			/*padding: 0 10px;*/
			color: white;
		}
		.hehe{
			height: 190px;
		}

		@media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
			.hehe{   
				height: 70px;
			}
			.kolomp{
				width: 100%;
			}

		}

	</style>
	
	
	
	
		<div class="" style="background-color: white;  ">
			@foreach($partner as $image)

			<div class="kolomp" style="background-color: white;" align="center">
				<br>

				<a href=""> 
					<div  style="overflow: hidden;  width: 95%;">

					{{-- <img height="200" width="200" src="{{url('storage/img/origins').'/'.$image->image_url}}"> --}}
					<a href="{{$image->target_url}}" target="_blank">
					<img height="200" width="200" src="{{$image->thumbnail_url}}">
					</a>
				</div>
			</a>                            
		</div>


		@endforeach

	</div>	
	
	<br><br><br>
	</div>
@endif
	<br><br><br>
<!-- Bootstrap Core JavaScript -->
{{-- <script src="{{asset('web/js/bootstrap.min.js')}}"></script>

<!-- //Bootstrap Core JavaScript -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon --> --}}

@endsection