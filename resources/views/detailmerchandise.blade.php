@extends('layout')
@section('content')
<br><br><br>
<div class="container" style="background-color: white;">
            <style scoped>


        *{
            margin: 0px;
            padding: 0px;
        }
        body{
            font-family: arial;
        }
        .main{

            margin: 2%;
        }

        .card{
            width: 50%;
            display: inline-block;
            box-shadow: 2px 2px 20px black;
            border-radius: 5px; 
            /*margin: 2%;*/
            /*overflow: hidden;*/
            /*height: 350px;*/
        }

        @media (max-width: 550px) { /* or 301 if you want really the same as previously.  */
                .card{   
                    width: 100%;
                }
            }

        .hehe{
            height: 300px;
        }

        @media (max-width: 550px) { /* or 301 if you want really the same as previously.  */
                .hehe{   
                    height: 200px;
                    
                }
            }
        .image img{
            /*width: 100%;*/
            /*height: 130px;*/
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;



        }

        .title{

            text-align: center;
            padding: 10px;
            color: black;

        }

        h1{
            font-size: 20px;
        }

        .des{
            padding: 3px;
            text-align: center;
            color: black;
            padding-top: 10px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        button{
            margin-top: 40px;
            margin-bottom: 10px;
            background-color: white;
            border: 1px solid black;
            border-radius: 5px;
            padding:10px;
            color: black;
        }
        button:hover{
            background-color: black;
            color: white;
            transition: .5s;
            cursor: pointer;
        }

    </style>
    

    <div class="main">

        <!--cards -->
        
        
        <div align="center">
        <div class="card">
            <div class="image" align="center">
                <img style="margin-top: 7%;" class="hehe" src="{{$merchandise->thumbnail_url}}">
            </div>
            <div class="title">
                <h1 style="text-overflow: ellipsis; overflow: hidden; white-space: pre;"><strong>{{$merchandise->name}}</strong></h1>
            </div>
            <br>
            <div class="des">
                <p style=" overflow: hidden; text-overflow: ellipsis;">
                {!!$merchandise->keterangan!!}</p>
                
            </div>
            <br><br><br>
        </div>
        <!--cards -->
        </div>
        
        
               

    </div>

</div>

             <br><br><br><br><br>
@endsection