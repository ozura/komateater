@extends('layout')
@section('content')
<!-- tables -->
<link rel="stylesheet" type="text/css" href="{{url('web/list-css/table-style.css')}}" />
<link rel="stylesheet" type="text/css" href="{{url('web/list-css/basictable.css')}}" />
<script type="text/javascript" src="{{url('web/list-js/jquery.basictable.min.js')}}"></script>
 <script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });
     $('#table-breakpoint1').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint2').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint3').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint4').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint5').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint6').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint7').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint8').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint9').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint10').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint11').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint12').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint13').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint14').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint15').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint16').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint17').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint18').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint19').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint20').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint21').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint22').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint23').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint24').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint25').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint26').basictable({
        breakpoint: 768
      });
    });
  </script>
  <link rel="stylesheet" type="text/css" href="{{url('web/css/lightbox.min.css')}}">
  <script type="text/javascript" src="{{url('web/js/lightbox-plus-jquery.min.js')}}"></script>
<!-- //tables -->
<!-- Latest-tv-series -->
	<div class="Latest-tv-series">
		
		<div class="container">
			
							<div class="agile_tv_series_grid">
								<div class="col-md-6 agile_tv_series_grid_left">
									<div class="w3ls_market_video_grid1">
										
										<!-- Copy & Pasted from YouTube -->
											@if(!empty($tproduksi->video))
											<iframe style="width: 100%; height: 312px;" src="{{$produksi->video}}" frameborder="0" allowfullscreen></iframe>
											@else
											<img style="width: 100%; height: 312px;" src="{{$produksi->foto}}">
											@endif
										
									</div>
								</div>
							<div class="col-md-6 agile_tv_series_grid_right" style="height: 312px;">
									<div align="center" style=" height: 312px;">
									{{-- <p class="fexi_header">Conjuring</p> --}}
									<p style="font-size: 25px;"><strong>{{$produksi->judul_lakon}}</strong></p>
									<p style="font-size: 20px;">------------------------</p>
									@if(!empty($produksi->awal_pentas))
									<p style="font-size: 20px;">{{$produksi->awal_pentas->format('D d, M Y ')}}</p>
									<p style="font-size: 20px;">sampai</p>
									<p style="font-size: 20px;">{{$produksi->akhir_pentas->format('D d, M Y ')}}</p>
									@endif
									<p style="font-size: 20px;">------------------------</p>
									<p style="font-size: 20px;">{{$produksi->tempat_pentas}}</p>
									{{-- <p style="font-size: 20px;">{{$produksi->lama_pentas}}</p> --}}
									</div>
								
								</div>
								<div class="clearfix"> </div>																							
							</div>		


							{{-- <div class="agile_tv_series_grid">
								<div class="col-md-3 agile_tv_series_grid_left">
									<div class="w3ls_market_video_grid1">
										<img src="https://www.pu.go.id//assets/images/content/banner.jpg" style="width: 100%; height: 312px;">
									
										
									</div>
								</div>
								<div class="col-md-9 agile_tv_series_grid_right">
									<div align="center" style="margin: 50px;">
									<p style="font-size: 25px;"><strong>SITI NURBAYA</strong></p>
									<p style="font-size: 20px;">22 Januari 2019</p>
									<p style="font-size: 20px;">Surabaya</p>
									</div>
									
									
								</div>
								<div class="clearfix"> </div>																							
							</div>	 --}}		 
						</div>

					</div>

			<div class="container" style="background-color: white;">
				<p style="font-size: 23px;" align="center"><strong>SINOPSIS</strong></p>
				{!!$produksi->sinopsis!!}
			</div>
	
<!-- //Latest-tv-series -->
	<div class="faq">
		<div class="container text-center">
			<h3 style="background-color: black; color: white; font-size: 30px;" >PEMAIN</h3>
		</div>
			<div class="container">
				
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="agile-news-table">
									
									<table id="">
										<thead>
										  <tr>
											<th>No.</th>
											<th>Nama</th>
											<th>Peran</th>
											
										  </tr>
										</thead>
										<tbody>
											<?php $no = 1 ?>
											@foreach($pemain as $pemains)
										  <tr>
											<td>{{$no}}</td>
											<td><a href="{{url('detailprofil').'/'.$pemains->kru_id}}"><span>{{$pemains->nama}}</span></a></td>
											<td>{{$pemains->role}}</td>
											
										  </tr>
										  <?php $no++; ?>
											@endforeach																  
										</tbody>
									</table>
								</div>
							</div>
							
							
				</div>
			</div>
	</div>
	
</div>
<div class="faq">
		<div class="container text-center">
			<h3 style="background-color: black; color: white; font-size: 30px;" >PEKERJA</h3>
		</div>
			<div class="container">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="agile-news-table">
									
									<table id="">
										<thead>
										  <tr>
											<th>No.</th>
											<th>Nama</th>
											<th>Jabatan</th>
											
										  </tr>
										</thead>
										<tbody>
											<?php $no = 1 ?>
											@foreach($pekerja as $pekerjas)
										  <tr>
											<td>{{$no}}</td>
											<td><a href="{{url('detailprofil').'/'.$pekerjas->kru_id}}"><span>{{$pekerjas->nama}}</span></a></td>
											<td>{{$pekerjas->role}}</td>
											
										  </tr>
										  <?php $no++; ?>
										 	@endforeach
										  																		  
										</tbody>
									</table>
								</div>
							</div>
							
							
				</div>
			</div>
	</div>
	
</div>
	{{-- <style type="text/css">
		.gallery{
			margin :100px 100px;
		}
		.gallery img{
			width: 230px;
			height: 100px;
			padding: 5px;
		}
	</style>
		<div class="gallery">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="tmp/images/46.jpg">
		</div>
 --}}

 <style scoped="" type="text/css">
 	.kolom1{
 		width: 20%;
 	}

 	@media screen and (max-width: 600px) {
 		.kolom1 {
 			width: 100%;
 			display: block;
 			margin-bottom: 20px;
 		}
}
 	
 </style>

<div class="container" style="background-color: white;">
            <div class="baris" style="background-color: white; ">
            	<?php $i = 0; ?>
            @foreach($gambar as $gam)
              <div class="kolom1" style="background-color: white;" align="center">
              	<br>

                 <a href="{{url('storage/img/origins').'/'.$gam->image_url}}" data-lightbox="mygallery"><img style="width: 95%; height: 150px;" src="{{url('storage/img/origins').'/'.$gam->image_url}}"></a>
             </div>
             <?php $i++; ?>
             <?php $lengkap = $gam->gallery_id?>
            @endforeach
         
            </div>	
            <br>
            @if($i == 10)
            <div align="center"> <a href="{{url('gallery/'.$produksi->gallery_id)}}"> <button style="width: 170px;" align="center" type="button" class="btn btn-danger">Selengkapnya</button></a>
                </div>
                @endif	
</div>
<br><br><br>
@endsection