@extends('layout')
@section('content')


<div class="jumbotron jumbotron-fluid">
  <div class="container text-center">
    <img style="border-radius: 50%;" src="{{$profile->foto}}" width="200" height="200" class="rounded-circle">
    @if(!empty($profile))
    <br><br>
    <h3  class="display-4">{{$profile->nama}}</h3>
    <p style="font-size: 14px;">{{$profile->role}}</p>
    @endif
  </div>
</div>
@if(!empty($profile))
<div class="container" style="background-color: white;">
				
				
				       {!!$profile->biografi!!}
			</div>
      <br><br><br>        
@endif
@endsection