<!doctype html>
<html lang="{{ app()->getLocale() }}" class="has-navbar-fixed-top-is-me">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
        <meta name="token" content="{{csrf_token()}}">
        <meta name="theme-color" content="#000000">

        <title>Teater Koma</title>
        <link rel="shortcut icon" href="{{asset('media/logo.png')}}">
        <link rel="manifest" href="{{asset('manifest.json')}}">
        
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <base href="{{url('')}}/">

        <meta name="ogTitle" property="og:title" content="PT PANCA INDAH JAYAMAHE">
        <meta name="ogDescription" property="og:description" content="">
        <meta name="ogImage" property="og:image" content="{{asset('media/logo.png')}}">
        <meta name="ogUrl" property="og:url" content="{{env('APP_URL', '/')}}">

        <meta name="apple-mobile-web-app-title" content="PT PANCA INDAH JAYAMAHE">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <link rel="apple-touch-icon" href="{{asset('media/logo.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('media/logo.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('media/logo.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('media/logo.png')}}">

        @stack('meta')
        
        <!-- external src -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
        <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
        
        <!-- internal src -->
        <link rel="stylesheet" href="{{asset('css/style.css?id='.uniqid())}}" />
        <link rel="stylesheet" href="{{asset('css/flag-icon/flag-icon.min.css')}}" />
        <link rel="stylesheet" href="{{asset('vendor/bulma-0.6.2/css/bulma.css')}}" />
        <link rel="stylesheet" href="{{asset('vendor/owlcarousel2-2.3.4/dist/assets/owl.carousel.min.css')}}" />
        <link rel="stylesheet" href="{{asset('vendor/owlcarousel2-2.3.4/dist/assets/owl.theme.default.min.css')}}" />
        <link rel="stylesheet" href="{{asset('vendor/bulma-tooltip/dist/css/bulma-tooltip.min.css')}}" />
    </head>
    <body style="background-image:url('{{asset('media/background-body.jpg')}}'); background-size:713px auto;">
        <div id="main-body" class="container">
        @include('partials.header-navigation')
        @yield('content')
        </div>
    </body>
    
    <!-- external src -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js" integrity="sha256-ihAoc6M/JPfrIiIeayPE9xjin4UWjsx2mjW/rtmxLM4=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    
    <!-- internal src -->
    <script type="text/javascript" src="{{asset('vendor/owlcarousel2-2.3.4/dist/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app-bulma.js')}}"></script>
    @stack('scripts')
</html>
