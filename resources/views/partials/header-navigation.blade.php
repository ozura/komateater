<figure class="image">
    <img src="{{asset('media/banner-top.jpg')}}">
</figure>
<nav class="navbar is-link is-new-link navbar" style="background-image: linear-gradient(141deg, #b8c6fb 0%, #3273dc 60%, #0e6799 75%);">
    <div class="navbar-brand" style="margin-left: 1rem;">
        <a href="{{url('/')}}">
            <img src="{{asset('media/logo.png')}}" width="70">
        </a>
        <div class="navbar-burger burger" data-target="main-navbar">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>


    <div id="main-navbar" class="navbar-menu">
        <div class="navbar-start">
    @foreach($menus->where('menu_root', 0)->sortBy('menu_order')->all() as $menu)
        @if(empty($menu->category_id))
            @if($menus->where('menu_root', $menu->menu_id)->count() == 0)
            <a class="navbar-item" href="{{$menu->menu_url}}">
                <b>{{\App\Helpers\GlobalFunction::getStringLang($menu->menu_text, $lang)}}</b>
            </a>
            @else
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    {{\App\Helpers\GlobalFunction::getStringLang($menu->menu_text, $lang)}}
                </a>
                <div class="navbar-dropdown is-boxed" style="z-index: 25;">
                @foreach($menus->where('menu_root', $menu->menu_id)->sortBy('menu_order')->all() as $submenu)
                    @if($menus->where('menu_root', $submenu->menu_id)->count() == 0)
                    <a class="navbar-item" href="{{$submenu->menu_url}}">
                        {{\App\Helpers\GlobalFunction::getStringLang($submenu->menu_text, $lang)}}
                    </a>
                    @else
                    <div class="navbar-item dropdown is-hoverable">
                        <div class="dropdown-trigger" aria-haspopup="true" aria-controls="submenu-{{$submenu->menu_id}}">
                            <a href="{{$submenu->menu_url}}" style="color: #4a4a4a;"> {{\App\Helpers\GlobalFunction::getStringLang($submenu->menu_text, $lang)}} </a> <i class="fa fa-caret-right" style="margin-left: 0.5rem;"></i>
                        </div>
                        <div class="dropdown-menu" id="submenu-{{$submenu->menu_id}}" role="menu" style="left: 100%; top: 0; padding-top: 0;">
                            <div class="dropdown-content">
                            @foreach($menus->where('menu_root', $submenu->menu_id)->sortBy('menu_order')->all() as $subsubmenu)
                                <a class="dropdown-item" href="{{$subsubmenu->menu_url}}">
                                    <p>{{\App\Helpers\GlobalFunction::getStringLang($subsubmenu->menu_text, $lang)}}</p>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    @foreach($menus->where('menu_root', $submenu->menu_id)->sortBy('menu_order')->all() as $subsubmenu)
                    <a class="navbar-item is-hidden-desktop" style="padding-top: 1rem; padding-bottom: 0;" href="{{$subsubmenu->menu_url}}">
                        {{\App\Helpers\GlobalFunction::getStringLang($subsubmenu->menu_text, $lang)}}
                    </a>
                    @endforeach
                </div>
                    @endif
                @endforeach
                </div>
            </div>
            @endif
        @else
        <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link">
                {{\App\Helpers\GlobalFunction::getStringLang($menu->menu_text, $lang)}}
            </a>
            <div class="navbar-dropdown is-boxed" style="z-index: 25;">
            @foreach(\App\Post::select('post_title', 'post_url')->where('category_id', $menu->category_id)->get() as $post)
                <a class="navbar-item" href="{{url('post/'.$post->post_url)}}">
                    {{\App\Helpers\GlobalFunction::getStringLang($post->post_title, $lang)}}
                </a>
            @endforeach
            </div>
        </div>
        @endif
    @endforeach
        </div>
        <div class="navbar-end" style="margin-right: 1rem;">
            <a class="navbar-item" href="{{url('change-lang?lang=id')}}">
                <span class="flag-icon flag-icon-id flag-icon-squared" style="margin-right:0.25rem;"></span> ID
            </a>
            <a class="navbar-item" href="{{url('change-lang?lang=eng')}}">
                <span class="flag-icon flag-icon-gb flag-icon-squared" style="margin-right:0.25rem;"></span> ENG
            </a>
        </div>
    </div>
</nav>