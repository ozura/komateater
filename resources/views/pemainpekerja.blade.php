@extends('layout')
@section('content')
<!-- tables -->
<link rel="stylesheet" type="text/css" href="{{url('web/list-css/table-style.css')}}" />
<link rel="stylesheet" type="text/css" href="{{url('web/list-css/basictable.css')}}" />
<script type="text/javascript" src="{{url('web/list-js/jquery.basictable.min.js')}}"></script>
 <script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });
     $('#table-breakpoint1').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint2').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint3').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint4').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint5').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint6').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint7').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint8').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint9').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint10').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint11').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint12').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint13').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint14').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint15').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint16').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint17').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint18').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint19').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint20').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint21').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint22').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint23').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint24').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint25').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint26').basictable({
        breakpoint: 768
      });
    });
  </script>
  <link rel="stylesheet" type="text/css" href="{{url('web/css/lightbox.min.css')}}">
  <script type="text/javascript" src="{{url('web/js/lightbox-plus-jquery.min.js')}}"></script>
<!-- //tables -->
<!-- Latest-tv-series -->
	

<div class="faq">
  
        <div class="container text-right">
          <a href="{{url('daftaranggota/search/?abjad=Abjad')}}">Urut Abjad</a>
        </div>
		<div class="container text-center">
			<h3 style="background-color: black; color: white; font-size: 30px;" >ANGGOTA TEATER KOMA</h3>
		</div>
			<div class="container">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="agile-news-table">
									
									<table id="">
										<thead>
										  <tr>
											<th>No.</th>
											<th>Nama</th>
											
										  </tr>
										</thead>
										<tbody>
											<?php $no =1; ?>
											@foreach($pekerja as $pekerjas)
										  <tr>
											<td>{{$no}}</td>
											<td><a href="{{url('detailprofil').'/'.$pekerjas->kru_id}}"><span>{{$pekerjas->nama}}</span></a></td>
											
										  </tr>
										  	<?php $no++; ?>
										 	@endforeach
										  																		  
										</tbody>
									</table>
                  {{$pekerja->links()}}
								</div>
							</div>
							
							
				</div>
			</div>
	</div>
	
</div>
	{{-- <style type="text/css">
		.gallery{
			margin :100px 100px;
		}
		.gallery img{
			width: 230px;
			height: 100px;
			padding: 5px;
		}
	</style>
		<div class="gallery">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="web/images/2.jpg">
			<img src="tmp/images/46.jpg">
		</div>
 --}}




@endsection