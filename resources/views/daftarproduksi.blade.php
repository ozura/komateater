@extends('layout')
@section('content')
<!-- header -->
	<!-- tables -->
<link rel="stylesheet" type="text/css" href="{{asset('web/list-css/table-style.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('web/list-css/basictable.css')}}" />
<script type="text/javascript" src="{{asset('web/list-js/jquery.basictable.min.js')}}"></script>
 <script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });
     $('#table-breakpoint1').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint2').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint3').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint4').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint5').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint6').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint7').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint8').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint9').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint10').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint11').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint12').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint13').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint14').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint15').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint16').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint17').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint18').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint19').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint20').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint21').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint22').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint23').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint24').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint25').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint26').basictable({
        breakpoint: 768
      });
    });
  </script>
<!-- //tables -->
<!-- //header -->


<!-- faq-banner -->
	<div class="faq">
    <div class="container">
      <form action="{{url('daftarproduksi/search')}}" method="get" class="form-inline">
          
            <input type="text" class="form-control" name="s" placeholder="Kata Kunci" 
            value="{{isset($s) ? $s :''}}">
            <button class="btn btn-danger" type="submit">Search</button>

          
      </form> 
    </div>
    
    <div class="container text-right">
      <a href="{{url('daftarproduksi/search/?abjad=Abjad')}}">Urut Abjad</a>
    </div>
   <div class="container text-right" style="">
    <form method="get" action="{{url('daftarproduksi/search')}}">
      <select name="tahun">
        <div style="overflow: auto; height: 100px;">
        <option value="0">Tahun:</option>
        <option value="1977">1977</option>
        <option value="1978">1978</option>
        <option value="1979">1979</option>
        <option value="1980">1980</option>
        <option value="1981">1981</option>
        <option value="1982">1982</option>
        <option value="1983">1983</option>
        <option value="1984">1984</option>
        <option value="1985">1985</option>
        <option value="1986">1986</option>
        <option value="1987">1987</option>
        <option value="1988">1988</option>
        <option value="1989">1989</option>
        <option value="1990">1990</option>
        <option value="1991">1991</option>
        <option value="1992">1992</option>
        <option value="1993">1993</option>
        <option value="1994">1994</option>
        <option value="1995">1995</option>
        <option value="1996">1996</option>
        <option value="1997">1997</option>
        <option value="1998">1998</option>
        <option value="1999">1999</option>
        <option value="2000">2000</option>
        <option value="2001">2001</option>
        <option value="2002">2002</option>
        <option value="2003">2003</option>
        <option value="2004">2004</option>
        <option value="2005">2005</option>
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
        <option value="2009">2009</option>
        <option value="2010">2010</option>
        <option value="2011">2011</option>
        <option value="2012">2012</option>
        <option value="2013">2013</option>
        <option value="2014">2014</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        </div>
      </select>
      <button type="submit">Search</button>
    </form>
  </div>
    
     <div class="container"  >
            
              {{-- <input type="text" name="Search" placeholder="Search" required=""> --}}
              {{-- <input type="submit" value="Go"> --}}
             {{--  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"> --}}
          </div>  
		<div class="container text-center">
     
			<h3 style="background-color: black; color: white; font-size: 30px;" >PRODUKSI PANGGUNG</h3>
		</div>
			<div class="container">
				
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="agile-news-table">
									
									<table id="table-breakpoint">
										<thead>
										  <tr>
											{{-- <th>No.</th> --}}
											<th>Judul Lakon</th>
											<th>Awal Pentas</th>
											<th>Akhir Pentas</th>
											{{-- <th>Lama Pentas</th> --}}
											<th>Tempat Pentas</th>
											
										  </tr>
										</thead>
										
										<tbody>
                      
											@foreach($daftar as $key => $daftars)
										  <tr>
                        
											{{-- <td>{{ ($daftar->currentpage()-1) * $daftar->perpage() + $key + 1 }}</td> --}}                  
											<td class="w3-list-img"><a href="{{url('detailproduksi').'/'.$daftars->produksi_id}}"> <span>{{$daftars->judul_lakon}}</span></a></td>
											<td>{{$daftars->awal_pentas->format('D d, M Y ')}}</td>
											<td>{{$daftars->akhir_pentas->format('D d, M Y ')}}</td>
											{{-- <td>{{$daftars->lama_pentas}} hari</td> --}}
											<td>{{$daftars->tempat_pentas}}</td>
											
										  </tr>
										@endforeach 								
                    
										</tbody>
										
									</table>
									{{$daftar->appends(['s'=> $s])->links()}}						

								</div>
							</div>
						
							
				</div>
			</div>
	</div>
	
</div>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("table-breakpoint");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
<!-- //faq-banner -->
<!-- footer -->
	
@endsection