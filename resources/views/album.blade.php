@extends('layout')
@section('content')
<link rel="stylesheet" type="text/css" href="web/css/lightbox.min.css">
  <script type="text/javascript" src="web/js/lightbox-plus-jquery.min.js"></script>

<style scoped>
    .barisp {margin: 0 -5px;}
    .kolomp {
              float: left;
              width: 20%;
              /*padding: 0 10px;*/
              color: white;
            }
          .hehe{
                height: 190px;
            }

            @media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
                .hehe{   
                    height: 70px;
                }
                .kolomp{
                    width: 33.3%;
                }
                
            }

</style>

<div class="container" style="background-color: white;">
            <div class="barisp" style="background-color: blue; ">
                @foreach($galleries as $gallery)
                
              <div class="kolomp" style="background-color: white; " align="center">
                <a href="{{url('gallery/'.$gallery->gallery_id)}}">
              	<br>
                 @if($image = \App\GalleryImage::where('gallery_id', $gallery->gallery_id)->join('images', 'images.image_id', '=', 'gallery_images.image_id')->first())
                            <div class="hehe" style="overflow: hidden;  width: 95%; display: flex;flex-direction: column;justify-content: center; align-items: center;" >
                             <img width="100%" src="{{url('storage/img/origins').'/'.$image->image_url}}">                            
                             </div>
                            @else
                            <div class="hehe" style="overflow: hidden; width: 95%; display: flex;flex-direction: column;justify-content: center; align-items: center;" >
                            <img width="100%"  src="https://bulma.io/images/placeholders/1280x960.png">
                          </div>  

                            @endif
                <div style="height: 27px;  overflow: hidden; text-overflow: ellipsis; ">
                <h3 style="color: black; ">{{$gallery->gallery_name}}</h3>
                </div>
                </a>
             </div>
                

             @endforeach
         
            </div>	
            
</div>
<div class="container" align="right" >{{$galleries->links()}}</div>
<br><br><br>
@endsection