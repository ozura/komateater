@extends('layout')
@section('content')
<!-- header -->
	<!-- tables -->
<link rel="stylesheet" type="text/css" href="web/list-css/table-style.css" />
<link rel="stylesheet" type="text/css" href="web/list-css/basictable.css" />
<script type="text/javascript" src="web/list-js/jquery.basictable.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });
     $('#table-breakpoint1').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint2').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint3').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint4').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint5').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint6').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint7').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint8').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint9').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint10').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint11').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint12').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint13').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint14').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint15').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint16').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint17').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint18').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint19').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint20').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint21').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint22').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint23').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint24').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint25').basictable({
        breakpoint: 768
      });
    $('#table-breakpoint26').basictable({
        breakpoint: 768
      });
    });
  </script>
<!-- //tables -->
<!-- //header -->


<div class="general_social_icons">
	<nav class="social">
		<ul>
			<li class="w3_twitter"><a href="#">Twitter <i class="fa fa-twitter"></i></a></li>
			<li class="w3_facebook"><a href="#">Facebook <i class="fa fa-facebook"></i></a></li>
			<li class="w3_dribbble"><a href="#">Dribbble <i class="fa fa-dribbble"></i></a></li>
			<li class="w3_g_plus"><a href="#">Google+ <i class="fa fa-google-plus"></i></a></li>				  
		</ul>  
  </nav>
</div>
<!-- faq-banner -->
	<div class="faq">
    <div class="container">
      <form action="{{url('daftarproduksi/search')}}" method="get" class="form-inline">
          
            <input type="text" class="form-control" name="s" placeholder="Kata Kunci" 
            value="{{isset($s) ? $s :''}}">
            <button class="btn btn-danger" type="submit">Searchzzz</button>
          
      </form> 
    </div>
     <div class="container"  >
            
              {{-- <input type="text" name="Search" placeholder="Search" required=""> --}}
              {{-- <input type="submit" value="Go"> --}}
             {{--  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"> --}}
          </div>  
		<div class="container text-center">
     
			<h3 style="background-color: black; color: white; font-size: 30px;" >PRODUKSI PANGGUNG</h3>
		</div>
			<div class="container">
				
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="agile-news-table">
									
									<table id="table-breakpoint">
										<thead>
										  <tr>
											<th>No.</th>
											<th>Judul Lakon</th>
											<th>Awal Pentas</th>
											<th>Akhir Pentas</th>
											<th>Lama Pentas</th>
											<th>Tempat Pentas</th>
											
										  </tr>
										</thead>
										
										<tbody>

											@foreach($daftar as $daftars)
										  <tr>

											<td>{{$daftars->produksi_id}}</td>
											<td class="w3-list-img"><a href="{{url('detailproduksi').'/'.$daftars->produksi_id}}"> <span>{{$daftars->judul_lakon}}</span></a></td>
											<td>{{$daftars->awal_pentas}}</td>
											<td>{{$daftars->akhir_pentas}}</td>
											<td>{{$daftars->lama_pentas}} hari</td>
											<td>{{$daftars->tempat_pentas}}</td>
											
										  </tr>
										@endforeach 								
										</tbody>
										
									</table>
									{{$daftar->appends(['s'=> $s])->links()}}						
								</div>
							</div>
						
							
				</div>
			</div>
	</div>
	
</div>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("table-breakpoint");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
<!-- //faq-banner -->
<!-- footer -->
	
@endsection