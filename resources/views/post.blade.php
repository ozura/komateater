@extends('layout')
@section('content')
	<br><br>
  
<style type="text/css">
     #share-buttons img { width: 35px; padding: 5px; border: 0; box-shadow: 0; display: inline;  }
</style>
<div class="container" style="background-color: white;">
    <br><br>
                @if($post->post_id != 10 && $post->post_id != 11)
                <p style="font-size: 30px;  "> 
                  <strong>{{\App\Helpers\GlobalFunction::getStringLang($post->post_title, $lang)}}</strong>
                </p>
                @endif
                <!-- <p class="subtitle is-6">Post At : {{date('j M Y', strtotime($post->created_at))}} | Tags : {{$post->category->category_name}}</p> -->
                <hr> 
                @php
                    $post_content = \App\Helpers\GlobalFunction::getStringLang($post->post_content, $lang);
                @endphp
                {!!$post_content!!}
            </div>
            <br><br><br>

 
<div id="share-buttons" align="center"> Share This Post to:  <br>
    
<a href="https://www.facebook.com/sharer/sharer.php?u=https://teaterkoma.mataduniadigital.com/post/{{$post->post_url}}" class="social-button " id=""><span class="fa fa-facebook"></span></a>  
<a href="https://twitter.com/intent/tweet?text=my share text&amp;url=https://teaterkoma.mataduniadigital.com/post/{{$post->post_url}}" class="social-button " id=""><span class="fa fa-twitter"></span></a>  
<a href="https://plus.google.com/share?url=https://teaterkoma.mataduniadigital.com/post/{{$post->post_url}}" class="social-button " id=""><span class="fa fa-google"></span></a>  
<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://teaterkoma.mataduniadigital.com/post/{{$post->post_url}}" class="social-button " id=""><span class="fa fa-linkedin"></span></a>    
<a href="https://wa.me/?text=https://teaterkoma.mataduniadigital.com/post/{{$post->post_url}}" class="social-button " id=""><span class="fa fa-whatsapp"></span></a>    

</div>
@endsection
<style>
.fa {
  padding: 20px;
  font-size: 30px;
  width: 50px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-android {
  background: #a4c639;
  color: white;
}

.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.fa-vimeo {
  background: #45bbff;
  color: white;
}

.fa-tumblr {
  background: #2c4762;
  color: white;
}

.fa-whatsapp {
  background: #00b489;
  color: white;
}

.fa-foursquare {
  background: #45bbff;
  color: white;
}

.fa-stumbleupon {
  background: #eb4924;
  color: white;
}

.fa-flickr {
  background: #f40083;
  color: white;
}

.fa-yahoo {
  background: #430297;
  color: white;
}

.fa-soundcloud {
  background: #ff5500;
  color: white;
}

.fa-reddit {
  background: #ff5700;
  color: white;
}

.fa-rss {
  background: #ff6600;
  color: white;
}
</style>