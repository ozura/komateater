$('.owl-carouselsatu').owlCarousel({
    loop:true,
    margin:10,
    nav:false,

    autoplay: 1000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1

        }
    }
});
$('.owl-carouseldua').owlCarousel({
    loop:true,
    margin:10,
 

    autoplay: false,
    nav:true,
     navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:5

        }
    }


});
var owl = $('.sliderWrapper');
owl.owlCarousel();
// Go to the next item
$('.nxtBtn').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.prvBtn').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
});