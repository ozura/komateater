<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Family
 */
class Family extends Model
{
    use SoftDeletes;
    
    protected $table = 'families';

    protected $primaryKey = 'family_id';

	public $timestamps = true;

    protected $fillable = [
        'family_order'
    ];

    protected $guarded = [];

}