<?php

namespace App\Helpers;

class GlobalFunction{
    static function getStringLang($string, $lang){
        $open_tag = '[['.$lang.']]';
        $close_tag = '[[\\'.$lang.']]';

        if(strpos($string, $open_tag) !== false && strpos($string, $close_tag) !== false){
            return GlobalFunction::get_string_between($string, $open_tag, $close_tag);
        }else{
            return $string;
        }
    }

    static function get_string_between($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);   
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }
   
}
