<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Tiket extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tikets';

    protected $primaryKey = 'tiket_id';

	public $timestamps = true;

    

}
