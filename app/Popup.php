<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Popup extends Model
{
    //
    use SoftDeletes;
    protected $table = 'popups';

    protected $primaryKey = 'popup_id';

	public $timestamps = true;

    

}
