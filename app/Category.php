<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 */
class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    protected $primaryKey = 'category_id';

    public $timestamps = true;
    
    protected $fillable = [
        'category_name',
        'is_editable'
    ];

    protected $guarded = [];

}