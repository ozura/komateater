<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Production extends Model
{
    //
    use SoftDeletes;
    protected $table = 'productions';

    protected $primaryKey = 'produksi_id';

	public $timestamps = true;

	public function scopeSearch($query, $s){
		return $query->where('judul_lakon','like','%' .$s. '%')
		->orWhere('awal_pentas','like','%' .$s. '%')
		->orWhere('tempat_pentas','like','%' .$s. '%');				
	}
    
    protected $dates = ['awal_pentas','akhir_pentas'];

}
