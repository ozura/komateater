<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Gallery
 */
class Gallery extends Model
{
    use SoftDeletes;
    
    protected $table = 'galleries';

    protected $primaryKey = 'gallery_id';

	public $timestamps = true;

    protected $fillable = [
        'gallery_name'
    ];

    protected $guarded = [];

    

    


}