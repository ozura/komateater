<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 */
class HVideo extends Model
{
    use SoftDeletes;
    
    protected $table = 'hvideos';

    protected $primaryKey = 'hvideo_id';

	public $timestamps = true;


    protected $guarded = [];

    

    


}