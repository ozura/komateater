<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 */
class HImage extends Model
{
    use SoftDeletes;
    
    protected $table = 'himages';

    protected $primaryKey = 'himage_id';

	public $timestamps = true;


    protected $guarded = [];

    

    


}