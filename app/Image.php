<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 */
class Image extends Model
{
    use SoftDeletes;
    
    protected $table = 'images';

    protected $primaryKey = 'image_id';

	public $timestamps = true;

    protected $fillable = [
        'image_name',
        'image_type',
        'image_url'
    ];

    protected $guarded = [];

    

    


}