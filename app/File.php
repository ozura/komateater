<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class File
 */
class File extends Model
{
    use SoftDeletes;
    
    protected $table = 'files';

    protected $primaryKey = 'file_id';

	public $timestamps = true;

    protected $fillable = [
        'file_name',
        'file_url'
    ];

    protected $guarded = [];

    

    


}