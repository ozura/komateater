<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class GalleryImage
 */
class GalleryImage extends Model
{
    use SoftDeletes;
    
    protected $table = 'gallery_images';

    protected $primaryKey = 'gallery_image_id';

	public $timestamps = true;

    protected $fillable = [
        'gallery_id',
        'image_id'
    ];

    protected $guarded = [];

    

    


}