<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Collection
 */
class Collection extends Model
{
    use SoftDeletes;
    
    protected $table = 'collections';

    protected $primaryKey = 'collection_id';

	public $timestamps = true;

    protected $fillable = [
        'collection_category_id',
        'name',
        'thumbnail_url',
        'target_url',
        'sequence',
        'keterangan'
    ];

    protected $guarded = [];

}