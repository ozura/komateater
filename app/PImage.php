<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 */
class PImage extends Model
{
    use SoftDeletes;
    
    protected $table = 'pimages';

    protected $primaryKey = 'pimage_id';

	public $timestamps = true;


    protected $guarded = [];

    

    


}