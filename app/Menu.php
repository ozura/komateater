<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Menu
 */
class Menu extends Model
{
    use SoftDeletes;

    protected $table = 'menus';

    protected $primaryKey = 'menu_id';

    public $timestamps = true;
    
    protected $fillable = [
        'menu_root',
        'menu_order',
        'category_id',
        'menu_text',
        'menu_url'
    ];

    protected $guarded = [];

}