<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ParentModel
 */
class ParentModel extends Model
{
    protected $table = 'parents';
    
    protected $primaryKey = 'parent_id';

    public $timestamps = true;
    
    protected $fillable = [
        'family_id',
        'person_id'
    ];

    protected $guarded = [];

}