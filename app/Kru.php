<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kru extends Model
{
    //
    protected $table = 'krus';
    protected $primaryKey = 'kru_id';

	public $timestamps = false;

	protected $fillable = [
        'nama',
        'biografi',
        'foto'
    ];
}
