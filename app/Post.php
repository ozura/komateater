<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 */
class Post extends Model
{
    use SoftDeletes;
    
    protected $table = 'posts';

    protected $primaryKey = 'post_id';

	public $timestamps = true;

    protected $fillable = [
        'category_id',
        'post_url',
        'post_title',
        'post_content',
        'post_image_url',
        'post_caption'
    ];

    protected $guarded = [];

    public function scopeSearch($query, $s){
        return $query->where('post_title','like','%' .$s. '%');
                 
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

}