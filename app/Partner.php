<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Collection
 */
class Partner extends Model
{
    use SoftDeletes;
    
    protected $table = 'partners';

    protected $primaryKey = 'partner_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
        'thumbnail_url',
        'target_url',
        'sequence',
        'keterangan'
    ];

    protected $guarded = [];

}