<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 */
class Video extends Model
{
    use SoftDeletes;
    
    protected $table = 'videos';

    protected $primaryKey = 'video_id';

	public $timestamps = true;

    protected $fillable = [
        'video_name',
        'video_url'
    ];

    protected $guarded = [];

    

    


}