<?php

namespace App\Providers;
use View;
use App\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
              $facebook = Setting::select('setting_content')->where('setting_id','2')->first();
         $twitter = Setting::select('setting_content')->where('setting_id','1')->first();
         $youtube = Setting::select('setting_content')->where('setting_id','4')->first();
         $instagram = Setting::select('setting_content')->where('setting_id','3')->first();
         $saddress = Setting::select('setting_content')->where('setting_id','9')->first();
        $smaps = Setting::select('setting_content')->where('setting_id','10')->first();

         
         View::share(['facebook' => $facebook, 'twitter' => $twitter,'youtube'=>$youtube,'instagram'=>$instagram,'saddress'=>$saddress,'smaps'=>$smaps]);
         
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
