<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Account
 */
class Account extends Authenticatable
{
    use SoftDeletes;
    
    protected $table = 'accounts';

    protected $primaryKey = 'account_id';

	public $timestamps = true;

    protected $fillable = [
        'username',
        'password',
        'role_id',
        'remember_token'
    ];

    protected $guarded = [];

    

    


}