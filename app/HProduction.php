<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HProduction extends Model
{
    //
    use SoftDeletes;
    protected $table = 'hproductions';

    protected $primaryKey = 'hproduksi_id';

	public $timestamps = true;

    protected $dates = ['awal_pentas','akhir_pentas'];

}
