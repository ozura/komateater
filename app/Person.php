<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Person
 */
class Person extends Model
{
    use SoftDeletes;
    
    protected $table = 'persons';

    protected $primaryKey = 'person_id';

	public $timestamps = true;

    protected $fillable = [
        'family_id',
        'person_name',
        'person_gender',
        'person_caption'
    ];

    protected $guarded = [];

}