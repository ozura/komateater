<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CollectionCategory
 */
class CollectionCategory extends Model
{
    use SoftDeletes;
    
    protected $table = 'collection_categories';

    protected $primaryKey = 'collection_category_id';

	public $timestamps = true;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

}