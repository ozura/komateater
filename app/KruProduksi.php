<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KruProduksi extends Model
{
    //
    protected $table = 'kru_produksi';
    protected $primaryKey = 'kru_produksi_id';

	public $timestamps = false;

	protected $fillable = [
        'role',
        'jenis'

    ];
}



