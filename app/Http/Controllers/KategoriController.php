<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use App\CollectionCategory;
use App\Collection;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class KategoriController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Merchandise', 'link' => 'collection']
        );

        return view('admin/pages/list-collection', compact('breadcrumb'));
    }

    
     public function commonList(Request $request){
        $list_data = CollectionCategory::select('collection_category_id','name')->get();
        // dd($list_data);

      // $pemain = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',1)->get();
        // ->groupBy('productions.judul_lakon')->distinct()

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->collection_category_id
                    );
                    return $data;
                })
                 // ->addColumn('tiket', function($item){
                    
                 //    if($item->tiket == 1){
                 //        return 'Ada';
                 //    }else{
                 //        return 'Tidak Ada ';
                 //    }
                ->make(true);
    }
      public function indexManage(Request $request, $id = 0){
        if($item = Collection::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Collection', 'link' => 'collection'],
                (object) ['name' => 'Update collection', 'link' => 'collection/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'merchandise', 'link' => 'merchandise'],
                (object) ['name' => 'New Merchandise', 'link' => 'merchandise/new']
            );
        }

        return view('admin/pages/manage-collection', compact('breadcrumb', 'item'));
    }
    public function actionSave2(Request $request){
        $input = (object) $request->input();
        
        $kategori = new CollectionCategory;
        $kategori->name = $input->kategori;

        
        
        if($kategori->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }



     public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($request->collection_id)){
            $collection = new Collection;
        }else{
            if($item = Collection::find($request->collection_id)){
                $collection = $item;
            }else{
                $collection = new Collection;
            }
        }

        $collection->collection_category_id = $input->category;
        $collection->name = $input->name;
        $collection->thumbnail_url = $input->foto;
        // dd($input->foto);
        $collection->keterangan = $input->keterangan;
        
        
        if($collection->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($item = CollectionCategory::find($input->kategori)){
            $collection = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($collection->delete()){
            return ['status' => 200, 'message' => 'Successfully deleted record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}