<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
// use App\Production;
use Yajra\Datatables\Datatables;
use App\KruProduksi;
use App\Kru;
use Auth;
use DB;
use Session;

class KruProduksiController extends Controller{
    
    public function indexList(Request $request ){
        $produksi_id = $request->input('produksi_id');
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'kruproduksi', 'link' => 'kruproduksi']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-kru-produksi', compact('breadcrumb','produksi_id'));
    }


    /* API */
    public function commonList(Request $request){
        $produksi_id = $request->input('produksi_id');
        $list_data = Kru::select('krus.kru_id','produksi_id','jenis','krus.nama','role')->join('kru_produksi', function ($join) use ($produksi_id) {$join->on('krus.kru_id', '=', 'kru_produksi.kru_id')->where('produksi_id', $produksi_id);})->orderBy('krus.nama')->get();
                            
        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->kru_id,
                        'produksi_id' => $item->produksi_id
                    );
                    return $data;
                })
                ->addColumn('jenistext', function($item){
                    if(empty($item->jenis)){
                        return 'tidak ikut';
                    }
                    if($item->jenis == 1){
                        return 'Pemain';
                    }else{
                        return 'Pekerja';
                    }
                })
                ->addColumn('roletext', function($item){
                    if(empty($item->role)){
                        return 'tidak ikut';
                    }else{
                        return $item->role;
                    }
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
       
        if($item = KruProduksi::where('kru_id', $input->kru_id)->where('produksi_id', $input->produksi_id)->where('jenis',$input->jenis)->first()){    

                 return ['status' => 201, 'message' => 'Duplikasi Data'];
            }
            
            
        
        $item = new KruProduksi;
        $item->kru_id  = $input->kru_id;
        $item->produksi_id       = $input->produksi_id;
        $item->jenis     = $input->jenis;
        $item->role     = $input->role;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

         if($item = KruProduksi::where('kru_id', $input->kru_id)->where('produksi_id', $input->produksi_id)->first()){    

                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        
        return ['status' => 201, 'message' => 'Operation error'];

    }
}