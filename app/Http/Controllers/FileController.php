<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\File;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Carbon\Carbon;
use Session;

class FileController extends Controller{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Files', 'link' => 'files']
        );

        return view('admin/pages/list-file', compact('breadcrumb'));
    }

    public function getFile($tanggal, $name_file){
        $name = 'public/file/'.$tanggal.'/'.$name_file;
        if (!Storage::exists($name)){
            return Response::make('File no found.', 404);
        }

        $file = Storage::get($name);
        $type = Storage::mimeType($name);
        $response = Response::make($file, 200)->header("Content-Type", $type);

        return $response;
    }

    public function commonList(Request $request){

        $list_data = File::select('file_id', 'file_url', 'file_name', 'updated_at')
                            ->orderby('updated_at', 'desc')
                            ->get();

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->file_id,
                        'url' => url('file').'/'.$item->file_url,
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        if ($request->hasFile('file_base')) {
            $tanggal = Carbon::today()->format('Y-m-d');
            $filename = uniqid().'.'.$request->file('file_base')->getClientOriginalExtension();
            $path = Storage::putFileAs('public/file/'.$tanggal, $request->file('file_base'), $filename);
            $path = str_replace('public/', 'storage/', $path);

            $file = new File;
            $file->file_name = $input->file_name;
            $file->file_url = $tanggal.'/'.$filename;
            $file->save();
            
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($item = File::find($input->file_id)){
            $file = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($file->delete()){
            $file = 'public/file/'.$file->file_url;
            if (!Storage::exists($file)){
                Storage::delete($file);
            }
            return ['status' => 200, 'message' => 'Successfully delete record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}