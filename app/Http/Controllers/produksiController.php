<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\Production;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class ProduksiController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Produksi', 'link' => 'produksi']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-produksi', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Production::find($id)){
            $awalpentas = $item->awal_pentas->format('Y-m-d');
            $akhirpentas = $item->akhir_pentas->format('Y-m-d');
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'Update produksi', 'link' => 'produksi/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'New produksi', 'link' => 'produksi/new']
            );
        }

        return view('admin/pages/manage-produksi', compact('breadcrumb', 'item','awalpentas','akhirpentas'));
    }

    /* API */
    public function commonList(Request $request){
        $list_data = Production::orderby('produksi_id', 'desc')
                            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->produksi_id
                    );
                    return $data;
                })
                 // ->addColumn('tiket', function($item){
                    
                 //    if($item->tiket == 1){
                 //        return 'Ada';
                 //    }else{
                 //        return 'Tidak Ada ';
                 //    }
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->produksi_id)){
            $item = new Production;
            $redirect = true;
        }else{
            if($item = Production::find($input->produksi_id)){                
                $redirect = false;
            }else{
                $item = new Production;
                $redirect = true;
            }
        }
        $awal = strtotime($input->akhir);
        $akhir =strtotime($input->awal);

        $datediff = $awal - $akhir;
        $lama = round($datediff / (60 * 60 * 24));

        $item->judul_lakon   = $input->judul_lakon;
        $item->foto       = $input->foto;
        $item->video       = $input->video_url;
        $item->awal_pentas       = $input->awal;
        $item->akhir_pentas       = $input->akhir;
        $item->tempat_pentas       = $input->tempat;
        $item->gallery_id       = $input->gallery_id;
        $item->sinopsis     = $input->sinopsis;
        $item->lama_pentas     = $lama;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = Production::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}