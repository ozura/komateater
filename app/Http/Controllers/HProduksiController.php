<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\HProduction;
use App\Production;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class HProduksiController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Produksi', 'link' => 'produksi']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-hproduksi', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Production::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'Update produksi', 'link' => 'produksi/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'New produksi', 'link' => 'produksi/new']
            );
        }

        return view('admin/pages/manage-produksi', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        $list_data = HProduction::select('productions.judul_lakon','hproductions.hproduksi_id')->leftJoin('productions','productions.produksi_id','=','hproductions.produksi_id')->where('productions.deleted_at',NULL)->orderBy('hproductions.updated_at','desc')->limit(6)->get();

      // $pemain = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',1)->get();
        // ->groupBy('productions.judul_lakon')->distinct()

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->hproduksi_id
                    );
                    return $data;
                })
                 // ->addColumn('tiket', function($item){
                    
                 //    if($item->tiket == 1){
                 //        return 'Ada';
                 //    }else{
                 //        return 'Tidak Ada ';
                 //    }
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->hproduksi_id)){
            $item = new HProduction;
            $redirect = true;
        }else{
            if($item = HProduction::where('produksi_id',$input->produksi_id)->first()){                
                $redirect = false;
            }else{
                $item = new HProduction;
                $redirect = true;
            }
        }
        // $item = new HProduction;
        // $item->hproduksi_id   = $input->produksi_id;
        $item->produksi_id   = $input->produksi_id;
        $item->flag   = rand();
        
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();
        
        if(!empty($input->id)){
            if($item = HProduction::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}