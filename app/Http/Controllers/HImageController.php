<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\HProduction;
use App\HImage;
use App\PImage;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class HImageController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Produksi', 'link' => 'produksi']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-hproduksi', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Production::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'Update produksi', 'link' => 'produksi/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'New produksi', 'link' => 'produksi/new']
            );
        }

        return view('admin/pages/manage-produksi', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        

      // $pemain = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',1)->get();
        // ->groupBy('productions.judul_lakon')->distinct()
        $list_data = HImage::select('images.image_url','images.image_name','himages.himage_id')->leftJoin('images','images.image_id','=','himages.image_id')->orderBy('himages.updated_at','desc')->limit(5)->get();
           // $list_data = Video::select('video_id', 'video_url', 'video_name', 'updated_at','tampilkan')
           //                  ->orderby('updated_at', 'desc')
           //                  ->get();
                            // dd($list_data);

        return Datatables::of($list_data)
                ->addColumn('image', function($item){
                    $data = array(
                        'src' => url('storage/img/origins').'/'.$item->image_url,
                        'alt' => $item->image_name,
                         'id' =>$item->himage_id,
                        'content' => $item
                    );
                    return $data;
                })
                
                ->make(true);

    }


     public function commonList2(Request $request){
        

      // $pemain = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',1)->get();
        // ->groupBy('productions.judul_lakon')->distinct()
        $list_data = PImage::select('images.image_url','images.image_name','pimages.pimage_id')->leftJoin('images','images.image_id','=','pimages.image_id')->orderBy('pimages.updated_at','desc')->limit(5)->get();
           // $list_data = Video::select('video_id', 'video_url', 'video_name', 'updated_at','tampilkan')
           //                  ->orderby('updated_at', 'desc')
           //                  ->get();
                            // dd($list_data);

        return Datatables::of($list_data)
                ->addColumn('image', function($item){
                    $data = array(
                        'src' => url('storage/img/origins').'/'.$item->image_url,
                        'alt' => $item->image_name,
                         'id' =>$item->pimage_id,
                        'content' => $item
                    );
                    return $data;
                })
                
                ->make(true);

    }

    public function actionSave(Request $request){

        $input = (object) $request->input();
        if(empty($input->image_id)){
            $item = new HImage;
            $redirect = true;
        }else{
            if($item = HImage::where('image_id',$input->image_id)->first()){                
                $redirect = false;
            }else{
                $item = new Himage;
                $redirect = true;
            }
        }
        // $item = new HProduction;
        // $item->hproduksi_id   = $input->produksi_id;
        $item->image_id   = $input->image_id;
        $item->flag   = rand();
        
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionSave2(Request $request){

        $input = (object) $request->input();
        if(empty($input->image_id)){
            $item = new PImage;
            $redirect = true;
        }else{
            if($item = PImage::where('image_id',$input->image_id)->first()){                
                $redirect = false;
            }else{
                $item = new PImage;
                $redirect = true;
            }
        }
        // $item = new HProduction;
        // $item->hproduksi_id   = $input->produksi_id;
        $item->image_id   = $input->image_id;
        $item->flag   = rand();
        
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();
        
        if(!empty($input->id)){
            if($item = HImage::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }

    public function actionDelete2(Request $request){
        $input = (object) $request->input();
        
        if(!empty($input->id)){
            if($item = PImage::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}