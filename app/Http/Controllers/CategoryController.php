<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;

use App\Category;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Validator;

class CategoryController extends Controller{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Categories', 'link' => 'categories']
        );

        return view('admin/pages/list-category', compact('breadcrumb'));
    }

    public function commonList(Request $request){
        $list_data = Category::select('category_id', 'category_name', 'is_editable')
                            ->get();

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->category_id,
                        'name' => $item->category_name,
                        'is_editable' => $item->is_editable
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        if(empty($input->category_id)){
            $category = new Category;
        }else{
            if($item = Category::find($input->category_id)){
                $category = $item;
            }else{
                $category = new Category;
            }
        }

        $validator = Validator::make($request->all(), [
            'category_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return ['status' => 201, 'message' => 'Not valid input'];
        }

        $category->category_name = $input->category_name;
        if($category->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(empty($input->category_id)){
            return ['status' => 201, 'message' => 'Operation error'];
        }else{
            if($item = Category::find($input->category_id)){
                $category = $item;
            }else{
                return ['status' => 201, 'message' => 'Operation error'];
            }
        }

        if($category->delete()){
            return 200;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}