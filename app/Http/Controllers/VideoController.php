<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\Helpers\UploadHandler;
use App\Video;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Carbon\Carbon;
use Validator;
use Session;

class VideoController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Videos', 'link' => 'video']
        );
        // dd('haloo');
        return view('admin/pages/list-videos', compact('breadcrumb'));
    }

    public function commonList(Request $request){
        // dd('haloo');
        $list_data = Video::select('video_id', 'video_url', 'video_name', 'updated_at','tampilkan')
                            ->orderby('updated_at', 'desc')
                            ->get();
                            // dd($list_data);

        return Datatables::of($list_data)
                ->addColumn('video', function($item){
                    $data = array(
                        'src' => $item->video_url,
                        'alt' => $item->video_name
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->video_id,
                        'content' => $item

                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        if(empty($input->video_id)){
            $item = new Video;
        }else{
            if($item = Video::find($input->video_id)){
                $item = $item;
            }else{
                $item = new Video;
            }
        }

        $validator = Validator::make($request->all(), [
            'video_name' => 'required|max:255',
            'video_url' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return ['status' => 201, 'message' => 'Not valid input'];
        }

        $item->video_url = $input->video_url;
        $item->video_name = $input->video_name;
        $item->tampilkan = $input->tampilkan;
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = Video::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
   

   
}