<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\Tiket;
use App\Popup;
use App\Production;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class PopupController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'tiket', 'link' => 'tiket']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-tiket', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Popup::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Popup', 'link' => 'popup'],
                (object) ['name' => 'Update popup', 'link' => 'popup/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'popup', 'link' => 'popup'],
                (object) ['name' => 'New popup', 'link' => 'popup/new']
            );
        }

        return view('admin/pages/popup', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        $list_data = Tiket::orderby('tiket_id', 'desc')
                            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->tiket_id
                    );
                    return $data;
                })
                 // ->addColumn('tiket', function($item){
                    
                 //    if($item->tiket == 1){
                 //        return 'Ada';
                 //    }else{
                 //        return 'Tidak Ada ';
                 //    }
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->popup_id)){
            $item = new Popup;
            $redirect = true;
        }else{
            if($item = Popup::find($input->popup_id)){                
                $redirect = false;
            }else{
                $item = new Popup;
                $redirect = true;
            }
        }
        
        $item->popup       = $input->popup;
        $item->nama       = $input->nama;
        $item->tampilkan       = $input->tampilkan;
        
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = Production::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}