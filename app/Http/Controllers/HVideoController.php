<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\HProduction;
use App\HVideo;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class HVideoController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Produksi', 'link' => 'produksi']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-hproduksi', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Production::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'Update produksi', 'link' => 'produksi/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Produksi', 'link' => 'produksi'],
                (object) ['name' => 'New produksi', 'link' => 'produksi/new']
            );
        }

        return view('admin/pages/manage-produksi', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        

      // $pemain = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',1)->get();
        // ->groupBy('productions.judul_lakon')->distinct()
        $list_data = HVideo::select('videos.video_url','videos.video_name')->leftJoin('videos','videos.video_id','=','hvideos.video_id')->orderBy('hvideos.updated_at','desc')->where('videos.deleted_at', null)->limit(6)->get();
           // $list_data = Video::select('video_id', 'video_url', 'video_name', 'updated_at','tampilkan')
           //                  ->orderby('updated_at', 'desc')
           //                  ->get();
                            // dd($list_data);

        return Datatables::of($list_data)
                ->addColumn('video', function($item){
                    $data = array(
                        'src' => $item->video_url,
                        'alt' => $item->video_name
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->video_id,
                        'content' => $item

                    );
                    return $data;
                })
                ->make(true);

    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->video_id)){
            $item = new HVideo;
            $redirect = true;
        }else{
            if($item = HVideo::where('video_id',$input->video_id)->first()){                
                $redirect = false;
            }else{
                $item = new HVideo;
                $redirect = true;
            }
        }
        // $item = new HProduction;
        // $item->hproduksi_id   = $input->produksi_id;
        $item->video_id   = $input->video_id;
        $item->flag   = rand();
        
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = HProduction::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}