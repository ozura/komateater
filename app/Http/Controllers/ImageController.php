<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\Helpers\UploadHandler;
use App\Image;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Carbon\Carbon;
use Session;

class ImageController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Images', 'link' => 'image']
        );

        return view('admin/pages/list-image', compact('breadcrumb'));
    }

    public function commonList(Request $request){
        $list_data = Image::select('image_id', 'image_url', 'image_name', 'updated_at')
                            ->orderby('updated_at', 'desc')
                            ->get();

        return Datatables::of($list_data)
                ->addColumn('image', function($item){
                    $data = array(
                        'src' => url('storage/img/thumbnails').'/'.$item->image_url,
                        'original' => url('storage/img/origins').'/'.$item->image_url,
                        'alt' => $item->image_name,
                        'date' => with(new Carbon($item->updated_at))->format('j-m-Y')
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->image_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        $tanggal = Carbon::today()->format('Y-m-d');
        $upload_handler = new UploadHandler;
        $upload_handler->setDateDir($tanggal);
        $info = $upload_handler->post();
        if(isset($info[0]->name) && (!isset($info[0]->error)))
        {
            $image = new Image;
            $name = substr($info[0]->name, 0 , strripos($info[0]->name, '.'));
            $path_parts = pathinfo($info[0]->name);
            $name = $path_parts['filename'];
            $type = $path_parts['extension'];
            $image->image_name = $name;
            $image->image_type = $type;
            $image->image_url = $tanggal."/".$name.".".$type;
            $image->save();
        }
        
        return back();
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($item = Image::find($input->image_id)){
            $image = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($image->delete()){
            if(file_exists('storage/img/origin/'.$image->image_url)){
                unlink('storage/img/origin/'.$image->image_url);
            }
            if(file_exists('storage/img/thumbnails/'.$image->image_url)){
                unlink('storage/img/thumbnails/'.$image->image_url);
            }
            return ['status' => 200, 'message' => 'Successfully delete record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionUploadImage(Request $request){
        $input = (object) $request->input();

        $tanggal = Carbon::today()->format('Y-m-d');
        $upload_handler = new UploadHandler;
        $upload_handler->setDateDir($tanggal);
        $info = $upload_handler->post();
        if(isset($info[0]->name) && (!isset($info[0]->error)))
        {
            $image = new Image;
            $name = substr($info[0]->name, 0 , strripos($info[0]->name, '.'));
            $path_parts = pathinfo($info[0]->name);
            $name = $path_parts['filename'];
            $type = $path_parts['extension'];
            $image->image_name = $name;
            $image->image_type = $type;
            $image->image_url = $tanggal."/".$name.".".$type;
            $image->save();
        }
        return url('storage/img/origins').'/'.$image->image_url;
    }
}