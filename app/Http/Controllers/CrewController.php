<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\Production;
use Yajra\Datatables\Datatables;
use App\Crew;
use Auth;
use DB;
use Session;

class CrewController extends Controller{
    
    public function indexList(Request $request ){
        $produksi_id = $request->input('produksi_id');
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Crew', 'link' => 'crew']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-crew', compact('breadcrumb','produksi_id'));
    }

    public function indexManage(Request $request, $id = 0){
        $produksi_id = $request->input('produksi_id');
        if($item = Production::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Crew', 'link' => 'crew'],
                (object) ['name' => 'Update crew', 'link' => 'crew/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Crew', 'link' => 'crew'],
                (object) ['name' => 'New crew', 'link' => 'crew/new']
            );
        }

        return view('admin/pages/manage-crew', compact('breadcrumb', 'item','produksi_id'));
    }

    /* API */
    public function commonList(Request $request){
        $produksi_id = $request->input('produksi_id');
        $list_data = Crew::where('produksi_id',$produksi_id)->orderby('crew_id', 'desc')
                            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->crew_id
                    );
                    return $data;
                })
                ->editColumn('jenis', function($item){
                    if($item->jenis == 1){
                        return 'Pemain';
                    }else{
                        return 'Pekerja';
                    }
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->crew_id)){
            $item = new Crew;
            $redirect = true;
        }else{
            if($item = Crew::find($input->crew_id)){                
                $redirect = false;
            }else{
                $item = new Crew;
                $redirect = true;
            }
        }
        
        $item->produksi_id = $input->produksi_id;
        $item->nama   = $input->nama;
        $item->jenis   = $input->jenis;
        $item->foto       = $input->foto;
        $item->role       = $input->role;
        $item->biografi     = $input->biografi;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = Production::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}