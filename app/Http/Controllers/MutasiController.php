<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

use App\Models\BARANG as Barang;
use App\Models\BRAND as Brand;
use App\Models\CARTDETAIL as CartDetail;
use App\Models\GUDANG as Gudang;
use App\Models\MUTASIBARANG as MutasiBarang;
use App\Models\MUTASIBARANGDETAIL as MutasiBarangDetail;
use App\Models\STOK as Stok;

use Carbon\Carbon;
use App\Libraries\HelperFunction;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Excel;
use Session;
use Validator;

class MutasiController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Mutasi Kolektif', 'link' => 'mutasi']
        );

        $gudang_gudang = Gudang::whereNotIn('ID_GUDANG', [2])->get();
        $brand_brand = Brand::get();

        return view('pages/list-mutasi', compact('breadcrumb', 'gudang_gudang', 'brand_brand'));
    }

    public function indexDailyNew(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Mutasi Daily', 'link' => 'mutasi-daily'],
            (object) ['name' => 'Mutasi Daily New', 'link' => 'mutasi-daily/new']
        );

        $gudang_gudang = Gudang::whereNotIn('ID_GUDANG', [2])->get();

        return view('pages/list-new-mutasi-daily', compact('breadcrumb', 'gudang_gudang'));
    }

    public function indexDailyList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Mutasi Daily', 'link' => 'mutasi-daily']
        );

        $gudang_gudang = Gudang::whereNotIn('ID_GUDANG', [2])->get();

        return view('pages/list-mutasi-daily', compact('breadcrumb', 'gudang_gudang'));
    }
    
    public function indexDetail(Request $request, $id = 0){
        if($mutasi_barang = MutasiBarang::find($id)){
            
        }else{
            return abort(404);
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Detail '.$mutasi_barang->NO_MUTASI_BARANG, 'link' => 'mutasi/detail'.$id],
        );
        
        return view('pages/list-mutasi-detail', compact('breadcrumb', 'mutasi_barang'));
    }
    
    public function indexReceiveList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Terima Mutasi Kolektif', 'link' => 'receive']
        );

        return view('pages/list-terima', compact('breadcrumb'));
    }

    public function indexReceiveDailyList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Terima Mutasi Daily', 'link' => 'receive-daily']
        );

        return view('pages/list-terima-daily', compact('breadcrumb'));
    }

    public function indexReceiveDetail(Request $request, $id = 0){
        if($mutasi_barang = MutasiBarang::find($id)){
            
        }else{
            return abort(404);
        }
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Terima Barang', 'link' => 'receive'],
            (object) ['name' => 'Detail '.$mutasi_barang->NO_MUTASI_BARANG, 'link' => 'receive/detail'.$id],
        );
        
        return view('pages/list-terima-detail', compact('breadcrumb', 'mutasi_barang'));
    }

    /* API */
    public function commonList(Request $request){
        $input = (object) $request->input();

        if(!empty($input->prefix)){
            $list_data = DB::select('SELECT 
                                    ID_MUTASI_BARANG, 
                                    NO_MUTASI_BARANG,
                                    NM_BRAND,
                                    G2.NM_GUDANG TO_GUDANG,
                                    TANGGAL_INPUT_ASAL,
                                    STATUS_MUTASI_BARANG,
                                    KETERANGAN_ASAL,
                                    KETERANGAN_TUJUAN
                                    FROM MUTASI_BARANG MB
                                    JOIN GUDANG G1 ON G1.ID_GUDANG = MB.ID_GUDANG_ASAL
                                    JOIN PENGGUNA P ON P.ID_PENGGUNA = MB.ID_PENGGUNA_ASAL
                                    JOIN GUDANG G2 ON G2.ID_GUDANG = MB.ID_GUDANG_TUJUAN
                                    JOIN BRAND BR ON BR.ID_BRAND = MB.ID_BRAND
                                    WHERE MB.ID_GUDANG_ASAL = 2
                                    AND MB.NO_MUTASI_BARANG LIKE "%'.$input->prefix.'%"
                                    ORDER BY TANGGAL_INPUT_ASAL DESC
                                    ');
        }else{
            $list_data = DB::select('SELECT 
                                    ID_MUTASI_BARANG, 
                                    NO_MUTASI_BARANG,
                                    NM_BRAND,
                                    G2.NM_GUDANG TO_GUDANG,
                                    TANGGAL_INPUT_ASAL,
                                    STATUS_MUTASI_BARANG,
                                    KETERANGAN_ASAL,
                                    KETERANGAN_TUJUAN
                                    FROM MUTASI_BARANG MB
                                    JOIN GUDANG G1 ON G1.ID_GUDANG = MB.ID_GUDANG_ASAL
                                    JOIN PENGGUNA P ON P.ID_PENGGUNA = MB.ID_PENGGUNA_ASAL
                                    JOIN GUDANG G2 ON G2.ID_GUDANG = MB.ID_GUDANG_TUJUAN
                                    JOIN BRAND BR ON BR.ID_BRAND = MB.ID_BRAND
                                    WHERE MB.ID_GUDANG_ASAL = 2
                                    ORDER BY TANGGAL_INPUT_ASAL DESC
                                    ');
        }

        return Datatables::of($list_data)
                ->editColumn('TANGGAL_INPUT_ASAL', function($item){
                    $status = 0;
                    if($item->STATUS_MUTASI_BARANG == 3){
                        $long_time = Carbon::parse($item->TANGGAL_INPUT_ASAL)->addMinutes(30);
                        if(strtotime(Carbon::now('Asia/Jakarta')) > strtotime($long_time)){
                            $status = 1;
                        }
                    }

                    $data = array(
                        'tanggal' => date_format(date_create($item->TANGGAL_INPUT_ASAL),"d M Y H:i").' WIB',
                        'status' => $status
                    );
                    return $data;
                })
                ->addColumn('STATUS', function($item){
                    $data = '';
                    switch($item->STATUS_MUTASI_BARANG){
                        case 1: $data = 'Belum input'; break;
                        case 2: $data = 'Sedang input barang'; break;
                        case 3: $data = 'Sudah Dikirim'; break;
                        case 4: $data = 'Sudah Diterima'; break;
                        case 10: $data = 'Cancel'; break;
                    }
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->ID_MUTASI_BARANG
                    );
                    return $data;
                })
                ->make(true);
    }

    public function commonReceiveList(Request $request){
        $input = (object) $request->input();

        if(!empty($input->prefix)){
            $list_data = DB::select('SELECT 
                                        ID_MUTASI_BARANG, 
                                        NO_MUTASI_BARANG,
                                        G1.NM_GUDANG FROM_GUDANG,
                                        TANGGAL_KIRIM_ASAL,
                                        STATUS_MUTASI_BARANG,
                                        KETERANGAN_ASAL,
                                        KETERANGAN_TUJUAN,
                                        NM_BRAND,
                                        IF(STATUS_MUTASI_BARANG=3, 1, 0) AS URUTAN
                                        FROM MUTASI_BARANG MB
                                        JOIN GUDANG G1 ON G1.ID_GUDANG = MB.ID_GUDANG_ASAL
                                        JOIN PENGGUNA P ON P.ID_PENGGUNA = MB.ID_PENGGUNA_ASAL
                                        JOIN GUDANG G2 ON G2.ID_GUDANG = MB.ID_GUDANG_TUJUAN
                                        JOIN BRAND BR ON BR.ID_BRAND = MB.ID_BRAND
                                        WHERE MB.ID_GUDANG_TUJUAN = 2
                                        AND MB.NO_MUTASI_BARANG LIKE "%'.$input->prefix.'%"
                                        AND MB.STATUS_MUTASI_BARANG > 2
                                        ORDER BY URUTAN DESC
                                        ');
        }else{
            $list_data = DB::select('SELECT 
                                        ID_MUTASI_BARANG, 
                                        NO_MUTASI_BARANG,
                                        G1.NM_GUDANG FROM_GUDANG,
                                        TANGGAL_KIRIM_ASAL,
                                        STATUS_MUTASI_BARANG,
                                        KETERANGAN_ASAL,
                                        KETERANGAN_TUJUAN,
                                        NM_BRAND,
                                        IF(STATUS_MUTASI_BARANG=3, 1, 0) AS URUTAN
                                        FROM MUTASI_BARANG MB
                                        JOIN GUDANG G1 ON G1.ID_GUDANG = MB.ID_GUDANG_ASAL
                                        JOIN PENGGUNA P ON P.ID_PENGGUNA = MB.ID_PENGGUNA_ASAL
                                        JOIN GUDANG G2 ON G2.ID_GUDANG = MB.ID_GUDANG_TUJUAN
                                        JOIN BRAND BR ON BR.ID_BRAND = MB.ID_BRAND
                                        WHERE MB.ID_GUDANG_TUJUAN = 2
                                        AND MB.STATUS_MUTASI_BARANG > 2
                                        ORDER BY URUTAN DESC
                                        ');
        }

        return Datatables::of($list_data)
                ->editColumn('TANGGAL_KIRIM_ASAL', function($item){
                    return date_format(date_create($item->TANGGAL_KIRIM_ASAL),"d M Y H:i").' WIB';
                })
                ->addColumn('STATUS', function($item){
                    $data = '';
                    switch($item->STATUS_MUTASI_BARANG){
                        case 1: $data = 'Belum input'; break;
                        case 2: $data = 'Sedang input barang'; break;
                        case 3: $data = 'Sudah Dikirim'; break;
                        case 4: $data = 'Sudah Diterima'; break;
                        case 10: $data = 'Cancel'; break;
                    }
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->ID_MUTASI_BARANG
                    );
                    return $data;
                })
                ->make(true);
    }

    public function barangAllList(Request $request){
        $input = (object) $request->input();

        $list_data = Barang::select('BARANG.ID_BARANG', 'STOK.ID_SIZE_DETAIL', 'BARANG.KODE_BARANG', 'NM_BRAND', 'JENIS_BARANG.NM_JENIS_BARANG', 'SIZE_DETAIL.NM_SIZE_DETAIL', 'STOK_PENDING_ENDORSE', 'STOK_AKHIR', 'BARANG.HARGA_MASUK_BARANG')
                            ->join('JENIS_BARANG', 'BARANG.ID_JENIS_BARANG', '=', 'JENIS_BARANG.ID_JENIS_BARANG')
                            ->join('BRAND', 'BARANG.ID_BRAND', '=', 'BRAND.ID_BRAND')
                            ->join('STOK', 'BARANG.ID_BARANG', '=', 'STOK.ID_BARANG')
                            ->join('SIZE_DETAIL', 'STOK.ID_SIZE_DETAIL', '=', 'SIZE_DETAIL.ID_SIZE_DETAIL')
                            ->where('ID_GUDANG', 2)
                            ->whereRaw('STOK_AKHIR - STOK_PENDING_ENDORSE > 0');

        if(!empty($input->brand)){
            $list_data = $list_data->where('BARANG.ID_BRAND', $input->brand);
        }

        return Datatables::of($list_data)
                ->addColumn('STOK', function($item){
                    return $item->STOK_AKHIR - $item->STOK_PENDING_ENDORSE;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->ID_BARANG,
                        'size' => $item->ID_SIZE_DETAIL
                    );
                    return $data;
                })
                ->make(true);
    }

    public function barangCheckList(Request $request, $id = 0){
        $input = (object) $request->input();

        $list_data = MutasiBarangDetail::select('MUTASI_BARANG_DETAIL.ID_MUTASI_BARANG_DETAIL', 'MUTASI_BARANG_DETAIL.QTY', 'MUTASI_BARANG_DETAIL.KONDISI_BARANG', 'BARANG.ID_BARANG', 'BARANG.KODE_BARANG', 'NM_BRAND', 'JENIS_BARANG.NM_JENIS_BARANG', 'SIZE_DETAIL.NM_SIZE_DETAIL', 'BARANG.HARGA_MASUK_BARANG')
                            ->join('BARANG', 'BARANG.ID_BARANG', '=', 'MUTASI_BARANG_DETAIL.ID_BARANG')
                            ->join('JENIS_BARANG', 'BARANG.ID_JENIS_BARANG', '=', 'JENIS_BARANG.ID_JENIS_BARANG')
                            ->join('BRAND', 'BARANG.ID_BRAND', '=', 'BRAND.ID_BRAND')
                            ->join('SIZE_DETAIL', 'MUTASI_BARANG_DETAIL.ID_SIZE_DETAIL', '=', 'SIZE_DETAIL.ID_SIZE_DETAIL')
                            ->where('ID_MUTASI_BARANG', $id)
                            ->get();

        return Datatables::of($list_data)
                ->addColumn('HARGA', function($item){
                    return 'Rp'.number_format($item->HARGA_MASUK_BARANG);
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->ID_MUTASI_BARANG_DETAIL,
                        'status' => $item->KONDISI_BARANG
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionNew(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_GUDANG_TUJUAN' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();
        $today = Carbon::now('Asia/Jakarta');
        
        $mutasi_barang = new MutasiBarang;
        $mutasi_barang->ID_GUDANG_ASAL = 2;
        $mutasi_barang->ID_BRAND = $input->ID_BRAND;
        $mutasi_barang->ID_PENGGUNA_ASAL = Auth::user()->ID_PENGGUNA;
        $mutasi_barang->TANGGAL_INPUT_ASAL = Carbon::now('Asia/Jakarta');
        $mutasi_barang->KETERANGAN_ASAL = $input->KETERANGAN_ASAL;
        $mutasi_barang->ID_GUDANG_TUJUAN = $input->ID_GUDANG_TUJUAN;
        $mutasi_barang->STATUS_MUTASI_BARANG = 1;
        $mutasi_barang->ASAL_MUTASI_BARANG = 1;
        $mutasi_barang->save();
        
        $mutasi_barang->NO_MUTASI_BARANG = 'MBK'.date_format(date_create($today),"Ymd").$mutasi_barang->ID_MUTASI_BARANG;
        $mutasi_barang->save();
        
        return ['status' => 200, 'message' => 'Success!'];
    }

    public function actionAddDetail(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG' => 'required',
            'ID_BARANG' => 'required',
            'ID_SIZE_DETAIL' => 'required',
            'QTY' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();
        
        if($stok_barang = Stok::where(['ID_BARANG' => $input->ID_BARANG, 'ID_SIZE_DETAIL' => $input->ID_SIZE_DETAIL, 'ID_GUDANG' => 2])->first()){
            $sisa = $stok_barang->STOK_AKHIR - $stok_barang->STOK_PENDING_ENDORSE;
            if($input->QTY <= 0){
                return ['status' => 201, 'message' => 'Error'];
            }
            if(($sisa - $input->QTY) < 0){
                return ['status' => 201, 'message' => 'Error'];
            }else{
                $stok_barang->STOK_PENDING_ENDORSE = $stok_barang->STOK_PENDING_ENDORSE + $input->QTY;
                $stok_barang->save();

                $mutasi_barang_detail = new MutasiBarangDetail;
                $mutasi_barang_detail->ID_MUTASI_BARANG = $input->ID_MUTASI_BARANG;
                $mutasi_barang_detail->ID_BARANG = $input->ID_BARANG;
                $mutasi_barang_detail->ID_SIZE_DETAIL = $input->ID_SIZE_DETAIL;
                $mutasi_barang_detail->QTY = $input->QTY;
                $mutasi_barang_detail->KONDISI_BARANG = 1;
                $mutasi_barang_detail->save();

                if($mutasi_barang = MutasiBarang::where(['ID_MUTASI_BARANG' => $input->ID_MUTASI_BARANG, 'STATUS_MUTASI_BARANG' => 1])->first()){
                    $mutasi_barang->STATUS_MUTASI_BARANG = 2;
                    $mutasi_barang->save();
                }

                return ['status' => 200, 'message' => 'Success!'];
            }
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
        
    }

    public function actionSendDaily(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_BARANG' => 'required',
            'ID_SIZE_DETAIL' => 'required',
            'ID_GUDANG_TUJUAN' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();
        
        if($stok_barang = Stok::where(['ID_BARANG' => $input->ID_BARANG, 'ID_SIZE_DETAIL' => $input->ID_SIZE_DETAIL, 'ID_GUDANG' => 2])->first()){
            $sisa = $stok_barang->STOK_AKHIR - $stok_barang->STOK_PENDING_ENDORSE;
            if(($sisa - 1) < 0){
                return ['status' => 201, 'message' => 'Error'];
            }else{
                $today = Carbon::now('Asia/Jakarta');

                $barang = Barang::find($input->ID_BARANG);
        
                $mutasi_barang = new MutasiBarang;
                $mutasi_barang->ID_GUDANG_ASAL = 2;
                $mutasi_barang->ID_BRAND = $barang->ID_BRAND;
                $mutasi_barang->ID_PENGGUNA_ASAL = Auth::user()->ID_PENGGUNA;
                $mutasi_barang->TANGGAL_INPUT_ASAL = Carbon::now('Asia/Jakarta');
                $mutasi_barang->TANGGAL_KIRIM_ASAL = Carbon::now('Asia/Jakarta');
                $mutasi_barang->KETERANGAN_ASAL = 'Kode Barang: '.$barang->KODE_BARANG;
                $mutasi_barang->ID_GUDANG_TUJUAN = $input->ID_GUDANG_TUJUAN;
                $mutasi_barang->STATUS_MUTASI_BARANG = 3;
                $mutasi_barang->ASAL_MUTASI_BARANG = 1;
                $mutasi_barang->save();
                
                $mutasi_barang->NO_MUTASI_BARANG = 'MBD'.date_format(date_create($today),"Ymd").$mutasi_barang->ID_MUTASI_BARANG;
                $mutasi_barang->save();

                $stok_barang->STOK_PENDING_ENDORSE = $stok_barang->STOK_PENDING_ENDORSE + 1;
                $stok_barang->save();

                $mutasi_barang_detail = new MutasiBarangDetail;
                $mutasi_barang_detail->ID_MUTASI_BARANG = $mutasi_barang->ID_MUTASI_BARANG;
                $mutasi_barang_detail->ID_BARANG = $input->ID_BARANG;
                $mutasi_barang_detail->ID_SIZE_DETAIL = $input->ID_SIZE_DETAIL;
                $mutasi_barang_detail->QTY = 1;
                $mutasi_barang_detail->KONDISI_BARANG = 1;
                $mutasi_barang_detail->save();

                return ['status' => 200, 'message' => 'Success!'];
            }
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
        
    }

    public function actionDeleteDetail(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG_DETAIL' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();

        if($mutasi_barang_detail = MutasiBarangDetail::find($input->ID_MUTASI_BARANG_DETAIL)){
            if($stok_barang = Stok::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 2])->first()){
                $stok_barang->STOK_PENDING_ENDORSE = $stok_barang->STOK_PENDING_ENDORSE - $mutasi_barang_detail->QTY;
                $stok_barang->save();

                $mutasi_barang_detail->delete();
                return ['status' => 200, 'message' => 'Success!'];
            }else{
                return ['status' => 201, 'message' => 'Error'];
            }
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
        
    }

    public function actionSend(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();

        if($mutasi_barang = MutasiBarang::where(['ID_MUTASI_BARANG' => $input->ID_MUTASI_BARANG, 'STATUS_MUTASI_BARANG' => 2])->first()){
            $mutasi_barang->STATUS_MUTASI_BARANG = 3;
            $mutasi_barang->TANGGAL_KIRIM_ASAL = Carbon::now('Asia/Jakarta');
            $mutasi_barang->save();

            return ['status' => 200, 'message' => 'Success!'];
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
    }

    public function actionCheckBarang(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG_DETAIL' => 'required',
            'STATUS' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();

        if($mutasi_barang_detail = MutasiBarangDetail::find($input->ID_MUTASI_BARANG_DETAIL)){
            $mutasi_barang_detail->KONDISI_BARANG = $input->STATUS;
            $mutasi_barang_detail->save();

            return ['status' => 200, 'message' => 'Success!'];
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
    }

    public function actionApproveYes(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();
        
        if($mutasi_barang = MutasiBarang::where(['ID_MUTASI_BARANG' => $input->ID_MUTASI_BARANG, 'STATUS_MUTASI_BARANG' => 3])->first()){
            $mutasi_barang_details = MutasiBarangDetail::where(['ID_MUTASI_BARANG' => $mutasi_barang->ID_MUTASI_BARANG])->get();
            foreach($mutasi_barang_details as $mutasi_barang_detail){
                if($mutasi_barang_detail->KONDISI_BARANG == 1){
                    return ['status' => 201, 'message' => 'Check kondisi barang lebih dahulu'];
                }
            }

            foreach($mutasi_barang_details as $mutasi_barang_detail){
                // Gudang Asal
                if($stok_barang_gudang = Stok::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 1])->first()){
                    $stok_barang_gudang->STOK_PENDING_ENDORSE = $stok_barang_gudang->STOK_PENDING_ENDORSE - $mutasi_barang_detail->QTY;
                    $stok_barang_gudang->STOK_KELUAR = $stok_barang_gudang->STOK_KELUAR + $mutasi_barang_detail->QTY;
                    $stok_barang_gudang->STOK_AKHIR = $stok_barang_gudang->STOK_AKHIR - $mutasi_barang_detail->QTY;
                    $stok_barang_gudang->save();
                }
                
                // Gudang Tujuan
                if($stok_barang = Stok::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 2])->first()){
                    $stok_barang->STOK_MASUK = $stok_barang->STOK_MASUK + $mutasi_barang_detail->QTY;
                    $stok_barang->STOK_AKHIR = $stok_barang->STOK_AKHIR + $mutasi_barang_detail->QTY;
                    $stok_barang->save();
                }else{
                    $stok_barang = new Stok;
                    $stok_barang->ID_GUDANG = 2;
                    $stok_barang->ID_BARANG = $mutasi_barang_detail->ID_BARANG;
                    $stok_barang->ID_SIZE_DETAIL = $mutasi_barang_detail->ID_SIZE_DETAIL;
                    $stok_barang->STOK_MASUK = $mutasi_barang_detail->QTY;
                    $stok_barang->STOK_AKHIR = $mutasi_barang_detail->QTY;
                    $stok_barang->save();
                }
            }
            $mutasi_barang->STATUS_MUTASI_BARANG = 4;
            // $mutasi_barang->KETERANGAN_TUJUAN = $input->KETERANGAN_TUJUAN;
            $mutasi_barang->TANGGAL_APPROVE_TUJUAN = Carbon::now('Asia/Jakarta');
            $mutasi_barang->save();

            return ['status' => 200, 'message' => 'Success!'];
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
    }

    public function actionApproveYesAndAddToCart(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();
        $pengguna = Auth::user();

        if($mutasi_barang = MutasiBarang::where(['ID_MUTASI_BARANG' => $input->ID_MUTASI_BARANG, 'STATUS_MUTASI_BARANG' => 3])->first()){
            $mutasi_barang_details = MutasiBarangDetail::where(['ID_MUTASI_BARANG' => $mutasi_barang->ID_MUTASI_BARANG])->get();
            foreach($mutasi_barang_details as $mutasi_barang_detail){
                if($mutasi_barang_detail->KONDISI_BARANG == 1){
                    return ['status' => 201, 'message' => 'Check kondisi barang lebih dahulu'];
                }

                if($cart_detail = CartDetail::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_PENGGUNA' => $pengguna->ID_PENGGUNA, 'ID_GUDANG' => 2])->first()){
                    return ['status' => 201, 'message' => 'Hapus barang yang sama di halaman kasir terlebih dahulu'];
                }
            }

            foreach($mutasi_barang_details as $mutasi_barang_detail){
                // Gudang Asal
                if($stok_barang_gudang = Stok::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 1])->first()){
                    $stok_barang_gudang->STOK_PENDING_ENDORSE = $stok_barang_gudang->STOK_PENDING_ENDORSE - $mutasi_barang_detail->QTY;
                    $stok_barang_gudang->STOK_KELUAR = $stok_barang_gudang->STOK_KELUAR + $mutasi_barang_detail->QTY;
                    $stok_barang_gudang->STOK_AKHIR = $stok_barang_gudang->STOK_AKHIR - $mutasi_barang_detail->QTY;
                    $stok_barang_gudang->save();
                }
                
                // Gudang Tujuan
                if($stok_barang = Stok::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 2])->first()){
                    $stok_barang->STOK_MASUK = $stok_barang->STOK_MASUK + $mutasi_barang_detail->QTY;
                    $stok_barang->STOK_AKHIR = $stok_barang->STOK_AKHIR + $mutasi_barang_detail->QTY;
                    $stok_barang->save();
                }else{
                    $stok_barang = new Stok;
                    $stok_barang->ID_GUDANG = 2;
                    $stok_barang->ID_BARANG = $mutasi_barang_detail->ID_BARANG;
                    $stok_barang->ID_SIZE_DETAIL = $mutasi_barang_detail->ID_SIZE_DETAIL;
                    $stok_barang->STOK_MASUK = $mutasi_barang_detail->QTY;
                    $stok_barang->STOK_AKHIR = $mutasi_barang_detail->QTY;
                    $stok_barang->save();
                }
            }
            $mutasi_barang->STATUS_MUTASI_BARANG = 4;
            // $mutasi_barang->KETERANGAN_TUJUAN = $input->KETERANGAN_TUJUAN;
            $mutasi_barang->TANGGAL_APPROVE_TUJUAN = Carbon::now('Asia/Jakarta');
            $mutasi_barang->save();

            /* START add to Cart with mutation Flag */
            if($barang = Barang::find($mutasi_barang_detail->ID_BARANG)){
            
            }else{
                return ['status' => 201, 'message' => 'Product not found'];
            }

            $cart_detail = new CartDetail;
            $cart_detail->ID_PENGGUNA = $pengguna->ID_PENGGUNA;
            $cart_detail->ID_BARANG = $mutasi_barang_detail->ID_BARANG;
            $cart_detail->ID_SIZE_DETAIL   = $mutasi_barang_detail->ID_SIZE_DETAIL;
            $cart_detail->QTY = 1;
            $cart_detail->HARGA_MASUK_BARANG = $barang->HARGA_MASUK_BARANG;
            $cart_detail->HARGA_AKHIR = HelperFunction::getFinalPrice($mutasi_barang_detail->ID_BARANG);
            $cart_detail->ID_GUDANG = 2;
            $cart_detail->IS_FROM_MUTASI = 1;

            if($cart_detail->save()){
                return ['status' => 200, 'message' => 'Product added!'];
            }else{
                return ['status' => 201, 'message' => 'Operation error'];
            }

            /* END add to Cart with mutation Flag */

            return ['status' => 200, 'message' => 'Success!'];
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
    }

    public function actionApproveNo(Request $request){
        $validator = Validator::make($request->all(), [
            'ID_MUTASI_BARANG' => 'required'
        ]);
    
        if ($validator->fails()) {
            return ['status' => 201, 'message' => $validator->errors()->first()];
        }

        $input = (object) $request->input();
        
        if($mutasi_barang = MutasiBarang::where(['ID_MUTASI_BARANG' => $input->ID_MUTASI_BARANG, 'STATUS_MUTASI_BARANG' => 3])->first()){
            $mutasi_barang_details = MutasiBarangDetail::where(['ID_MUTASI_BARANG' => $mutasi_barang->ID_MUTASI_BARANG])->get();
            foreach($mutasi_barang_details as $mutasi_barang_detail){
                if($mutasi_barang_detail->KONDISI_BARANG == 1){
                    return ['status' => 201, 'message' => 'Check kondisi barang lebih dahulu'];
                }
            }

            foreach($mutasi_barang_details as $mutasi_barang_detail){
                // Gudang Asal
                if($stok_barang = Stok::where(['ID_BARANG' => $mutasi_barang_detail->ID_BARANG, 'ID_SIZE_DETAIL' => $mutasi_barang_detail->ID_SIZE_DETAIL, 'ID_GUDANG' => 1])->first()){
                    $stok_barang->STOK_PENDING_ENDORSE = $stok_barang->STOK_PENDING_ENDORSE - $mutasi_barang_detail->QTY;
                    /* FATAL */
                    // $stok_barang->STOK_AKHIR = $stok_barang->STOK_AKHIR + $mutasi_barang_detail->QTY;
                    $stok_barang->save();
                }
            }
            $mutasi_barang->STATUS_MUTASI_BARANG = 10;
            // $mutasi_barang->KETERANGAN_TUJUAN = $input->KETERANGAN_TUJUAN;
            $mutasi_barang->TANGGAL_APPROVE_TUJUAN = Carbon::now('Asia/Jakarta');
            $mutasi_barang->save();

            return ['status' => 200, 'message' => 'Success!'];
        }else{
            return ['status' => 201, 'message' => 'Error'];
        }
    }
}