<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\GalleryImage;
use App\Video;
use App\Production;
use App\Crew;
use App\Kru;
use App\Setting;
use App\Collection;
use App\CollectionCategory;
use App\KruProduksi;
use DB;



class indexController extends Controller
{
    //
      public function showProduksi(){
    	return view('produksi');
    }
    public function showDaftarProduksi(){
      // $urut = produksi::paginate(1)->orderBy('id', 'DESC')->get();

      $daftar = Production::orderBy('awal_pentas','desc')->paginate(20);
      // dd($urut);

      $s =null;
      // $daftar = produksi::paginate(1);
      // dd($daftar);
      return view('daftarproduksi',['daftar'=>$daftar,'s'=>$s]);
    }

    public function showDetailProduksi($id){
      
      // $crew = Crew::where('produksi_id',$id)->get();
      // $pemain =Crew::where('produksi_id',$id)->where('jenis',1) ->get();
      // $pekerja =Crew::where('produksi_id',$id)->where('jenis',2) ->get();
      $gamid = Production::select('gallery_id')->where('produksi_id',$id)->pluck('gallery_id');
      // dd($gamid);

      $pemain = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',1)->whereNotNull('nama')->get();


      $pekerja = KruProduksi::select('kru_produksi.role','kru_produksi.jenis','krus.nama','krus.kru_id')->leftJoin('krus','krus.kru_id','=','kru_produksi.kru_id')->where('produksi_id',$id)->where('jenis',2)->whereNotNull('nama')->get();
      
      $gambar = GalleryImage::select('gallery_image_id', 'images.image_id', 'images.image_url', 'images.image_name', 'images.updated_at')
                        ->join('images', 'gallery_images.image_id', '=', 'images.image_id')
                        ->where('gallery_images.gallery_id', $gamid)
                        ->orderby('updated_at', 'desc')->limit(10)
                        ->get();
      // dd($gambar);
      
      $produksi = Production::where('produksi_id',$id)->first();


      // dd($produksi);

    	return view('detailproduksi', compact('pemain','pekerja','produksi','gambar'));
    }


     public function showFolderFoto(){
        return view('folderfoto');
    }
    public function showFolderId(){
        return view('folderid');
    }
    public function showDaftarVideo(){

      $video = Video::select('video_name','video_url')->orderBy('video_id','desc')->get();
      // dd($video);
      return view('daftarvideo',['video'=>$video]);
    }

    public function showDaftarBerita(){
       $news_posts = Post::select('post_id', 'post_title', 'post_url', 'post_caption', 'post_image_url', 'created_at')->where('category_id', 1)->limit(10)->orderBy('created_at', 'desc')->paginate(8);  
      return view('daftarberita',['news_posts'=>$news_posts]);
    }

      public function showDaftarKataKoma(){
       $katakoma = Post::select('post_id', 'post_title', 'post_url', 'post_caption', 'post_image_url', 'created_at')->where('category_id', 5)->limit(10)->orderBy('created_at', 'desc')->paginate(8);  
      return view('daftarkatakoma',['katakoma'=>$katakoma]);
    }


    public function showDetailProfil($id){
      // dd($id);
       // $profile = Crew::where('crew_id',$id)->first();

       $profile = Kru::where('kru_id',$id)->first();
       // dd($profile);

       return view('detailprofil',['profile'=>$profile]);
    }

    public function showDaftarPemain() {
        
    }

    public function showDaftarPekerja() {

    }

    public function showDaftarMerchandise() {

      $smerchandise = Setting::select('setting_name','description','setting_content')->where('setting_id','12')->first();
      $cmerchandise = CollectionCategory::get();
      $merchandise = Collection::get();

      return view('daftarmerchandise',compact('smerchandise','cmerchandise','merchandise'));
    }

    public function showPemainPekerja(){
      
      // $crew = Crew::where('produksi_id',$id)->get();
      // $pemain =Crew::where('produksi_id',$id)->where('jenis',1) ->get();
      // $pekerja =Crew::where('produksi_id',$id)->where('jenis',2) ->get();


      $pemain = Kru::where('pemain',1)->get();


      $pekerja = Kru::where('pekerja',1)->paginate(25);

      // dd($produksi);

      return view('pemainpekerja', compact('pemain','pekerja'));
    }

    public function showDaftarPendiri(){
      $pendiri = Kru::where('pendiri',1)->get();

      return view('daftarpendiri',['pendiri' => $pendiri]);
    }


        public function searchDaftarProduksi(Request $request){
      // $urut = produksi::paginate(1)->orderBy('id', 'DESC')->get();
          $s = $request->input('s');
          $tahun = $request->input('tahun');
          
          if(!empty($s)){
          $daftar = Production::latest()
          ->search($s)
          ->paginate(10);
          }
          elseif(!empty(request('abjad'))){
            $daftar = Production::orderBy('judul_lakon','asc')->paginate(10)
            ->appends('abjad',request('abjad'));
          }
          elseif(!empty($tahun)) {
            $daftar = Production::whereYear('awal_pentas',$tahun)->paginate(10)
            ->appends('tahun',request('tahun'));
            
          }
          else{
            $daftar = Production::whereYear('awal_pentas',0)->paginate(10)
            ->appends('tahun',request('tahun'));
          }
      // $daftar = Production::orderBy('produksi_id','asc')->paginate(2);
      // dd($urut);
        // dd($daftar);
      // $daftar = produksi::paginate(1);
      // dd($daftar);
      return view('daftarproduksi',compact('daftar','s'));
    }

    public function searchDaftarKataKoma(Request $request){
      // $urut = produksi::paginate(1)->orderBy('id', 'DESC')->get();
          $s = $request->input('s');
          
          
          if(!empty($s)){
          $katakoma = Post::latest()
          ->search($s)
          ->paginate(10);
          }else{
            $katakoma = null;
          }
         
      // $katakoma = Production::orderBy('produksi_id','asc')->paginate(2);
      // dd($urut);
        // dd($katakoma);
      // $katakoma = produksi::paginate(1);
      // dd($katakoma);
      return view('daftarkatakoma',compact('katakoma','s'));
    }



        public function searchDaftarAnggota(Request $request){
      // $urut = produksi::paginate(1)->orderBy('id', 'DESC')->get();
          
          if(!empty(request('abjad'))){
            $pekerja = Kru::where('pekerja',1)->orderBy('nama','asc')->paginate(25)
            ->appends('abjad',request('abjad'));
          }else{
            $pekerja = null;
          }
          
      return view('pemainpekerja',compact('pekerja'));
    }


    public function detailMerchandise(Request $request, $id){

      $merchandise= Collection::where('collection_id',$id)->first();
      // dd($merchandise);
        return view('detailmerchandise',['merchandise'=>$merchandise]);
    }


    
}



