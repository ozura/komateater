<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
// use App\Production;
use Yajra\Datatables\Datatables;
use App\Kru;
use Auth;
use DB;
use Session;

class KruController extends Controller{
    
    public function indexList(Request $request ){
        $produksi_id = $request->input('produksi_id');
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Kru', 'link' => 'kru']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-kru', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        
        if($item = Kru::find($id)){ 
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Kru', 'link' => 'kru'],
                (object) ['name' => 'Update kru', 'link' => 'kru/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Kru', 'link' => 'kru'],
                (object) ['name' => 'New kru', 'link' => 'kru/new']
            );
        }

        return view('admin/pages/manage-kru', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        
        $list_data = kru::orderby('kru_id', 'desc')
                            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->kru_id
                    );
                    return $data;
                })
                ->editColumn('jenis', function($item){
                    if($item->jenis == 1){
                        return 'Pemain';
                    }else{
                        return 'Pekerja';
                    }
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->kru_id)){
            $item = new kru;
            $redirect = true;
        }else{
            if($item = kru::find($input->kru_id)){                
                $redirect = false;
            }else{
                $item = new kru;
                $redirect = true;
            }
        }
        
        
        $item->nama   = $input->nama;
        $item->foto       = $input->foto;
        $item->pendiri       = $input->pendiri;
        
        $item->pekerja       = $input->pekerja;
        $item->biografi     = $input->biografi;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = Kru::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}