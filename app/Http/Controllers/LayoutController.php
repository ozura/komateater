<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use App\Production;
use App\HProduction;
use App\HVideo;
use App\Category;
use App\Collection;
use App\CollectionCategory;
use App\Gallery;
use App\GalleryImage;
use App\Post;
use App\Popup;
use App\Image;
use App\HImage;
use App\PImage;
use App\Menu;
use App\Setting;
use App\Video;
use App\Tiket;
use App\Partner;
use Auth;
use Carbon\Carbon;
use Session;
use View;

class LayoutController extends BaseController{
    
    public function indexHome(Request $request){
        $portfolios = Post::select('post_id', 'post_title', 'post_url', 'post_caption', 'post_image_url', 'created_at')->where('category_id', 2)->limit(7)->orderBy('created_at', 'desc')->get();
        $news_posts = Post::select('post_id', 'post_title', 'post_url', 'post_caption', 'post_image_url', 'created_at')->where('category_id', 1)->limit(4)->orderBy('created_at', 'desc')->get();
        $galleries = Gallery::select('gallery_id', 'gallery_name', 'created_at')->limit(6)->orderBy('created_at', 'desc')->get();
        $collection_banner = Collection::select('thumbnail_url', 'target_url')->where('collection_category_id', 1)->orderBy('sequence', 'asc')->get();
        $collection_client = Collection::select('thumbnail_url', 'target_url')->where('collection_category_id', 2)->orderBy('sequence', 'asc')->get();
        $image = Image::select('image_id', 'image_url', 'image_name', 'updated_at')
                            ->limit(3)->orderby('updated_at', 'desc')
                            ->get();
        $smerchandise = Setting::select('setting_name','description','setting_content')->where('setting_id','12')->first();
        $spartner = Setting::select('setting_name','description','setting_content')->where('setting_id','11')->first();
       
        // $setiket = Setting::select('setting_content')->where('setting_id','5')->first();
        
         // $video = Video::select('video_name','video_url')->orderBy('video_id','desc')->get();
          $video = HVideo::select('videos.video_url','videos.video_name')->leftJoin('videos','videos.video_id','=','hvideos.video_id')->where('videos.deleted_at',null)->orderBy('hvideos.updated_at','desc')->limit(6)->get();
          $slideshow = HImage::select('images.image_url','images.image_name')->leftJoin('images','images.image_id','=','himages.image_id')->orderBy('himages.updated_at','desc')->limit(5)->get();
          // $partner = PImage::select('images.image_url','images.image_name')->leftJoin('images','images.image_id','=','pimages.image_id')->orderBy('pimages.updated_at','desc')->limit(6)->get();

          $partner = Partner::all();
         $tproduksi = Production::orderby('produksi_id','desc')->first();
         // $produksi = Production::where('tampilkan','1')->orderby('urutan')->limit(6)->get();

        $tproduksi = HProduction::select('productions.judul_lakon','productions.produksi_id','productions.foto','productions.tempat_pentas','productions.awal_pentas','productions.akhir_pentas','productions.video')->leftJoin('productions','productions.produksi_id','=','hproductions.produksi_id')->orderBy('hproductions.updated_at','desc')->first();
         $produksi = HProduction::select('productions.judul_lakon','productions.produksi_id','productions.foto')->leftJoin('productions','productions.produksi_id','=','hproductions.produksi_id')->where('productions.deleted_at',NULL)->orderBy('hproductions.updated_at','desc')->limit(3)->get();
         $cmerchandise = CollectionCategory::get();
         // dd($produksi);
         $merchandise = Collection::get();
          // $tiket = Production::whereNotNull('tiket')->where('judul_lakon',$setiket->setting_content)->orderby('updated_at','desc')->limit(1)->first();

         $tiket = Tiket::orderBy('updated_at')->first();
         $popup = Popup::first();
          // dd($tiket);
        //  $facebook = Setting::select('setting_content')->where('setting_id','2')->first();
        //  $twitter = Setting::select('setting_content')->where('setting_id','1')->first();
        //  $youtube = Setting::select('setting_content')->where('setting_id','4')->first();
        //  $instagram = Setting::select('setting_content')->where('setting_id','3')->first();
        //   $saddress = Setting::select('setting_content')->where('setting_id','9')->first();
        // $smaps = Setting::select('setting_content')->where('setting_id','10')->first();

        //  View::share(['facebook' => $facebook, 'twitter' => $twitter,'youtube'=>$youtube,'instagram'=>$instagram,'saddress'=>$saddress,'smaps'=>$smaps]);
         

        if(Session::has('lang')){
            $lang = Session::get('lang');
        }else{
            $lang = 'ID';
        }

        $menus = Menu::orderBy('menu_root', 'menu_order')->get();
        return view('index', compact('lang', 'news_posts', 'galleries', 'collection_banner', 'collection_client','menus', 'portfolios','image','video','tproduksi','produksi','smerchandise','tiket','cmerchandise','merchandise','slideshow','facebook','twitter','youtube','instagram','partner','popup','spartner','saddress','smaps'));

    }

    public function indexPost(Request $request, $post_url = ''){
        if($post = Post::where('post_url', $post_url)->first()){
            $category = Category::find($post->category_id);
            $other_posts = Post::select('post_id', 'post_title', 'post_caption', 'post_url','post_image_url', 'created_at')->where('post_id', '<>', $post->post_id)->where('category_id', 1)->limit(3)->orderBy('created_at', 'desc')->get();
            // dd($post);
            if(Session::has('lang')){
                $lang = Session::get('lang');
            }else{
                $lang = 'ID';
            }
            $menus = Menu::orderBy('menu_root', 'menu_order')->get();
            return view('post', compact('lang', 'post', 'category', 'other_posts', 'menus'));
        }else{
            return abort(404);
        }
    }

    public function indexPosts(Request $request, $category_id = 0){
        $posts = Post::select('post_id', 'post_title', 'post_url', 'post_image_url', 'created_at')->where('category_id', $category_id)->orderBy('post_id','desc')->limit(6)->get();
        $category = Category::find($category_id);
        // dd($posts);
        if(Session::has('lang')){
            $lang = Session::get('lang');
        }else{
            $lang = 'ID';
        }
        $menus = Menu::orderBy('menu_root', 'menu_order')->get();
        return view('pages/posts', compact('lang', 'posts', 'category', 'menus'));
    }

    public function indexGalleryImages(Request $request, $gallery_id = 0){
        $gallery_images = GalleryImage::select('images.image_id', 'images.image_url', 'images.image_name', 'images.updated_at')
                                ->join('images', 'gallery_images.image_id', '=', 'images.image_id')
                                ->where('gallery_images.gallery_id', $gallery_id)
                                ->orderby('updated_at', 'desc')
                                ->get();
        $gallery = Gallery::find($gallery_id);
        $other_posts = Post::select('post_id', 'post_title', 'post_caption', 'post_url','post_image_url', 'created_at')->where('category_id', 1)->limit(3)->orderBy('created_at', 'desc')->get();

        if(Session::has('lang')){
            $lang = Session::get('lang');
        }else{
            $lang = 'ID';
        }
        $menus = Menu::orderBy('menu_root', 'menu_order')->get();
        return view('album-images', compact('lang', 'gallery', 'gallery_images', 'other_posts','menus'));
    }

    public function indexGalleries(Request $request){
        $galleries = Gallery::select('gallery_id', 'gallery_name', 'updated_at')
                                ->orderby('updated_at', 'desc')
                                ->paginate(15);

        $other_posts = Post::select('post_id', 'post_title', 'post_caption', 'post_url','post_image_url', 'created_at')->where('category_id', 1)->limit(3)->orderBy('created_at', 'desc')->get();

        if(Session::has('lang')){
            $lang = Session::get('lang');
        }else{
            $lang = 'ID';
        }
        $menus = Menu::orderBy('menu_root', 'menu_order')->get();
        return view('album', compact('lang', 'galleries', 'other_posts', 'menus'));
    }

}