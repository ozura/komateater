<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;

use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class PostController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Posts', 'link' => 'post']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-post', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $post_id = 0){
        if($post = Post::find($post_id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Posts', 'link' => 'post'],
                (object) ['name' => 'Update Post', 'link' => 'post/update/'.$post_id]
            );
        }else{
            $post = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Posts', 'link' => 'post'],
                (object) ['name' => 'New Post', 'link' => 'post/new']
            );
        }

        return view('admin/pages/manage-post', compact('breadcrumb', 'post'));
    }

    /* API */
    public function commonList(Request $request){
        $list_data = Post::select('post_id', 'post_title', 'category_name', 'posts.created_at')
                        ->join('categories', 'categories.category_id', '=', 'posts.category_id')
                        ->orderBy('posts.created_at', 'desc')
                        ->get();

        return Datatables::of($list_data)
                ->editColumn('created_at', function($item){
                    return $item->created_at->format('j M Y H:i').' WIB';
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->post_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->post_id)){
            $post = new Post;
            $redirect = true;
        }else{
            if($item = Post::find($input->post_id)){
                $post = $item;
                $redirect = false;
            }else{
                $post = new Post;
                $redirect = true;
            }
        }
        $bom = '\xEF\xBB\xBF';
        
        $post_title = GlobalFunction::getStringLang($input->post_title, 'ID');
        $post->category_id      = $input->category_id;
        $post->post_url         = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ','-',trim(basename(stripslashes(strtolower($post_title))), ".\x00..\x20")));
        $post->post_image_url   = $input->post_image_url;
        $post->post_title       = $input->post_title;
        $post->post_content     = $input->post_content;
        $post->post_caption     = str_replace($bom, '', ucfirst(strtolower(stripslashes(strip_tags($input->post_content)))));
        
        if($post->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($item = Post::find($input->post)){
            $post = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($post->delete()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}