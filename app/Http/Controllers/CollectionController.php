<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use App\CollectionCategory;
use App\Collection;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class CollectionController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Merchandise', 'link' => 'collection']
        );

        return view('admin/pages/list-collection', compact('breadcrumb'));
    }

    public function commonList(Request $request){
        $list_data = Collection::selectRaw('
                            collection_id,
                            collections.name collection_name,
                            collection_categories.collection_category_id,
                            collection_categories.name category_name,
                            thumbnail_url,
                            target_url,
                            sequence
                        ')
                        ->join('collection_categories', 'collection_categories.collection_category_id', '=', 'collections.collection_category_id')
                        ->where('collection_categories.deleted_at',null)
                        ->orderBy('collections.collection_category_id', 'asc')
                        ->orderBy('collections.sequence', 'asc')
                        ->get();

        return Datatables::of($list_data)
                ->addColumn('image', function($item){
                    if(!empty($item->thumbnail_url)){
                        $thumbnail = $item->thumbnail_url;
                    }else{
                        $thumbnail = url('media/no-image.jpg');
                    }
                    
                    $data = array(
                        'thumbnail' => $thumbnail,
                        'category' => $item->category_name,
                        'name' => $item->collection_name
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    if(!empty($item->target_url)){
                        $url = $item->target_url;
                    }else{
                        $url = '#';
                    }

                    $data = array(
                        'id' => $item->collection_id,
                        'target' => $url,
                        'content' => $item
                    );
                    return $data;
                })
                ->make(true);
    }

     
      public function indexManage(Request $request, $id = 0){
        if($item = Collection::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Collection', 'link' => 'collection'],
                (object) ['name' => 'Update collection', 'link' => 'collection/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'merchandise', 'link' => 'merchandise'],
                (object) ['name' => 'New Merchandise', 'link' => 'merchandise/new']
            );
        }

        return view('admin/pages/manage-collection', compact('breadcrumb', 'item'));
    }
    public function actionSave2(Request $request){
        $input = (object) $request->input();
        
        $kategori = new CollectionCategory;
        $kategori->name = $input->kategori;

        
        
        if($kategori->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }



     public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($request->collection_id)){
            $collection = new Collection;
        }else{
            if($item = Collection::find($request->collection_id)){
                $collection = $item;
            }else{
                $collection = new Collection;
            }
        }

        $collection->collection_category_id = $input->category;
        $collection->name = $input->name;
        $collection->thumbnail_url = $input->foto;
        // dd($input->foto);
        $collection->keterangan = $input->keterangan;
        
        
        if($collection->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($item = Collection::find($input->collection)){
            $collection = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($collection->delete()){
            return ['status' => 200, 'message' => 'Successfully deleted record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}