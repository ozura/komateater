<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
use App\Tiket;
use App\Production;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Session;

class TiketController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'tiket', 'link' => 'tiket']
        );

        $data = array(
            'breadcrumb' => $breadcrumb
        );
        return view('admin/pages/list-tiket', compact('breadcrumb'));
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Tiket::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Tiket', 'link' => 'tiket'],
                (object) ['name' => 'Update tiket', 'link' => 'tiket/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'tiket', 'link' => 'tiket'],
                (object) ['name' => 'New tiket', 'link' => 'tiket/new']
            );
        }

        return view('admin/pages/manage-tiket', compact('breadcrumb', 'item'));
    }

    /* API */
    public function commonList(Request $request){
        $list_data = Tiket::orderby('tiket_id', 'desc')
                            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->tiket_id
                    );
                    return $data;
                })
                 // ->addColumn('tiket', function($item){
                    
                 //    if($item->tiket == 1){
                 //        return 'Ada';
                 //    }else{
                 //        return 'Tidak Ada ';
                 //    }
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->tiket_id)){
            $item = new Tiket;
            $redirect = true;
        }else{
            if($item = Tiket::find($input->tiket_id)){                
                $redirect = false;
            }else{
                $item = new Tiket;
                $redirect = true;
            }
        }
        
        $item->tiket       = $input->tiket;
        $item->nama       = $input->nama;
        $item->tampilkan       = $input->tampilkan;
        
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Successfully save record!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    
     public function actionDelete(Request $request){
        $input = (object) $request->input();

        if(!empty($input->id)){
            if($item = Production::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Delete Successfully'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}