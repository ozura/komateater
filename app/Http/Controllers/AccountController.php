<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

use Yajra\Datatables\Datatables;
use App\Account;
use App\Post;

use Auth;
use DB;
use Session;

class AccountController extends Controller{
    public function indexSignIn(Request $request){
        if(Auth::check()){
            return Redirect::to('cdmanager/dashboard');
        }else{
            return view('admin/pages/sign-in');
        }
    }

    public function indexDashboard(Request $request){
        return view('admin/pages/dashboard');
    }

    public function indexWelcome(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome']
        );

        $posts = Post::select('post_id', 'post_title')->limit(5)->orderBy('created_at', 'desc')->get();

        return view('admin/pages/welcome', compact('breadcrumb', 'posts'));
    }

    public function actionRegister(Request $request){
        $account = new Account;
        $account->username = 'admin';
        $account->password = Hash::make('admin');
        $account->save();

        Session::flash('toast-success', 'Register success');
        return Redirect::back();
    }

    public function actionSignIn(Request $request){
        $input = (object) $request->input();
        if (Auth::attempt(['username' => $input->username, 'password' => $input->password], true)) {
            return Redirect::to('cdmanager/dashboard');
        }else{
            Session::flash('toast-error', 'Sign in failed');
            return Redirect::back()->withInput();
        }
    }

    public function actionSignOut(Request $request){
        $input = (object) $request->input();
        Auth::logout();
        Session::flash('toast-success', 'Sign out successfully');
        return Redirect::to('cdmanager');
    }

    public function actionChangePassword(Request $request){
        $account = Auth::user();
        $input = (object) $request->input();
        if($input->new_password == $input->new_confirm_password){
            if (Auth::once(['username' => $account->username, 'password' => $input->old_password])) {
                $account->password = Hash::make($input->new_password);
                $account->save();
                return ['status' => 200, 'message' => 'Change password successfully'];
            }else{
                return ['status' => 201, 'message' => 'Your old password is incorrect'];
            }
        }else{
            return ['status' => 201, 'message' => 'Re-type your new password again'];
        }
    }
}