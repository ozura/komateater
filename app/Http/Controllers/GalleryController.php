<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\Helpers\UploadHandler;
use App\Gallery;
use App\GalleryImage;
use App\Image;
use Yajra\Datatables\Datatables;

use Auth;
use DB;
use Carbon\Carbon;
use Session;

class GalleryController extends BaseController{
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Galleries', 'link' => 'gallery']
        );

        return view('admin/pages/list-gallery', compact('breadcrumb'));
    }

    public function indexImageList(Request $request, $gallery_id = 0){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Galleries', 'link' => 'gallery'],
            (object) ['name' => 'Gallery Images', 'link' => 'gallery/image/'.$gallery_id]
        );

        return view('admin/pages/list-gallery-image', compact('breadcrumb', 'gallery_id'));
    }

    public function commonList(Request $request){

        $list_data = Gallery::select('gallery_id', 'gallery_name', 'updated_at')
                            ->orderby('updated_at', 'desc')
                            ->get();

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->gallery_id,
                        'name' => $item->gallery_name
                    );
                    return $data;
                })
                ->make(true);
    }

    public function commonImageList(Request $request, $gallery_id = 0){

        $list_data = GalleryImage::select('gallery_image_id', 'images.image_id', 'images.image_url', 'images.image_name', 'images.updated_at')
                        ->join('images', 'gallery_images.image_id', '=', 'images.image_id')
                        ->where('gallery_images.gallery_id', $gallery_id)
                        ->orderby('updated_at', 'desc')
                        ->get();

        return Datatables::of($list_data)
                ->addColumn('image', function($item){
                    $data = array(
                        'src' => url('storage/img/thumbnails').'/'.$item->image_url,
                        'original' => url('storage/img/origins').'/'.$item->image_url,
                        'alt' => $item->image_name,
                        'date' => with(new Carbon($item->updated_at))->format('j-m-Y')
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->gallery_image_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $input = (object) $request->input();

        if(!empty($input->gallery_id)){
            $gallery = Gallery::find($input->gallery_id);
        }else{
            $gallery = new Gallery;
        }
        $gallery->gallery_name = $input->gallery_name;
        $gallery->save();

        return ['status' => 200, 'message' => 'Successfully save record!'];
    }

    public function actionSaveImage(Request $request){
        $input = (object) $request->input();

        $gallery_image = new GalleryImage;
        $gallery_image->gallery_id = $input->gallery_id;
        
        $tanggal = Carbon::today()->format('Y-m-d');
        $upload_handler = new UploadHandler;
        $upload_handler->setDateDir($tanggal);
        $info = $upload_handler->post();
        if(isset($info[0]->name) && (!isset($info[0]->error)))
        {
            $image = new Image;
            $name = substr($info[0]->name, 0 , strripos($info[0]->name, '.'));
            $path_parts = pathinfo($info[0]->name);
            $name = $path_parts['filename'];
            $type = $path_parts['extension'];
            $image->image_name = $name;
            $image->image_type = $type;
            $image->image_url = $tanggal."/".$name.".".$type;
            $image->save();

            $gallery_image->image_id = $image->image_id;
            $gallery_image->save();
        }

        return back();
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($item = Gallery::find($input->gallery_id)){
            $gallery = $item;
            GalleryImage::where('gallery_id', $gallery->gallery_id)->delete();
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($gallery->delete()){
            return ['status' => 200, 'message' => 'Successfully delete record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDeleteImage(Request $request){
        $input = (object) $request->input();
        if($item = GalleryImage::find($input->gallery_image_id)){
            $gallery_image = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($gallery_image->delete()){
            return ['status' => 200, 'message' => 'Successfully delete record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}