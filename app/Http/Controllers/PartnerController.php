<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;

use Yajra\Datatables\Datatables;
use App\Partner;
use Auth;
use DB;
use Session;

class PartnerController extends Controller{
    
    public function indexList(Request $request){
        $breadcrumb = array(
            (object) ['name' => 'Dashboard', 'link' => 'welcome'],
            (object) ['name' => 'Partner', 'link' => 'partner']
        );

        return view('admin/pages/manage-partner', compact('breadcrumb'));
    }

    public function commonList(Request $request){
        $list_data = Partner::all();

        return Datatables::of($list_data)
                ->addColumn('image', function($item){
                    if(!empty($item->thumbnail_url)){
                        $thumbnail = $item->thumbnail_url;
                    }else{
                        $thumbnail = url('media/no-image.jpg');
                    }
                    
                    $data = array(
                        'thumbnail' => $thumbnail,
                        
                        'name' => $item->name
                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    if(!empty($item->target_url)){
                        $url = $item->target_url;
                    }else{
                        $url = '#';
                    }

                    $data = array(
                        'id' => $item->partner_id,
                        'target' => $url,
                        'content' => $item
                    );
                    return $data;
                })
                ->make(true);
    }

      public function indexManage(Request $request, $id = 0){
        if($item = Collection::find($id)){
            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'Collection', 'link' => 'collection'],
                (object) ['name' => 'Update collection', 'link' => 'collection/update/'.$id]
            );
        }else{
            $item = null;

            $breadcrumb = array(
                (object) ['name' => 'Dashboard', 'link' => 'welcome'],
                (object) ['name' => 'tiket', 'link' => 'tiket'],
                (object) ['name' => 'New tiket', 'link' => 'tiket/new']
            );
        }

        return view('admin/pages/manage-collection', compact('breadcrumb', 'item'));
    }
    public function actionSave2(Request $request){
        $input = (object) $request->input();
        
        $kategori = new CollectionCategory;
        $kategori->name = $input->kategori;

        
        
        if($kategori->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }



     public function actionSave(Request $request){
        $input = (object) $request->input();
        if(empty($input->partner)){
            $partner = new Partner;
        }else{
            if($item = Partner::find($input->partner)){
                $partner = $item;
            }else{
                $partner = new Partner;
            }
        }

        
        $partner->name = $input->name;
        $partner->thumbnail_url = $input->image;
        $partner->target_url = $input->redirect;
        // $partner->sequence = $input->order;
        
        if($partner->save()){
            return ['status' => 200, 'message' => 'Successfully save record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        // dd($input->partner);
        if($item = Partner::find($input->partner)){
            $partner = $item;
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }

        if($partner->delete()){
            return ['status' => 200, 'message' => 'Successfully deleted record!'];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
}