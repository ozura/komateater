<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crew extends Model
{
    //
    protected $table = 'crews';
    protected $primaryKey = 'crew_id';

	public $timestamps = false;
}
