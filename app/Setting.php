<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Setting
 */
class Setting extends Model
{
    use SoftDeletes;
    
    protected $table = 'setting';

    protected $primaryKey = 'setting_id';

	public $timestamps = true;

    protected $fillable = [
        'setting_name',
        'description',
        'setting_content'
    ];

    protected $guarded = [];

}